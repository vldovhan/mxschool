<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Kaltura version file.
 *
 * @package    local_kaltura_announcements
 * @author     KALTURA
 * @copyright  2016 KALTURA, kaltura.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

require('../../config.php');

$search     = optional_param('search', '', PARAM_RAW);
$download   = optional_param('download', '', PARAM_ALPHA);
$action     = optional_param('action', '', PARAM_RAW);
$id         = optional_param('id', 0, PARAM_INT);

$systemcontext   = context_system::instance();
require_login();
$title = get_string('pluginname', 'local_mxschool');

$PAGE->set_url(new moodle_url("/local/mxschool/index.php", array()));
$PAGE->navbar->add($title);
$PAGE->set_pagelayout('admin');
$PAGE->set_context(context_system::instance());
$PAGE->set_title($title);
$PAGE->set_heading($title);

echo $OUTPUT->header();
echo $OUTPUT->heading($title);

echo html_writer::start_tag('div', array('class' => 'mxschool-box'));
    echo html_writer::start_tag('ul', array('class' => 'mxschool-manage-menu'));
        echo html_writer::tag('li', html_writer::link(new moodle_url('/local/mxschool/advisor_selection/index.php'), get_string('advisor_selection', 'local_mxschool')));
        echo html_writer::tag('li', html_writer::link(new moodle_url('/local/mxschool/vacationandtravel/index.php'), get_string('vacationandtravel', 'local_mxschool')));
        echo html_writer::tag('li', html_writer::link(new moodle_url('/local/mxschool/rooming/index.php'), get_string('rooming_name', 'local_mxschool')));
        echo html_writer::tag('li', html_writer::link(new moodle_url('/local/mxschool/user_management/index.php'), get_string('user_management', 'local_mxschool')));
        echo html_writer::tag('li', html_writer::link(new moodle_url('/local/mxschool/announcements/index.php'), get_string('announcements', 'local_mxschool')));
        echo html_writer::tag('li', html_writer::link(new moodle_url('/local/mxschool/notifications/settings.php'), get_string('notifications', 'local_mxschool')));
        echo html_writer::tag('li', html_writer::link(new moodle_url('/local/mxschool/preferences/index.php'), get_string('preferences_name', 'local_mxschool')));
        echo html_writer::tag('li', html_writer::link(new moodle_url('/local/mxschool/tutors/index.php'), get_string('tutors_name', 'local_mxschool')));
        echo html_writer::tag('li', html_writer::link(new moodle_url('/local/mxschool/check-in/index.php'), get_string('checkin_name', 'local_mxschool')));
        echo html_writer::tag('li', html_writer::link(new moodle_url('/local/mxschool/weekend/index.php'), get_string('weekend_name', 'local_mxschool')));
        echo html_writer::tag('li', html_writer::link(new moodle_url('/local/mxschool/driving/index.php'), get_string('driving_name', 'local_mxschool')));
        echo html_writer::tag('li', html_writer::link(new moodle_url('/local/mxschool/esignout/index.php'), get_string('esignout_name', 'local_mxschool')));
    echo html_writer::end_tag('ul');
echo html_writer::end_tag('div', array('class' => 'mxschool-box'));

echo $OUTPUT->footer();
