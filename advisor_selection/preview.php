<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

require('../../../config.php');

$id         = optional_param('id', 0, PARAM_INT);

require_login();
require_capability('local/mxschool:advisor_selection_manage', context_system::instance());

$title = get_string('adviser_record', 'local_mxschool');

if ($id){
    $sql = "SELECT mas.id, mas.keep_current_advisor, mas.comment, s.student, ca.current_advisor, a1.advisor_1, a2.advisor_2, a3.advisor_3, a4.advisor_4, a5.advisor_5, fa.finaladvisor_name
        FROM {local_mxschool_advisors} mas
            LEFT JOIN (SELECT s.id, CONCAT(u.lastname, ', ', u.firstname) as student FROM {local_mxschool_students} s LEFT JOIN {user} u ON u.id = s.userid WHERE u.id > 0) s ON s.id = mas.studentid
            LEFT JOIN (SELECT f.id, CONCAT(u.firstname, ' ', u.lastname) as current_advisor FROM {local_mxschool_faculty} f LEFT JOIN {user} u ON u.id = f.userid WHERE u.id > 0) ca ON ca.id = mas.currentadvisor
            LEFT JOIN (SELECT f.id, CONCAT(u.firstname, ' ', u.lastname) as advisor_1 FROM {local_mxschool_faculty} f LEFT JOIN {user} u ON u.id = f.userid WHERE u.id > 0) a1 ON a1.id = mas.advisor1
            LEFT JOIN (SELECT f.id, CONCAT(u.firstname, ' ', u.lastname) as advisor_2 FROM {local_mxschool_faculty} f LEFT JOIN {user} u ON u.id = f.userid WHERE u.id > 0) a2 ON a2.id = mas.advisor2
            LEFT JOIN (SELECT f.id, CONCAT(u.firstname, ' ', u.lastname) as advisor_3 FROM {local_mxschool_faculty} f LEFT JOIN {user} u ON u.id = f.userid WHERE u.id > 0) a3 ON a3.id = mas.advisor3
            LEFT JOIN (SELECT f.id, CONCAT(u.firstname, ' ', u.lastname) as advisor_4 FROM {local_mxschool_faculty} f LEFT JOIN {user} u ON u.id = f.userid WHERE u.id > 0) a4 ON a4.id = mas.advisor4
            LEFT JOIN (SELECT f.id, CONCAT(u.firstname, ' ', u.lastname) as advisor_5 FROM {local_mxschool_faculty} f LEFT JOIN {user} u ON u.id = f.userid WHERE u.id > 0) a5 ON a5.id = mas.advisor5
            LEFT JOIN (SELECT f.id, CONCAT(u.firstname, ' ', u.lastname) as finaladvisor_name FROM {local_mxschool_faculty} f LEFT JOIN {user} u ON u.id = f.userid WHERE u.id > 0) fa ON fa.id = mas.finaladvisor
        WHERE mas.id = $id";
    $record = $DB->get_record_sql($sql);
}

$PAGE->set_url(new moodle_url("/local/mxschool/advisor_selection/preview.php", array('id'=>$id)));
$PAGE->navbar->add(get_string('pluginname', 'local_mxschool'), new moodle_url('/local/mxschool/index.php'));
$PAGE->navbar->add(get_string('advisor_selection', 'local_mxschool'), new moodle_url('/local/mxschool/advisor_selection/index.php'));
$PAGE->navbar->add(get_string('view_and_edit', 'local_mxschool'), new moodle_url('/local/mxschool/advisor_selection/view.php'));
$PAGE->navbar->add($title);
$PAGE->set_pagelayout('admin');
$PAGE->set_context(context_system::instance());
$PAGE->set_title($title);
$PAGE->set_heading($title);

echo $OUTPUT->header();
echo $OUTPUT->heading($title);

echo html_writer::start_tag("div", array('class'=>'mxschool-preview-box'));
if ($record){
    echo html_writer::start_tag('table', array('class'=>'flexible generaltable generalbox'));
        echo html_writer::start_tag('tr', array('class'=>'r1'));
            echo html_writer::tag('th', 'Student\'s name:');
            echo html_writer::tag('td', $record->student);
        echo html_writer::end_tag('tr');
        echo html_writer::start_tag('tr', array('class'=>'r0'));
            echo html_writer::tag('th', 'Current Advisor:');
            echo html_writer::tag('td', $record->current_advisor);
        echo html_writer::end_tag('tr');
        echo html_writer::start_tag('tr', array('class'=>'r1'));
            echo html_writer::tag('th', 'Keep current advisor?');
            echo html_writer::tag('td', ucfirst($record->keep_current_advisor));
        echo html_writer::end_tag('tr');
        echo html_writer::start_tag('tr', array('class'=>'r0'));
            echo html_writer::tag('th', 'Comments:');
            echo html_writer::tag('td', $record->comment);
        echo html_writer::end_tag('tr');
        if ($record->keep_current_advisor == 'no'){
            echo html_writer::start_tag('tr', array('class'=>'r1'));
                echo html_writer::tag('th', '1st Choice:');
                echo html_writer::tag('td', $record->advisor_1);
            echo html_writer::end_tag('tr');
            echo html_writer::start_tag('tr', array('class'=>'r0'));
                echo html_writer::tag('th', '2nd Choice:');
                echo html_writer::tag('td', $record->advisor_2);
            echo html_writer::end_tag('tr');
            echo html_writer::start_tag('tr', array('class'=>'r1'));
                echo html_writer::tag('th', '3st Choice:');
                echo html_writer::tag('td', $record->advisor_3);
            echo html_writer::end_tag('tr');
            echo html_writer::start_tag('tr', array('class'=>'r0'));
                echo html_writer::tag('th', '4nd Choice:');
                echo html_writer::tag('td', $record->advisor_4);
            echo html_writer::end_tag('tr');
             echo html_writer::start_tag('tr', array('class'=>'r1'));
                echo html_writer::tag('th', '5st Choice:');
                echo html_writer::tag('td', $record->advisor_5);
            echo html_writer::end_tag('tr');
        }
        echo html_writer::start_tag('tr', array('class'=>($record->keep_current_advisor == 'no') ? 'r0' : 'r1'));
            echo html_writer::tag('th', 'Chosen Advisor:');
            echo html_writer::tag('td', ($record->finaladvisor_name) ? $record->finaladvisor_name : 'Not selected yet by the Dean');
        echo html_writer::end_tag('tr');
    echo html_writer::end_tag('table');
    
    echo html_writer::start_tag('div', array('class'=>'mxschool-bottom-actions'));
        echo html_writer::tag('button', 'Edit', array('onclick'=>'location="'.$CFG->wwwroot.'/local/mxschool/advisor_selection/edit.php?id='.$id.'"'));
        echo html_writer::tag('button', 'Delete', array('onclick'=>'if (confirm("Are you sure want to delete this redord?")) location="'.$CFG->wwwroot.'/local/mxschool/advisor_selection/view.php?action=delete&id='.$id.'"'));
        echo html_writer::tag('button', 'Back to the list', array('onclick'=>'location="'.$CFG->wwwroot.'/local/mxschool/advisor_selection/view.php"'));
    echo html_writer::end_tag('div');
}
echo html_writer::end_tag("div");

echo $OUTPUT->footer();
