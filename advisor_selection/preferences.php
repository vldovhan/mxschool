<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */


require('../../../config.php');
require_once('preferences_form.php');
require_once($CFG->dirroot."/local/mxschool/classes/alerts/alerts.php");

$systemcontext   = context_system::instance();
require_login();
require_capability('local/mxschool:advisor_selection_settings', context_system::instance());
$title = get_string('preferences_sa', 'local_mxschool');

$PAGE->set_url(new moodle_url("/local/mxschool/advisor_selection/preferences.php", array()));
$PAGE->navbar->add(get_string('pluginname', 'local_mxschool'), new moodle_url('/local/mxschool/index.php'));
$PAGE->navbar->add(get_string('advisor_selection', 'local_mxschool'), new moodle_url('/local/mxschool/advisor_selection/index.php'));
$PAGE->navbar->add($title);
$PAGE->requires->jquery();
$PAGE->requires->js('/local/mxschool/assets/js/script.js', true);
$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('admin');
$PAGE->set_title($title);
$PAGE->set_heading($title);

$advisors = $DB->get_records_sql("SELECT f.*, CONCAT(u.firstname, ' ', u.lastname) as username, u.email 
                                    FROM {local_mxschool_faculty} f 
                                        LEFT JOIN {user} u ON u.id = f.userid 
                                    ORDER BY u.firstname, u.lastname");

$settings = array();
$settings['second_semester_starts'] = get_config('local_mxschool', 'second_semester_starts');
$settings['dorms_close'] = get_config('local_mxschool', 'dorms_close');
$settings['advisor_form_enable'] = get_config('local_mxschool', 'advisor_form_enable');
$settings['advisor_form_time_prior'] = get_config('local_mxschool', 'advisor_form_time_prior');
//$settings['advisor_form_message'] = get_config('local_mxschool', 'advisor_form_message');

$editform = new edit_form(null, array('settings'=>$settings, 'advisors'=>$advisors));

if ($editform->is_cancelled()) {
    // The form has been cancelled, take them back to what ever the return to is.
    redirect(new moodle_url('/local/mxschool/advisor_selection/index.php'));
} else if ($data = $editform->get_data()) {
    // Process data if submitted.
    set_config('advisor_form_enable', $data->advisor_form_enable, 'local_mxschool');
    set_config('advisor_form_time_prior', $data->advisor_form_time_prior, 'local_mxschool');
    //set_config('advisor_form_message', $data->advisor_form_message['text'], 'local_mxschool');
    
    if (count($advisors) > 0){
        foreach ($advisors as $advisor){
            $advisor_availability = (isset($data->{'advisor_'.$advisor->id}) and $data->{'advisor_'.$advisor->id} > 0) ? 1 : 0;
            if ($advisor_availability != $advisor->available){
                $advisor->available = $advisor_availability;
                $DB->update_record('local_mxschool_faculty', $advisor);
            }
        }
    }
    
    if (isset($data->reset_records)){
        $records = $DB->get_records_sql("SELECT * FROM {local_mxschool_advisors} WHERE status > 0");
        if (count($records)){
            foreach ($records as $record){
                $record->status = 0;
                $DB->update_record('local_mxschool_advisors', $record);
            }
        }
    }
    
    $jAlert->create(array('type'=>'success', 'text'=>'Preferences was successfully saved'));
    redirect(new moodle_url('/local/mxschool/advisor_selection/index.php'));
}

// Print the form.

$PAGE->set_context(context_system::instance());
$PAGE->set_title($title);
$PAGE->set_heading($title);

echo $OUTPUT->header();
echo $OUTPUT->heading($title);

echo html_writer::start_tag('div', array('class' => 'mx-adviser-settings-form'));
$editform->display();
echo html_writer::end_tag('div');

echo $OUTPUT->footer();
