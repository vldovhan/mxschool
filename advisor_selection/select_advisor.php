<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Kaltura version file.
 *
 * @package    local_mxschool
 * @author     mxschool
 * @copyright  2016 mxschool.edu
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */


require('../../../config.php');
require_once('select_advisor_form.php');

$systemcontext   = context_system::instance();
require_login();

$title = get_string('advisor_selection_form', 'local_mxschool');

$PAGE->set_url(new moodle_url("/local/mxschool/advisor_selection/select_advisor.php", array()));
$PAGE->navbar->add($title);
$PAGE->requires->jquery();
$PAGE->requires->js('/local/mxschool/assets/js/script.js', true);
$PAGE->requires->js('/local/mxschool/advisor_selection/sselect_script.js', true);
$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('admin');
$PAGE->set_title($title);
$PAGE->set_heading($title);

$settings = array();
$settings['second_semester_starts'] = get_config('local_mxschool', 'second_semester_starts');
$settings['dorms_close'] = get_config('local_mxschool', 'dorms_close');
$settings['advisor_form_enable'] = get_config('local_mxschool', 'advisor_form_enable');
$settings['advisor_form_time_prior'] = get_config('local_mxschool', 'advisor_form_time_prior');
$settings['advisor_form_message'] = get_config('local_mxschool', 'advisor_form_message');

$advisors = $DB->get_records_sql("SELECT f.*, CONCAT(u.firstname, ' ', u.lastname) as username, u.email 
                                    FROM {local_mxschool_faculty} f 
                                        LEFT JOIN {user} u ON u.id = f.userid 
                                        WHERE u.id > 0 AND f.available > 0
                                    ORDER BY u.firstname, u.lastname");

$student = $DB->get_record_sql("SELECT s.id, s.userid, u.firstname, u.lastname, s.advisor
                         FROM {local_mxschool_students} s
                            LEFT JOIN {user} u ON u.id = s.userid
                                WHERE s.userid = ?", array($USER->id));
$record = $DB->get_record('local_mxschool_advisors', array('studentid'=>$student->id));

$advisor = false;
if ($record->currentadvisor){
    $currentadvisor = $DB->get_record_sql("SELECT f.id, f.userid, u.firstname, u.lastname
                         FROM {local_mxschool_faculty} f
                            LEFT JOIN {user} u ON u.id = f.userid
                                WHERE f.id = ?", array($record->currentadvisor));
    if ($currentadvisor){
        $record->currentadvisor_id = $record->currentadvisor;
        $record->currentadvisor = $currentadvisor->firstname.' '.$currentadvisor->lastname;
    }
} elseif ($student->advisor){
    $currentadvisor = $DB->get_record_sql("SELECT f.id, f.userid, u.firstname, u.lastname
                         FROM {local_mxschool_faculty} f
                            LEFT JOIN {user} u ON u.id = f.userid
                                WHERE f.id = ?", array($student->advisor));
    if ($currentadvisor){
        $record->currentadvisor_id = $currentadvisor->id;
        $record->currentadvisor = $currentadvisor->firstname.' '.$currentadvisor->lastname;
    }
}

$editform = new edit_form(null, array('record'=>$record, 'settings'=>$settings, 'student'=>$student, 'advisors'=>$advisors));

if ($editform->is_cancelled()) {
    // The form has been cancelled, take them back to what ever the return to is.
    redirect(new moodle_url('/my'));
} else if ($data = $editform->get_data()) {
    
    $selection = new stdClass();
    if ($data->id > 0){
        $selection->id = $data->id;   
    }
    $selection->studentid = $data->studentid;
    $selection->currentadvisor = $data->currentadvisor_id;
    $selection->keep_current_advisor = $data->keep_current_advisor;
    if ($data->keep_current_advisor == 'no'){
        $selection->advisor1 = $data->advisor1;    
        $selection->advisor2 = $data->advisor2;    
        $selection->advisor3 = $data->advisor3;    
        $selection->advisor4 = $data->advisor4;    
        $selection->advisor5 = $data->advisor5;    
    } else {
        $selection->advisor1 = 0;    
        $selection->advisor2 = 0;    
        $selection->advisor3 = 0;    
        $selection->advisor4 = 0;    
        $selection->advisor5 = 0;
    }
    
    $selection->status = 0;  
    $selection->comment = $data->comment;  
    $selection->timecreated = time();    
    $selection->timemodified = time();    
    
    if ($data->id > 0){
        $DB->update_record('local_mxschool_advisors', $selection);
    } else {
        $selection->id = $DB->insert_record('local_mxschool_advisors', $selection);
    }
    
    redirect(new moodle_url('/my'));
}

// Print the form.

$PAGE->set_context(context_system::instance());
$PAGE->set_title($title);
$PAGE->set_heading($title);

echo $OUTPUT->header();
echo $OUTPUT->heading($title);

echo html_writer::start_tag('div', array('class' => 'mx-adviser-select-form'));
$editform->display();
echo html_writer::end_tag('div');

echo $OUTPUT->footer();

