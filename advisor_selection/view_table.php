<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/tablelib.php');

class view_table extends table_sql {
    function __construct($uniqueid, $search, $filter, $download) {
		global $CFG, $USER;

        parent::__construct($uniqueid);
        
        if ($download){
            $columns = array('student', 'current_advisor', 'keep_current_advisor', 'advisor_1', 'advisor_2', 'advisor_3', 'advisor_4', 'advisor_5', 'finaladvisor_name', 'finaladvisor');
            $headers = array(
                get_string('student', 'local_mxschool'),
                get_string('currentadvisor', 'local_mxschool'),
                get_string('keep_current_advisor', 'local_mxschool'),
                'First Choice',
                'Second Choice',
                'Third Choice',
                'Fourth Choice',
                'Fifth Choice',
                get_string('choosenadvisor', 'local_mxschool'));
        } else {
            $columns = array('student', 'current_advisor', 'keep_current_advisor', 'advisor_1', 'advisor_2', 'advisor_3', 'advisor_4', 'advisor_5', 'finaladvisor_name', 'actions');
            $headers = array(
                get_string('student', 'local_mxschool'),
                get_string('currentadvisor', 'local_mxschool'),
                get_string('keep_current_advisor', 'local_mxschool'),
                'First Choice',
                'Second Choice',
                'Third Choice',
                'Fourth Choice',
                'Fifth Choice',
                get_string('choosenadvisor', 'local_mxschool'),
                get_string('actions', 'local_mxschool'));
        }

        $this->define_columns($columns);
        $this->define_headers($headers);
        
        $sql_search = ($search) ? " AND (s.student LIKE '%$search%' OR ca.current_advisor LIKE '%$search%' OR a1.advisor_1 LIKE '%$search%' OR a2.advisor_2 LIKE '%$search%' OR a3.advisor_3 LIKE '%$search%' OR a4.advisor_4 LIKE '%$search%' OR a5.advisor_5 LIKE '%$search%' OR fa.finaladvisor_name LIKE '%$search%')" : "";
        if ($filter){
            if ($filter == '1') {
                $sql_search .= " AND mas.finaladvisor > 0";
            } elseif ($filter == '2'){
                $sql_search .= " AND mas.finaladvisor > 1 AND mas.status = 0";
            } elseif ($filter == '3'){
                $sql_search .= " AND mas.finaladvisor > 1 AND mas.status > 0";
            } 
        }
        
        $fields = "mas.id, mas.keep_current_advisor, mas.comment, mas.status, s.student, ca.current_advisor, a1.advisor_1, a2.advisor_2, a3.advisor_3, a4.advisor_4, a5.advisor_5, fa.finaladvisor_name";
        $from = "{local_mxschool_advisors} mas
                    LEFT JOIN (SELECT s.id, CONCAT(u.lastname, ', ', u.firstname) as student FROM {local_mxschool_students} s LEFT JOIN {user} u ON u.id = s.userid WHERE u.id > 0) s ON s.id = mas.studentid
                    LEFT JOIN (SELECT f.id, CONCAT(u.firstname, ' ', u.lastname) as current_advisor FROM {local_mxschool_faculty} f LEFT JOIN {user} u ON u.id = f.userid WHERE u.id > 0) ca ON ca.id = mas.currentadvisor
                    LEFT JOIN (SELECT f.id, CONCAT(u.firstname, ' ', u.lastname) as advisor_1 FROM {local_mxschool_faculty} f LEFT JOIN {user} u ON u.id = f.userid WHERE u.id > 0) a1 ON a1.id = mas.advisor1
                    LEFT JOIN (SELECT f.id, CONCAT(u.firstname, ' ', u.lastname) as advisor_2 FROM {local_mxschool_faculty} f LEFT JOIN {user} u ON u.id = f.userid WHERE u.id > 0) a2 ON a2.id = mas.advisor2
                    LEFT JOIN (SELECT f.id, CONCAT(u.firstname, ' ', u.lastname) as advisor_3 FROM {local_mxschool_faculty} f LEFT JOIN {user} u ON u.id = f.userid WHERE u.id > 0) a3 ON a3.id = mas.advisor3
                    LEFT JOIN (SELECT f.id, CONCAT(u.firstname, ' ', u.lastname) as advisor_4 FROM {local_mxschool_faculty} f LEFT JOIN {user} u ON u.id = f.userid WHERE u.id > 0) a4 ON a4.id = mas.advisor4
                    LEFT JOIN (SELECT f.id, CONCAT(u.firstname, ' ', u.lastname) as advisor_5 FROM {local_mxschool_faculty} f LEFT JOIN {user} u ON u.id = f.userid WHERE u.id > 0) a5 ON a5.id = mas.advisor5
                    LEFT JOIN (SELECT f.id, CONCAT(u.firstname, ' ', u.lastname) as finaladvisor_name FROM {local_mxschool_faculty} f LEFT JOIN {user} u ON u.id = f.userid WHERE u.id > 0) fa ON fa.id = mas.finaladvisor
                ";
        $where = 'mas.id > 0'.$sql_search;
        $this->set_sql($fields, $from, $where, array());
        $this->define_baseurl("$CFG->wwwroot/local/mxschool/advisor_selection/view.php?search=".$search."&filter=".$filter);
    }
    
    function col_keep_current_advisor($values) {
      return ucfirst($values->keep_current_advisor);
    }
    
    function col_actions($values) {
      global $OUTPUT, $PAGE;
        
        if ($this->is_downloading()){
            return '';
        }
        
      $strdelete  = get_string('delete');
      $stredit  = get_string('edit');
      $strpreview  = get_string('preview', 'local_mxschool');
      
        $edit = array();
        
        $aurl = new moodle_url('/local/mxschool/advisor_selection/preview.php', array('id'=>$values->id));
        $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/hide', $strpreview, 'core', array('class' => 'iconsmall')));
        
        $aurl = new moodle_url('/local/mxschool/advisor_selection/edit.php', array('id'=>$values->id));
        $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/edit', $stredit, 'core', array('class' => 'iconsmall')));

        $aurl = new moodle_url('/local/mxschool/advisor_selection/view.php', array('action'=>'delete', 'id'=>$values->id));
        $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/delete', $strdelete, 'core', array('class' => 'iconsmall')), null, array('onclick'=>"if (!confirm('Are you sure want to delete this redord?')) return false;"));
        
      return implode('', $edit);
    }
}
