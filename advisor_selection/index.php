<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

require('../../../config.php');
//require_once('lib.php');

require_login();
require_capability('local/mxschool:advisor_selection_manage', context_system::instance());
$title = get_string('advisor_selection', 'local_mxschool');

/*$faculties = $DB->get_records_sql("SELECT * FROM {local_mxschool_faculty}");   
foreach ($faculties as $faculty){
    /*$mxfaculty = $DB->get_record_sql("SELECT * FROM {user} WHERE id = '$faculty->userid'");
    if ($mxfaculty){
        $mxfaculty->phone1 = $faculty->phone;
        $DB->update_record('user', $mxfaculty);
    }*/
    /*$mxfaculty = $DB->get_record_sql("SELECT * FROM {local_mxschool_faculty} WHERE email = '$faculty->email'");
    if ($mxfaculty){
        $mxfaculty->userid = $faculty->id;
        $DB->update_record('local_mxschool_faculty', $mxfaculty);
    }*/
    //role_assign(10, $faculty->id, $systemcontext->id);
//}

/*$students = $DB->get_records_sql("SELECT * FROM {local_mxschool_students}");
foreach ($students as $student){
      
    $mxstudent = $DB->get_record_sql("SELECT f.id FROM {local_mxschool_faculty} f WHERE f.id = '$student->advisor'");
    
    if (!$mxstudent){
        $student->advisor = 0;
        $DB->update_record('local_mxschool_students', $student);
        echo $student->advisor.'<hr />';
    }*/
    
    /*$mxstudent = $DB->get_record_sql("SELECT * FROM {user} WHERE email = '$student->email'");
    if ($mxstudent){
        $mxstudent->phone2 = $student->studentcell;
        $DB->update_record('user', $mxstudent);
        
        $student->userid = $mxstudent->id;
        $DB->update_record('local_mxschool_students', $student);
        role_assign(5, $student->userid, $systemcontext->id);
        echo $student->userid.'<hr />';
    }*/
    /*$mxfaculty = $DB->get_record_sql("SELECT * FROM {local_mxschool_faculty} WHERE email = '$faculty->email'");
    if ($mxfaculty){
        $mxfaculty->userid = $faculty->id;
        $DB->update_record('local_mxschool_faculty', $mxfaculty);
    }*/
//}

$PAGE->set_url(new moodle_url("/local/mxschool/advisor_selection/index.php", array()));
$PAGE->navbar->add(get_string('pluginname', 'local_mxschool'), new moodle_url('/local/mxschool/index.php'));
$PAGE->navbar->add($title);
$PAGE->set_pagelayout('course');
$PAGE->set_context(context_system::instance());
$PAGE->set_title($title);
$PAGE->set_heading($title);

echo $OUTPUT->header();
echo $OUTPUT->heading($title, 2);

echo html_writer::start_tag('div', array('class' => 'mxschool-box'));
    echo html_writer::start_tag('ul', array('class' => 'mxschool-manage-menu'));
        echo html_writer::tag('li', html_writer::link(new moodle_url('/local/mxschool/advisor_selection/select.php'), get_string('select_advisor', 'local_mxschool')));
        echo html_writer::tag('li', html_writer::link(new moodle_url('/local/mxschool/advisor_selection/view.php'), get_string('view_and_edit', 'local_mxschool')));
        echo html_writer::tag('li', html_writer::link(new moodle_url('/local/mxschool/advisor_selection/manage.php'), get_string('manage_student_selections', 'local_mxschool')));
        echo html_writer::tag('li', html_writer::link(new moodle_url('/local/mxschool/advisor_selection/pending.php'), get_string('students_not_selected', 'local_mxschool')));
        if (has_capability('local/mxschool:advisor_selection_settings', context_system::instance())){
            echo html_writer::tag('li', html_writer::link(new moodle_url('/local/mxschool/advisor_selection/preferences.php'), get_string('preferences_sa', 'local_mxschool')));
        }
    echo html_writer::end_tag('ul');
echo html_writer::end_tag('div', array('class' => 'mxschool-box'));

echo $OUTPUT->footer();

