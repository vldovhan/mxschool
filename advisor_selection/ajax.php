<?php
define('AJAX_SCRIPT', true);
require_once('../../../config.php');

require_login();

$id		  = optional_param('id', 0, PARAM_INT);
$type	  = optional_param('type', 0, PARAM_INT);
$action = optional_param('action', '', PARAM_RAW);
$form	= (object)clean_param_array($_POST['form'], PARAM_RAW, true);

if($action == 'get_current_advisor' and $id){
    $advisor_name = ''; $advisor_id = 0; $available = 0;
    
    $student = $DB->get_record("local_mxschool_students", array('id'=>$id));
    if (isset($student->advisor) and (int)$student->advisor > 0) {
        $faculty = $DB->get_record_sql("SELECT f.id, f.available, CONCAT(u.firstname, ' ', u.lastname) as username FROM {local_mxschool_faculty} f LEFT JOIN {user} u ON u.id = f.userid WHERE f.id = '$student->advisor' AND u.id > 0");
        $record = $DB->get_record('local_mxschool_advisors', array('studentid'=>$student->id));
        if ($faculty /*and $record*/){
            $advisor_name = $faculty->username;
            $advisor_id = $faculty->id;
            $available = $faculty->available;
        }
    }
    
    echo json_encode(array(
       'advisor_name' => $advisor_name,
       'advisor_id' => $advisor_id,
			 'available' => $available
    ));
} elseif ($action == 'set_final_advisor'){
    $rid	  = optional_param('rid', '', PARAM_RAW);
    $adv	  = optional_param('adv', '', PARAM_RAW);
    $id  = (int)str_replace('final-adv-', '', $rid);
    
    if ($id > 0){
        $record = $DB->get_record('local_mxschool_advisors', array('id'=>$id));
        if ($record){
            $record->finaladvisor = $adv;
            $DB->update_record('local_mxschool_advisors', $record);
            echo '1';
        }
    }
} elseif ($action == 'set_final_advisor_other'){
    $rid	  = optional_param('rid', '', PARAM_RAW);
    $adv	  = optional_param('adv', '', PARAM_RAW);
    $id  = (int)str_replace('other-adv-select-', '', $rid);
    
    if ($id > 0){
        $record = $DB->get_record('local_mxschool_advisors', array('id'=>$id));
        if ($record){
            $record->keep_current_advisor = 'no';
            if($record->advisor1 == 0){
                $record->advisor1 = $adv;   
            } elseif($record->advisor2 == 0){
                $record->advisor2 = $adv;
            } elseif($record->advisor3 == 0){
                $record->advisor3 = $adv;
            } elseif($record->advisor4 == 0){
                $record->advisor4 = $adv;
            } elseif($record->advisor5 == 0){
                $record->advisor5 = $adv;
            } else {
                $record->advisor1 = $adv;
            }
            $record->finaladvisor = $adv;
            $DB->update_record('local_mxschool_advisors', $record);
            
            echo '1';
        }
    }
}
exit;