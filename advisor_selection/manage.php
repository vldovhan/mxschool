<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

require('../../../config.php');
require ('manage_table.php');
require ('../lib.php');

$search     = optional_param('search', '', PARAM_RAW);
$download   = optional_param('download', '', PARAM_ALPHA);
$filter     = optional_param('filter', 0, PARAM_INT);
$action     = optional_param('action', '', PARAM_RAW);
$id         = optional_param('id', 0, PARAM_INT);

if ($download == ''){
    require_once($CFG->dirroot."/local/mxschool/classes/alerts/alerts.php");
}

require_login();
require_capability('local/mxschool:advisor_selection_manage', context_system::instance());

if ($action == 'send'){
    require('../classes/notifications.php');
    
    $items = $DB->get_records_sql("SELECT s.id, mas.id as selectid, s.userid, CONCAT(u.firstname, ' ', u.lastname) as student, u.email, pa.previous_advisor, ca.current_advisor
                                FROM {local_mxschool_advisors} mas 
                                    LEFT JOIN {local_mxschool_students} s ON s.id = mas.studentid
                                    LEFT JOIN {user} u ON u.id = s.userid 
                                    LEFT JOIN (SELECT f.id, CONCAT(u.firstname, ' ', u.lastname) as previous_advisor FROM {local_mxschool_faculty} f LEFT JOIN {user} u ON u.id = f.userid WHERE u.id > 0) pa ON pa.id = mas.currentadvisor
                                    LEFT JOIN (SELECT f.id, CONCAT(u.firstname, ' ', u.lastname) as current_advisor FROM {local_mxschool_faculty} f LEFT JOIN {user} u ON u.id = f.userid WHERE u.id > 0) ca ON ca.id = mas.finaladvisor
                                WHERE mas.finaladvisor > 0 AND mas.status = 0 AND s.id > 0 AND u.id > 0 AND u.deleted = 0");
    
    if (count($items) > 0){
        $msg = new mxNotifications(2);
        foreach ($items as $item){
            $params = array();
            $params['item'] = $item;
            $msg->rebuild(2, $params);
            $msg->process();
            $select = $DB->get_record('local_mxschool_advisors', array('id'=>$item->selectid));
            if ($select){
                $select->status = 1;
                $select->currentadvisor = $select->finaladvisor;
                $DB->update_record('local_mxschool_advisors', $select);
                
                $student = $DB->get_record('local_mxschool_students', array('id'=>$select->userid));
                if ($student){
                    $student->advisor = $select->finaladvisor;
                    $DB->update_record('local_mxschool_students', $student);
                }
            }            
        }
        $jAlert->create(array('type'=>'success', 'text'=>'Notifications was successfully sent'));
    } else {
        $jAlert->create(array('type'=>'warning', 'text'=>'No selected advisors to send notifications'));
    }
    
    redirect(new moodle_url("/local/mxschool/advisor_selection/manage.php"));
}

$title = get_string('manage_student_selections', 'local_mxschool');

$PAGE->set_url(new moodle_url("/local/mxschool/advisor_selection/manage.php", array()));
$PAGE->navbar->add(get_string('pluginname', 'local_mxschool'), new moodle_url('/local/mxschool/index.php'));
$PAGE->navbar->add(get_string('advisor_selection', 'local_mxschool'), new moodle_url('/local/mxschool/advisor_selection/index.php'));
$PAGE->navbar->add($title);
$PAGE->requires->jquery();
$PAGE->set_pagelayout('admin');
$PAGE->set_context(context_system::instance());
$PAGE->set_title($title);
$PAGE->set_heading($title);

$table = new manage_table('manage_table', $search, $filter);
$table->is_collapsible = false;
$table->is_downloading($download, 'Advisor_selection_'.date('m_d_Y'));

if (!$table->is_downloading()) {
    echo $OUTPUT->header();
    echo $OUTPUT->heading($title);
    echo html_writer::start_tag("form",  array("action"=> $PAGE->url, 'class'=>'mxschool-search-form'));
    echo html_writer::start_tag("label",  array());
    echo html_writer::start_tag('select', array('name' => 'filter'));
        echo html_writer::tag('option', 'Show all', array('value' => ''));
        $options = array('value' => 1); if($filter == 1) $options['selected'] = 'selected';
        echo html_writer::tag('option', 'With new advisor', $options);
        $options = array('value' => 2); if($filter == 2) $options['selected'] = 'selected';
        echo html_writer::tag('option', 'With old advisor', $options);
        $options = array('value' => 3); if($filter == 3) $options['selected'] = 'selected';
        echo html_writer::tag('option', 'Pending', $options);
        $options = array('value' => 4); if($filter == 4) $options['selected'] = 'selected';
        echo html_writer::tag('option', 'Choosen', $options);
        $options = array('value' => 5); if($filter == 5) $options['selected'] = 'selected';
        echo html_writer::tag('option', 'Finalized', $options);
    echo html_writer::end_tag("select");
    echo html_writer::end_tag("label");
    echo html_writer::start_tag("label",  array());
    echo html_writer::empty_tag('input', array('type' => 'text', 'name' => 'search', 'placeholder' => get_string('search').' ...', 'value' => $search));
    echo html_writer::empty_tag('input', array('type' => 'submit', 'value' => get_string('search')));
    echo html_writer::empty_tag('input', array('type' => 'button', 'value' => 'Email All selected', 'onclick'=>'location="'.$CFG->wwwroot.'/local/mxschool/advisor_selection/manage.php?action=send"', 'style'=>'float:left;'));
    echo html_writer::end_tag("label");
    echo html_writer::end_tag("form");

    echo html_writer::start_tag('div', array('class' => 'mxschool-table-box'));
}

$table->out(20, true);

if (!$table->is_downloading()) {
    echo html_writer::end_tag("div");

echo $OUTPUT->footer();

?>

<script>
    jQuery('.adv-select').change(function(){
        var adv = jQuery(this).val();
        var rid = jQuery(this).parent().parent().attr('id');
        jQuery(this).parent().parent().parent().find('.clone-select-box').hide();
        jQuery(this).parent().parent().parent().find('.adv-select-other').removeAttr('checked');
        var el = jQuery(this)
        jQuery.ajax({
          url: "<?php echo $CFG->wwwroot; ?>/local/mxschool/advisor_selection/ajax.php",
          type: 'POST',
          dataType: 'json',
          data: {
                    'rid' :rid,
                    'adv' :adv,
                    'action' :'set_final_advisor'
                },
          success: function(data) {
              el.parent().parent().append('<span class="saved">Saved</span>');
              removeSaved();
          }
        });
    });
    
    jQuery('.adv-select-other').change(function(){
        jQuery(this).parent().parent().find('.adv-select').removeAttr('checked');
        jQuery(this).parent().parent().parent().find('.clone-select-box').show();
    });
    
    jQuery('.other-adv-select').change(function(){
        var adv = jQuery(this).val();
        var title = jQuery(this).find('option:selected').text();
        var rid = jQuery(this).attr('id');
        var dataid = jQuery(this).attr('data-id');
        var el = jQuery(this);
        if (adv != ''){
            jQuery.ajax({
              url: "<?php echo $CFG->wwwroot; ?>/local/mxschool/advisor_selection/ajax.php",
              type: 'POST',
              dataType: 'json',
              data: {
                        'rid' :rid,
                        'adv' :adv,
                        'action' :'set_final_advisor_other'
                    },
              success: function(data) {
              }
            });
            var radio = '<label class="adv-select-div"><input class="adv-select" type="radio" checked="checked" name="final-adv-'+dataid+'" value="'+adv+'" checked onclick="select_advisor('+adv+', \'other-adv-select-'+dataid+'\', '+dataid+');">'+title+'</label>';
            jQuery('#final-adv-'+dataid+' .adv-select-other').removeAttr('checked');
            jQuery('#final-adv-'+dataid+' .adv-select-other').parent().before(radio);
            jQuery('#final-adv-'+dataid+' .clone-select-box').hide();
            jQuery('#final-adv-'+dataid+' .clone-select-box select option:selected').hide();
            jQuery('#final-adv-'+dataid+' .clone-select-box select').val('');
        }
    });
    
    function select_advisor(adv, rid, dataid){
        jQuery.ajax({
          url: "<?php echo $CFG->wwwroot; ?>/local/mxschool/advisor_selection/ajax.php",
          type: 'POST',
          dataType: 'json',
          data: {
                    'rid' :rid,
                    'adv' :adv,
                    'action' :'set_final_advisor_other'
                },
          success: function(data) {
              jQuery('#final-adv-'+dataid+' .adv-select-other').parent().parent().append('<span class="saved">Saved</span>');
              removeSaved();
          }
        });
        jQuery('#final-adv-'+dataid+' .adv-select-other').removeAttr('checked');
        jQuery('#final-adv-'+dataid+' .adv-select-other').find('.clone-select-box').hide();
    }
    
    function removeSaved(){
        setTimeout(function () {
          $('span.saved').fadeOut(function() {
            $(this).remove();
          });
        }, 2000);   
    }
</script>

<?php
}
