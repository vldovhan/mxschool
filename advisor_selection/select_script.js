$(document).ready(function(){
    jQuery('#id_submitbutton').prop('disabled', true);
    jQuery('#fitem_id_currentadvisor').hide();
    jQuery('#fgroup_id_radioar').hide();
    jQuery('.advisor-text').hide();
    jQuery('#fitem_id_advisor1').hide();
    jQuery('#fitem_id_advisor2').hide();
    jQuery('#fitem_id_advisor3').hide();
    jQuery('#fitem_id_advisor4').hide();
    jQuery('#fitem_id_advisor5').hide();
    jQuery('.mform .student').change(function(){
        $('#id_advisor1').val('0');
        $('#id_advisor2').val('0');
        $('#id_advisor3').val('0');
        $('#id_advisor4').val('0');
        $('#id_advisor5').val('0');
        jQuery('.advisor-text').hide();
        jQuery('#fitem_id_advisor1').hide();
        jQuery('#fitem_id_advisor2').hide();
        jQuery('#fitem_id_advisor3').hide();
        jQuery('#fitem_id_advisor4').hide();
        jQuery('#fitem_id_advisor5').hide();

        var id = jQuery(this).val();
        if (id != ''){
            $.ajax({
              url: "ajax.php",
              type: 'POST',
              dataType: 'json',
              data: {
                        'id' :id,
                        'action' :'get_current_advisor'
                    },
              success: function(data) {
                var advisor = data.advisor_name;
                var advisor_id = data.advisor_id;
                var available = data.available;
                // Show current advisor name in chosen advisor text input.
                $('.mform .current-advisor').val(advisor);
                $('.currentadvisor_id').val(advisor_id);

                jQuery('#fitem_id_currentadvisor').show();
                jQuery('#fgroup_id_radioar').show();

                if (advisor == '' || available == 0){
                    $('#id_keep_current_advisor_no').trigger('click');
                }
                disable_enable_advisors();
              }
            });
        } else {
            jQuery('#fitem_id_currentadvisor').hide();
            jQuery('#id_currentadvisor').val();
            jQuery('.currentadvisor_id').val(0);
            jQuery('#fgroup_id_radioar').hide();
            jQuery('.advisor-text').hide();
            jQuery('#fitem_id_advisor1').hide();
            jQuery('#fitem_id_advisor2').hide();
            jQuery('#fitem_id_advisor3').hide();
            jQuery('#fitem_id_advisor4').hide();
            jQuery('#fitem_id_advisor5').hide();
            jQuery('#id_submitbutton').prop('disabled', true);
            disable_enable_advisors();
        }
    });

    $('#id_keep_current_advisor_yes').click(function(){
        jQuery('.advisor-text').hide();
        jQuery('#fitem_id_advisor1').hide();
        jQuery('#fitem_id_advisor2').hide();
        jQuery('#fitem_id_advisor3').hide();
        jQuery('#fitem_id_advisor4').hide();
        jQuery('#fitem_id_advisor5').hide();
        jQuery('#id_submitbutton').removeAttr('disabled');
    });

    $('#id_keep_current_advisor_no').click(function(){
        jQuery('#id_submitbutton').prop('disabled', true);
        jQuery('.advisor-text').show();
        jQuery('#fitem_id_advisor1').show();
        if ($('#id_advisor2').val() != '0'){
            $('#fitem_id_advisor2').show();
        }
        if ($('#id_advisor3').val() != '0'){
            $('#fitem_id_advisor3').show();
        }
        if ($('#id_advisor4').val() != '0'){
            $('#fitem_id_advisor4').show();
        }
        if ($('#id_advisor5').val() != '0'){
            $('#fitem_id_advisor5').show();
        }
        chech_valid();
    });

    $('#id_advisor1').change(function(){
        var cur_advisor_id = $('.currentadvisor_id').val();
        if (jQuery(this).val() != '0'){
            jQuery('#fitem_id_advisor2').show();
        }
        disable_enable_advisors();
        chech_valid();
    });

    $('#id_advisor2').change(function(){
        var cur_advisor_id = $('.currentadvisor_id').val();
        if (jQuery(this).val() != '0' && (cur_advisor_id == '0' || (cur_advisor_id > 0 && cur_advisor_id != jQuery(this).val()))){
            jQuery('#fitem_id_advisor3').show();
        }
        disable_enable_advisors();
        chech_valid();
    });

    $('#id_advisor3').change(function(){
        var cur_advisor_id = $('.currentadvisor_id').val();
        if (jQuery(this).val() != '0' && (cur_advisor_id == '0' || (cur_advisor_id > 0 && cur_advisor_id != jQuery(this).val()))){
            jQuery('#fitem_id_advisor4').show();
        }
        disable_enable_advisors();
        chech_valid();
    });

    $('#id_advisor4').change(function(){
        var cur_advisor_id = $('.currentadvisor_id').val();
        if (jQuery(this).val() != '0' && (cur_advisor_id == '0' || (cur_advisor_id > 0 && cur_advisor_id != jQuery(this).val()))){
            jQuery('#fitem_id_advisor5').show();
        }
        disable_enable_advisors();
        chech_valid();
    });

    $('#id_advisor5').change(function(){
        disable_enable_advisors();
        chech_valid();
    });

});

function chech_valid(){
    var cur_advisor_id = $('.currentadvisor_id').val();
    if (cur_advisor_id > 0){
        if ($('#id_advisor1').val() == cur_advisor_id ||
            $('#id_advisor2').val() == cur_advisor_id ||
            $('#id_advisor3').val() == cur_advisor_id ||
            $('#id_advisor4').val() == cur_advisor_id ||
            $('#id_advisor4').val() == cur_advisor_id ){
            jQuery('#id_submitbutton').removeAttr('disabled');        
        } else if ($('#id_advisor1').val() == '0' || 
            $('#id_advisor2').val() == '0' || 
            $('#id_advisor3').val() == '0' || 
            $('#id_advisor4').val() == '0' || 
            $('#id_advisor5').val() == '0'){
            jQuery('#id_submitbutton').prop('disabled', true);
        } else {
            jQuery('#id_submitbutton').removeAttr('disabled');
        }
    } else {
        if ($('#id_advisor1').val() == '0' || 
            $('#id_advisor2').val() == '0' || 
            $('#id_advisor3').val() == '0' || 
            $('#id_advisor4').val() == '0' || 
            $('#id_advisor5').val() == '0'){
            jQuery('#id_submitbutton').prop('disabled', true);
        } else {
            jQuery('#id_submitbutton').removeAttr('disabled');
        }
    }
}

function disable_enable_advisors(theclass) {
  var cur_advisor_id = $('.currentadvisor_id').val();
  var advisor1_id = $('#id_advisor1').find(":selected").val();
  var advisor2_id = $('#id_advisor2').find(":selected").val();
  var advisor3_id = $('#id_advisor3').find(":selected").val();  
  var advisor4_id = $('#id_advisor4').find(":selected").val();
  var advisor5_id = $('#id_advisor5').find(":selected").val();
    // Remove all disabled attributes for options in advisor1 select.
    $("#id_advisor1 option").removeAttr("disabled").show();
    // Remove all disabled attributes for options in advisor2 select.
    $("#id_advisor2 option").removeAttr("disabled").show();
    // Remove all disabled attributes for options in advisor3 select.
    $("#id_advisor3 option").removeAttr("disabled").show();
    // Remove all disabled attributes for options in advisor4 select.
    $("#id_advisor4 option").removeAttr("disabled").show();
    // Remove all disabled attributes for options in advisor5 select.
    $("#id_advisor5 option").removeAttr("disabled").show();

    // Disable just the option for current advisor in advisor choice 1.
    $("#id_advisor1 option[value='" + cur_advisor_id + "']").attr("disabled", "disabled").hide();
    /*$("#id_advisor2 option[value='" + cur_advisor_id + "']").attr("disabled", "disabled").hide();
    $("#id_advisor3 option[value='" + cur_advisor_id + "']").attr("disabled", "disabled").hide();
    $("#id_advisor4 option[value='" + cur_advisor_id + "']").attr("disabled", "disabled").hide();
    $("#id_advisor5 option[value='" + cur_advisor_id + "']").attr("disabled", "disabled").hide();*/
    
    if (advisor1_id != '0'){
        $("#id_advisor1 option[value='" + advisor1_id + "']").attr("disabled", "disabled").hide();
        $("#id_advisor2 option[value='" + advisor1_id + "']").attr("disabled", "disabled").hide();
        $("#id_advisor3 option[value='" + advisor1_id + "']").attr("disabled", "disabled").hide();
        $("#id_advisor4 option[value='" + advisor1_id + "']").attr("disabled", "disabled").hide();
        $("#id_advisor5 option[value='" + advisor1_id + "']").attr("disabled", "disabled").hide();      
    }

    if (advisor2_id != '0'){
        $("#id_advisor1 option[value='" + advisor2_id + "']").attr("disabled", "disabled").hide();
        $("#id_advisor2 option[value='" + advisor2_id + "']").attr("disabled", "disabled").hide();
        $("#id_advisor3 option[value='" + advisor2_id + "']").attr("disabled", "disabled").hide();
        $("#id_advisor4 option[value='" + advisor2_id + "']").attr("disabled", "disabled").hide();
        $("#id_advisor5 option[value='" + advisor2_id + "']").attr("disabled", "disabled").hide();
    }

    if (advisor3_id != '0'){
        $("#id_advisor1 option[value='" + advisor3_id + "']").attr("disabled", "disabled").hide();
        $("#id_advisor2 option[value='" + advisor3_id + "']").attr("disabled", "disabled").hide();
        $("#id_advisor3 option[value='" + advisor3_id + "']").attr("disabled", "disabled").hide();
        $("#id_advisor4 option[value='" + advisor3_id + "']").attr("disabled", "disabled").hide();
        $("#id_advisor5 option[value='" + advisor3_id + "']").attr("disabled", "disabled").hide();
    }

    if (advisor4_id != '0'){
        $("#id_advisor1 option[value='" + advisor4_id + "']").attr("disabled", "disabled").hide();
        $("#id_advisor2 option[value='" + advisor4_id + "']").attr("disabled", "disabled").hide();
        $("#id_advisor3 option[value='" + advisor4_id + "']").attr("disabled", "disabled").hide();
        $("#id_advisor4 option[value='" + advisor4_id + "']").attr("disabled", "disabled").hide();
        $("#id_advisor5 option[value='" + advisor4_id + "']").attr("disabled", "disabled").hide();
    }
    
    if (advisor5_id != '0'){
        $("#id_advisor1 option[value='" + advisor5_id + "']").attr("disabled", "disabled").hide();
        $("#id_advisor2 option[value='" + advisor5_id + "']").attr("disabled", "disabled").hide();
        $("#id_advisor3 option[value='" + advisor5_id + "']").attr("disabled", "disabled").hide();
        $("#id_advisor4 option[value='" + advisor5_id + "']").attr("disabled", "disabled").hide();
        $("#id_advisor5 option[value='" + advisor5_id + "']").attr("disabled", "disabled").hide();
    }

    $('#id_advisor1').find(":selected").removeAttr("disabled").show();
    $('#id_advisor2').find(":selected").removeAttr("disabled").show();
    $('#id_advisor3').find(":selected").removeAttr("disabled").show();
    $('#id_advisor4').find(":selected").removeAttr("disabled").show();
    $('#id_advisor5').find(":selected").removeAttr("disabled").show();

}