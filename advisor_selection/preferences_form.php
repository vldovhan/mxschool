<?php

defined('MOODLE_INTERNAL') || die;

require_once($CFG->libdir.'/formslib.php');

/**
 * The form for handling editing a course.
 */
class edit_form extends moodleform {
    protected $id;
    protected $context;
    
    /**
     * Form definition.
     */
    function definition() {
        global $CFG, $PAGE;

        $mform          = $this->_form;
        $settings       = $this->_customdata['settings'];
        $advisors       = $this->_customdata['advisors'];
        
        $systemcontext   = context_system::instance();
        $this->context = $systemcontext;
        
        $radioarray=array();
        $radioarray[] = $mform->createElement('radio', 'advisor_form_enable', '', get_string('second_semester_starts', 'local_mxschool').((isset($settings['second_semester_starts']) and $settings['second_semester_starts'] > 0) ? ' ('.date('m/d/Y', $settings['second_semester_starts']).')' : ''), 'second_semester_starts');
        $radioarray[] = $mform->createElement('radio', 'advisor_form_enable', '', get_string('dorms_close', 'local_mxschool').((isset($settings['dorms_close']) and $settings['dorms_close'] > 0) ? ' ('.date('m/d/Y', $settings['dorms_close']).')' : ''), 'dorms_close');
        $mform->addGroup($radioarray, 'radioar', get_string('advisor_form_enable', 'local_mxschool'), array(''), false);
        $mform->addRule('radioar', get_string('required_field', 'local_mxschool'), 'required', null, 'client');

        $options = array(''=>'----------', '-1 day'=>'1 day', '-2 days'=>'2 days', '-3 days'=>'3 days', '-4 days'=>'4 days', '-5 days'=>'5 days', '-6 days'=>'6 days', '-1 week'=>'1 week', '-2 weeks'=>'2 weeks', '-3 weeks'=>'4 weeks' );
        $mform->addElement('select', 'advisor_form_time_prior', get_string('time_prior', 'local_mxschool'), $options);
        $mform->addRule('advisor_form_time_prior', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        
        $mform->addElement('checkbox', 'reset_records', get_string('reset_student_records', 'local_mxschool'));
        
        /*$editoroptions = array('maxfiles' => 0, 'maxbytes'=>$CFG->maxbytes, 'trusttext'=>false, 'noclean'=>true, 'context'=>$systemcontext, 'subdirs'=>0);
        
        $mform->addElement('editor', 'advisor_form_message', get_string('advisor_message', 'local_mxschool'), null, $editoroptions);
        $mform->getElement('advisor_form_message')->setValue(array('text' => $settings['advisor_form_message']));
        $mform->setType('advisor_form_message', PARAM_RAW);*/
        
        $mform->addElement('html', '<h3 class="form-label-h3">'.get_string('advisor_availability', 'local_mxschool').'</h3>');
        
        $mform->addElement('html', '<div class="advisors-box clearfix">');
        if (count($advisors) > 0){
            foreach ($advisors as $advisor){
                $mform->addElement('checkbox', 'advisor_'.$advisor->id, $advisor->username, '', array('class'=>'onoff-switcher'));
                if ($advisor->available > 0){
                    $mform->setDefault('advisor_'.$advisor->id, true);
                }
            }
        }
        $mform->addElement('html', '</div>');
        
        $this->add_action_buttons(get_string('cancel'), get_string('save', 'local_mxschool'));
        
        // Finally set the current form data
        $this->set_data($settings);
    }

    /**
     * Fill in the current page data for this course.
     */
    function definition_after_data() {
        global $DB;

        $mform = $this->_form;

    }

    /**
     * Validation.
     *
     * @param array $data
     * @param array $files
     * @return array the errors that were found
     */
    function validation($data, $files) {
        global $DB;

        $errors = parent::validation($data, $files);

        return $errors;
    }
}

