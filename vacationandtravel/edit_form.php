<?php

defined('MOODLE_INTERNAL') || die;

require_once($CFG->libdir.'/formslib.php');

/**
 * The form for handling editing a course.
 */
class edit_form extends moodleform {
    protected $id;
    protected $context;
    
    /**
     * Form definition.
     */
    function definition() {
        global $CFG, $PAGE, $DB;

        $mform          = $this->_form;
        $type           = $this->_customdata['type'];
        $record         = $this->_customdata['record'];
        $students       = $this->_customdata['students'];
        $dorm           = $this->_customdata['dorm'];
        $sites          = $this->_customdata['sites'];
        
        $systemcontext   = context_system::instance();
        $this->context = $systemcontext;
        $strgeneral  = get_string('general');
        
        $mform->addElement('hidden', 'id');
        
        $students_options = array(''=>'----------');
        if (count($students)){
            foreach ($students as $student){
                $students_options[$student->id] = $student->lastname.', '.$student->firstname;
            }
        }
        
        $mform->addElement('header', 'moodle', $strgeneral);
        
        $mform->addElement('select', 'studentid', get_string('studentname', 'local_mxschool'), $students_options);
        $mform->addRule('studentid', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        $mform->setType('studentid', PARAM_INT);
        
        $mform->addElement('text', 'dorm', get_string('dorm', 'local_mxschool'), array('disabled'=>'disabled'));
        $mform->setType('dorm', PARAM_RAW);
        if (isset($dorm->name)){
            $mform->setDefault('dorm', $dorm->name);
        }
        
        $mform->addElement('text', 'destination', get_string('destination', 'local_mxschool'));
        $mform->setType('destination', PARAM_RAW);
        $mform->addRule('destination', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        
        $mform->addElement('text', 'stu_cellphone', get_string('stu_cellphone', 'local_mxschool'), array('class'=>'_mask-input'));
        $mform->setType('stu_cellphone', PARAM_RAW);
        $mform->addRule('stu_cellphone', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        
        $times = array('startyear' => date('Y',strtotime('-6 month',get_config('local_mxschool','second_semester_starts'))),'stopyear'  => date('Y',get_config('local_mxschool','second_semester_starts')));
        // departure box
        $mform->addElement('header', 'moodle', get_string('departure', 'local_mxschool'));
        
        $mform->addElement('date_time_selector', 'depart_date_time', get_string('departure_date_time', 'local_mxschool'),$times);
        $mform->addRule('depart_date_time', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        
        $radioarray=array();
        $radioarray[] = $mform->createElement('radio', 'depart_transportation', '', get_string('yes'), 'yes');
        $radioarray[] = $mform->createElement('radio', 'depart_transportation', '', get_string('no'), 'no');
        $mform->addGroup($radioarray, 'depart_tr_radioar', get_string('transportation_needed', 'local_mxschool'), array(''), false);
        $mform->addRule('depart_tr_radioar', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        
        $radioarray=array();
        $radioarray[] = $mform->createElement('radio', 'depart_transport_type', '', 'Car', '1');
        $radioarray[] = $mform->createElement('radio', 'depart_transport_type', '', 'Plane', '2');
        $radioarray[] = $mform->createElement('radio', 'depart_transport_type', '', 'Train', '3');
        $radioarray[] = $mform->createElement('radio', 'depart_transport_type', '', '<span>Non MX</span> Bus', '4');
        $radioarray[] = $mform->createElement('radio', 'depart_transport_type', '', 'MX NYC Direct Bus', '5');
        $mform->addGroup($radioarray, 'depart_tr_type', get_string('transportation_type', 'local_mxschool'), array(''), false);
        $mform->addRule('depart_tr_type', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        
        $mform->addElement('text', 'depart_driver', 'Driver(s)');
        $mform->setType('depart_driver', PARAM_RAW);
        
        $radioarray=array();
        if (count($sites[1]['plane']) > 0){
            foreach($sites[1]['plane'] as $sid=>$sname){
                $radioarray[] = $mform->createElement('radio', 'depart_airport', '', $sname, $sid);       
            }
        }
        $mform->addGroup($radioarray, 'depart_airport_array', 'Departure Airport', array(''), false);
        
        $mform->addElement('text', 'depart_carrier', 'Carrier');
        $mform->setType('depart_carrier', PARAM_RAW);
        
        $mform->addElement('text', 'depart_flight', 'Flight #');
        $mform->setType('depart_flight', PARAM_RAW);
        
        $radioarray=array();
        $radioarray[] = $mform->createElement('radio', 'depart_customs', '', 'Yes', '1');
        $radioarray[] = $mform->createElement('radio', 'depart_customs', '', 'No', '2');
        $mform->addGroup($radioarray, 'depart_customs_array', 'Clearing Customs in Boston?', array(''), false);
        
        $radioarray=array();
        if (count($sites[1]['train']) > 0){
            foreach($sites[1]['train'] as $sid=>$sname){
                $radioarray[] = $mform->createElement('radio', 'depart_train', '', $sname, $sid);   
            }
        }
        $radioarray[] = $mform->createElement('radio', 'depart_train', '', 'Other', '0');
        $mform->addGroup($radioarray, 'depart_train_array', 'Departure Train station', array(''), false);
        
        $mform->addElement('text', 'depart_train_other', 'Other');
        $mform->setType('depart_train_other', PARAM_RAW);
        
        $radioarray=array();
        if (count($sites[1]['bus']) > 0){
            foreach($sites[1]['bus'] as $sid=>$sname){
                $radioarray[] = $mform->createElement('radio', 'depart_bus', '', $sname, $sid);   
            }
        }
        $radioarray[] = $mform->createElement('radio', 'depart_bus', '', 'Other', '0');
        $mform->addGroup($radioarray, 'depart_bus_array', 'Departure Bus station', array(''), false);
        
        $mform->addElement('text', 'depart_bus_other', 'Other');
        $mform->setType('depart_bus_other', PARAM_RAW);
        
        $radioarray=array();
        if (count($sites[1]['nyc_direct']) > 0){
            foreach($sites[1]['nyc_direct'] as $sid=>$sname){
                $radioarray[] = $mform->createElement('radio', 'depart_nyc_stops', '', $sname, $sid);   
            }
        }
        $mform->addGroup($radioarray, 'depart_nyc_stops_array', 'Departure NYC stop', array(''), false);
        
        // return box
        $mform->addElement('header', 'moodle', get_string('return', 'local_mxschool'));
        
        $mform->addElement('date_time_selector', 'return_date_time', get_string('return_date_time', 'local_mxschool'),$times);
        $mform->addRule('return_date_time', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        
        $radioarray=array();
        $radioarray[] = $mform->createElement('radio', 'return_transportation', '', get_string('yes'), 'yes');
        $radioarray[] = $mform->createElement('radio', 'return_transportation', '', get_string('no'), 'no');
        $mform->addGroup($radioarray, 'return_tr_radioar', get_string('transportation_needed', 'local_mxschool'), array(''), false);
        $mform->addRule('return_tr_radioar', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        
        $radioarray=array();
        $radioarray[] = $mform->createElement('radio', 'return_transport_type', '', 'Car', '1');
        $radioarray[] = $mform->createElement('radio', 'return_transport_type', '', 'Plane', '2');
        $radioarray[] = $mform->createElement('radio', 'return_transport_type', '', 'Train', '3');
        $radioarray[] = $mform->createElement('radio', 'return_transport_type', '', '<span>Non MX</span> Bus', '4');
        $radioarray[] = $mform->createElement('radio', 'return_transport_type', '', 'MX NYC Direct Bus', '5');
        $mform->addGroup($radioarray, 'return_tr_type', get_string('transportation_type', 'local_mxschool'), array(''), false);
        $mform->addRule('return_tr_type', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        
        $mform->addElement('text', 'return_driver', 'Driver(s)');
        $mform->setType('return_driver', PARAM_RAW);
        
        $radioarray=array();
        if (count($sites[2]['plane']) > 0){
            foreach($sites[2]['plane'] as $sid=>$sname){
                $radioarray[] = $mform->createElement('radio', 'return_airport', '', $sname, $sid);   
            }
        }
        $mform->addGroup($radioarray, 'return_airport_array', 'Return Airport', array(''), false);
        
        $mform->addElement('text', 'return_carrier', 'Carrier');
        $mform->setType('return_carrier', PARAM_RAW);
        
        $mform->addElement('text', 'return_flight', 'Flight #');
        $mform->setType('return_flight', PARAM_RAW);
        
        $radioarray=array();
        $radioarray[] = $mform->createElement('radio', 'return_customs', '', 'Yes', '1');
        $radioarray[] = $mform->createElement('radio', 'return_customs', '', 'No', '2');
        $mform->addGroup($radioarray, 'return_customs_array', 'Clearing Customs in Boston?', array(''), false);
        
        $radioarray=array();
        if (count($sites[2]['train']) > 0){
            foreach($sites[2]['train'] as $sid=>$sname){
                $radioarray[] = $mform->createElement('radio', 'return_train', '', $sname, $sid);   
            }
        }
        $radioarray[] = $mform->createElement('radio', 'return_train', '', 'Other', '0');
        $mform->addGroup($radioarray, 'return_train_array', 'Return Train station', array(''), false);
        
        $mform->addElement('text', 'return_train_other', 'Other');
        $mform->setType('return_train_other', PARAM_RAW);
        
        $radioarray=array();
        if (count($sites[2]['bus']) > 0){
            foreach($sites[2]['bus'] as $sid=>$sname){
                $radioarray[] = $mform->createElement('radio', 'return_bus', '', $sname, $sid);   
            }
        }
        $radioarray[] = $mform->createElement('radio', 'return_bus', '', 'Other', '0');
        $mform->addGroup($radioarray, 'return_bus_array', 'Return Bus station', array(''), false);
        
        $mform->addElement('text', 'return_bus_other', 'Other');
        $mform->setType('return_bus_other', PARAM_RAW);
        
        $radioarray=array();
        if (count($sites[2]['nyc_direct']) > 0){
            foreach($sites[2]['nyc_direct'] as $sid=>$sname){
                $radioarray[] = $mform->createElement('radio', 'return_nyc_stops', '', $sname, $sid);   
            }
        }
        $mform->addGroup($radioarray, 'return_nyc_stops_array', 'Return NYC stop', array(''), false);
        
        
        // buttons
        $this->add_action_buttons(get_string('cancel'), get_string('submit'));
        
        // Finally set the current form data
        $this->set_data($record);
    }

    /**
     * Fill in the current page data for this course.
     */
    function definition_after_data() {
        global $DB;

        $mform = $this->_form;

    }

    /**
     * Validation.
     *
     * @param array $data
     * @param array $files
     * @return array the errors that were found
     */
    function validation($data, $files) {
        global $DB;

        $errors = parent::validation($data, $files);
        $data = (object)$data;
        
        if ($data->depart_date_time >= $data->return_date_time){
            $errors['return_date_time'] = 'Invalid Return Date & Time';
        }

        return $errors;
    }
}

