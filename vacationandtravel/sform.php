<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     mxschool
 * @copyright  2016 mxschool.edu
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */


require('../../../config.php');
require_once('edit_form.php');
require_once('../lib.php');
require_once($CFG->dirroot."/local/mxschool/classes/alerts/alerts.php");

$id         = optional_param('id', 0, PARAM_INT);
$type       = optional_param('type', 0, PARAM_INT);

require_login();

$title = get_string('vacationandtravel_form', 'local_mxschool');

$PAGE->set_url(new moodle_url("/local/mxschool/vacationandtravel/form.php", array()));
$PAGE->navbar->add(get_string('pluginname', 'local_mxschool'), new moodle_url('/local/mxschool/index.php'));
$PAGE->navbar->add(get_string('vacationandtravel', 'local_mxschool'), new moodle_url('/local/mxschool/vacationandtravel/index.php'));
$PAGE->navbar->add(get_string('vacationandtravel_form', 'local_mxschool'), new moodle_url('/local/mxschool/vacationandtravel/fotm.php'));

$PAGE->navbar->add($title);
$PAGE->requires->jquery();
$PAGE->requires->js('/local/mxschool/assets/js/jquery.maskedinput.min.js', true);
$PAGE->requires->js('/local/mxschool/assets/js/script.js', true);
$PAGE->requires->js('/local/mxschool/vacationandtravel/form_script.js', true);
$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('admin');
$PAGE->set_title($title);
$PAGE->set_heading($title);

$record = $DB->get_record('local_mxschool_transport', array('id'=>$id));
$student = null; $dorm = null;

if (isset($record->studentid)){
    $student = $DB->get_record('local_mxschool_students', array('id'=>$record->studentid));
} else {
    $student = $DB->get_record('local_mxschool_students', array('userid'=>$USER->id));
}

if (!$student or $student->userid != $USER->id){
    print_error('You have not permissions to visit this page!');
} else {
    $record->studentid = $student->id;
    $dorm = $DB->get_record("local_mxschool_dorms", array('abbreviation'=>$student->dorm));
    $user = $DB->get_record('user', array('id'=>$student->userid));
}

$sites = getSites();
$students = $DB->get_records_sql("SELECT s.id, s.userid, u.firstname, u.lastname, d.name as dormname
                         FROM {local_mxschool_students} s
                            LEFT JOIN {user} u ON u.id = s.userid
                            LEFT JOIN {local_mxschool_dorms} d ON d.abbreviation = s.dorm
                                WHERE u.id > 0 AND u.deleted = 0
                         ORDER BY u.lastname, u.firstname, s.middle ASC");

$editform = new edit_form(null, array('record'=>$record, 'students'=>$students, 'dorm'=>$dorm, 'student'=>$student, 'sites'=>$sites));

if ($editform->is_cancelled()) {
    // The form has been cancelled, take them back to what ever the return to is.
    redirect(new moodle_url('/local/mxschool/vacationandtravel/records.php'));
} else if ($data = $editform->get_data()) {
    
    $new_record = $data;
    $new_record->timemodified = time();
    
    if ($data->id > 0){
        $new_record->id = $data->id;
        $DB->update_record('local_mxschool_transport', $data);   
    } else {
        $new_record->status = 0;
        $new_record->timecreated = time();
        $DB->insert_record('local_mxschool_transport', $data);   
    }
    
    require('../classes/notifications.php');
    
    $student = $DB->get_record('local_mxschool_students', array('id'=>$data->studentid));
    $user = $DB->get_record('user', array('id'=>$student->userid));
    
    $params = array();
    $item = new stdClass();
    $item->student = fullname($user);
    $params['item'] = $item;
    
    $msg = new mxNotifications(3, $params);
    $msg->process();
    
    $jAlert->create(array('type'=>'success', 'text'=>get_string('vacationandtravel_form', 'local_mxschool').' was successfully saved'));
    redirect(new moodle_url('/local/mxschool/vacationandtravel/records.php'));
    
}

// Print the form.

$PAGE->set_context(context_system::instance());
$PAGE->set_title($title);
$PAGE->set_heading($title);

echo $OUTPUT->header();
echo $OUTPUT->heading($title);

echo html_writer::start_tag('div', array('class' => 'mx-transportation-form st-form'));
$editform->display();
echo html_writer::end_tag('div');

echo $OUTPUT->footer();

