<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/tablelib.php');

class return_table extends table_sql {
    
    public $_transport_types = array(''=>'', '1'=>'Car', '2'=>'Plane', '3'=>'Train', '4'=>'Bus', '5'=>'NYC Direct');
    public $_trans_types = array(''=>'----', 'car'=>'Car', 'van'=>'Van', 'school transport'=>'School Transport', 'none'=>'None Needed');
    public $_sites = array();
    public $_pickup_sites = array();
    public $_pickup_times = array();
    
    function __construct($uniqueid, $search, $filter, $download) {
		global $CFG, $USER, $DB;

        parent::__construct($uniqueid);
        
        $this->set_sites();
        $this->set_pickup_sites();
        $this->set_pickup_times();
        
        $columns = array('student', 'destination', 'stu_cellphone', 'return_date_time', 'return_transport_type', 'return_driver', 'return_details', 'trans_type_return', 'pickup_site_return', 'pickup_time_return', 'confirmed_return', 'email_sendable_return', 'email_sent_return');
        $headers = array(
            get_string('student', 'local_mxschool'),
            get_string('destination', 'local_mxschool'),
            get_string('stu_cellphone', 'local_mxschool'),
            'Date & Time',
            get_string('transportation_type', 'local_mxschool'),
            'Airport / Station',
            'Details',
            'Trans Type',
            'Pickup Site',
            'Pickup Time',
            'Confirmed',
            'Email',
            'Sent'
        );
        
        if ($download == ''){
            $columns[] = 'actions';
            $headers[] = 'Actions';
            $this->no_sorting('actions');
        }
        $this->no_sorting('return_driver');
        $this->no_sorting('return_details');

        $this->define_columns($columns);
        $this->define_headers($headers);
        
        $sql_search = ($search) ? " AND (t.destination LIKE '%$search%' OR t.return_driver LIKE '%$search%' OR u.firstname LIKE '%$search%' OR u.lastname LIKE '%$search%' OR u.email LIKE '%$search%' OR sa.site LIKE '%$search%' OR st.site LIKE '%$search%' OR sb.site LIKE '%$search%')" : "";
        
        if ($filter['start'] != '' or $filter['end'] != ''){
            $start = strtotime($filter['start']);
            $end = strtotime($filter['end']);
            if ($filter['start'] != '' && $filter['end'] != ''){
                $sql_search .= " AND t.return_date_time BETWEEN $start AND $end";
            } elseif ($filter['start'] != ''){
                $sql_search .= " AND t.return_date_time > $start";
            } elseif ($filter['end'] != ''){
                $sql_search .= " AND t.return_date_time < $end";
            }
        }
        if ($filter['status'] != ''){
            if ($filter['status'] == 'pending'){
                $sql_search .= " AND (t.confirmed_return = 2 OR t.confirmed_return IS NULL)";
            } elseif($filter['status'] == 'confirmed'){
                $sql_search .= " AND t.confirmed_return = 1";
            }
        }
        
        $fields = "t.*, CONCAT(u.firstname, ' ', u.lastname) as student";
        $from = "{local_mxschool_transport} t
                    LEFT JOIN {local_mxschool_students} s ON s.id = t.studentid
                    LEFT JOIN {user} u ON u.id = s.userid
                    LEFT JOIN {local_mxschool_pickup_sites} sa ON sa.id = t.return_airport 
                    LEFT JOIN {local_mxschool_pickup_sites} st ON st.id = t.return_train 
                    LEFT JOIN {local_mxschool_pickup_sites} sb ON sb.id = t.return_bus ";
        
        $where = 't.id > 0 AND u.deleted = 0'.$sql_search;
        $this->set_sql($fields, $from, $where, array());
        $this->define_baseurl($CFG->wwwroot.$_SERVER['REQUEST_URI']);
    }
    
    function col_return_date_time($values) {
        if ($this->is_downloading()){
            $date = date('m/d/Y h:i A', $values->return_date_time);
        } else {
            $date = date('m/d/Y', $values->return_date_time);
            $date .= '<br />'.date('h:i A', $values->return_date_time);   
        }
      return $date;
    }
    
    function col_return_transport_type($values) {
      return $this->_transport_types[$values->return_transport_type];
    }
    
    function col_return_driver($values) {
        $data = '-';
        if ($values->return_transport_type == '2' and isset($this->_sites[$values->return_airport])){
            $data = $this->_sites[$values->return_airport];
        } elseif ($values->return_transport_type == '3'){
            if ($values->return_train != '0' and isset($this->_sites[$values->return_train])){
                $data = $this->_sites[$values->return_train];
            } elseif ($values->return_train == '0'){
                $data = 'Other';
            }
        } elseif ($values->return_transport_type == '4'){
            if ($values->return_bus != '0' and isset($this->_sites[$values->return_bus])){
                $data = $this->_sites[$values->return_bus];
            } elseif ($values->return_bus == '0'){
                $data = 'Other';
            }
        }
      return $data;
    }
    
    function col_return_details($values) {
        $data = '-';
        if ($values->return_transport_type == '1'){
            if ($this->is_downloading()){
                $data = 'Driver(s): '.$values->return_driver;   
            } else {
                $data = '<div class="tr_details"><label>Driver(s):</label>'.$values->return_driver.'</div>';
            }
        } elseif ($values->return_transport_type == '2'){
            if ($this->is_downloading()){
                $data = 'Carrier: '.$values->return_carrier;   
                $data .= '; Flight #: '.$values->return_flight;   
                $data .= '; Customs?: '.(($values->return_customs == 1) ? 'Yes' : 'No');   
            } else {
                $data = '<div class="tr_details"><label>Carrier:</label>'.$values->return_carrier.'</div>';
                $data .= '<div class="tr_details"><label>Flight #:</label>'.$values->return_flight.'</div>';
                $data .= '<div class="tr_details"><label>Customs?:</label>'.(($values->return_customs == 1) ? 'Yes' : 'No').'</div>';
            }
        } elseif ($values->return_transport_type == '3'){
            if ($values->return_train_other != ''){
                if ($this->is_downloading()){
                    $data = $values->return_train_other;
                } else {
                    $data = '<div class="tr_details">'.$values->return_train_other.'</div>';
                }
            }
        } elseif ($values->return_transport_type == '4'){
            if ($values->return_bus_other != ''){
                if ($this->is_downloading()){
                    $data = $values->return_bus_other;
                } else {
                    $data = '<div class="tr_details">'.$values->return_bus_other.'</div>';
                }
            }
        } elseif ($values->return_transport_type == '5'){
            if (isset($this->_sites[$values->return_nyc_stops])){
                if ($this->is_downloading()){
                    $data = 'NYC Stop: '.$this->_sites[$values->return_nyc_stops];
                } else {
                    $data = '<div class="tr_details"><label>NYC Stop: </label>'.$this->_sites[$values->return_nyc_stops].'</div>';
                }
            }
        }
      return $data;
    }
    
    function col_trans_type_return($values) {
        global $DB, $CFG;
        $output = '-';

        if ($this->is_downloading()){
            $output = (isset($this->_trans_types[$values->trans_type_return]) and $values->trans_type_return != '') ? $this->_trans_types[$values->trans_type_return] : '-';
        } else {
            $output = html_writer::start_tag('select', array('name' => 'trans-type-select-'.$values->id, 'id'=>'trans-type-'.$values->id, 'data-id'=>$values->id, 'class'=>'trans-type-select'));
            foreach ($this->_trans_types as $key=>$name){
                $params = array('value' => $key);
                if ($key == $values->trans_type_return){
                    $params['selected'] = 'selected';
                }
                $output .= html_writer::tag('option', $name, $params);   
            }
            $output .= html_writer::end_tag("select");   
        }

        return $output;
    }
    
    function col_pickup_site_return($values) {
        global $DB, $CFG;
        $output = '-';

        if ($this->is_downloading()){
            $output = (isset($this->_pickup_sites[$values->pickup_site_return]) and $this->_pickup_sites[$values->pickup_site_return] != '') ? $this->_pickup_sites[$values->pickup_site_return] : '-';
        } else {
            $output = html_writer::start_tag('select', array('name' => 'pickup-site-return-'.$values->id, 'id'=>'pickup-site-return-'.$values->id, 'data-id'=>$values->id, 'class'=>'pickup-site-return'));
                foreach ($this->_pickup_sites as $key=>$name){
                    $params = array('value' => $key);
                    if ($key == $values->pickup_site_return){
                        $params['selected'] = 'selected';
                    }
                    $output .= html_writer::tag('option', $name, $params);   
                }
            $output .= html_writer::end_tag("select");
        }

        return $output;
    }
    
    function col_pickup_time_return($values) {
        global $DB, $CFG;
        $output = '-';

        if ($this->is_downloading()){
            if ($values->pickup_time_return === '0'){
                $time_return_other = (int)$values->pickup_time_return_other;
                if ($time_return_other > 0){
                    $time_return_other = date('m/d/Y h:i A', $time_return_other);
                } else {
                    $time_return_other = '-';
                }
                $output = $time_return_other;
            } else {
                //$output = (isset($this->_pickup_times[$values->pickup_time_return])) ? date('m/d/Y h:i A', $this->_pickup_times[$values->pickup_time_return]) : '-';
                $output = ($values->pickup_time_return == '-1') ? date('m/d/Y h:i A', $values->return_date_time) : '-';
            }
        } else {
            $output = html_writer::start_tag('select', array('name' => 'pickup-time-return-'.$values->id, 'id'=>'pickup-time-return-'.$values->id, 'data-id'=>$values->id, 'class'=>'pickup-time-return'));
                /*foreach ($this->_pickup_times as $key=>$name){
                    $params = array('value' => $key);
                    if ($key == $values->pickup_time_return){
                        $params['selected'] = 'selected';
                    }
                    $output .= html_writer::tag('option', date('m/d/Y h:i A', $name), $params);   
                }*/
                $params = array('value' => '');
                $output .= html_writer::tag('option', '----', $params);   
                $params = array('value' => '-1');
                if ($values->pickup_time_return == '-1'){
                    $params['selected'] = 'selected';
                }
                $output .= html_writer::tag('option', date('m/d/Y h:i A', $values->return_date_time), $params); 
            
                $params = array('value' => '0');
                if ($values->pickup_time_return === '0'){
                    $params['selected'] = 'selected';
                }
                $output .= html_writer::tag('option', 'Other', $params);   
            $output .= html_writer::end_tag("select");
            $time_return_other = (int)$values->pickup_time_return_other;
            if ($time_return_other > 0){
                $time_return_other = date('m/d/Y h:i A', $time_return_other);
            } else {
                $time_return_other = '';
            }
            $output .= html_writer::start_tag('div', array('id' => 'pickup_time_other_box_'.$values->id, 'class' => 'pickup-time-other-box', 'style'=>(($values->pickup_time_return === '0') ? '' : 'display:none;')));
                $output .= html_writer::start_tag("div", array('class'=>'input-append datetimepicker'));
                $output .= html_writer::empty_tag('input', array('type' => 'text', 'name' => 'pickup-time-other-'.$values->id, 'id' => 'pickup_time_other_'.$values->id, 'value' => $time_return_other, 'class' => 'date-time', 'data-format'=>'MM/dd/yyyy HH:mm PP'));
                $output .= html_writer::start_tag("span", array('class'=>'add-on'));
                $output .= html_writer::tag("span", '', array('class'=>'fa fa-calendar'));
                $output .= html_writer::end_tag("span");
                $output .= html_writer::end_tag("div");
                $output .= html_writer::empty_tag('input', array('type' => 'button', 'data-id'=>$values->id, 'value' => get_string('save', 'local_mxschool')));
            $output .= html_writer::end_tag('div');
        }

        return $output;
    }
    
    
    
    function col_confirmed_return($values) {
        global $OUTPUT, $PAGE;    
            
        $output = '-';
        if ($this->is_downloading()){
            if ($values->confirmed_return == '1'){
                $output = 'Yes';
            } elseif ($values->confirmed_return == '2'){
                $output = 'No';
            }
        } else {
            $output = html_writer::start_tag('div', array('class' => 'confirmed-return-box', 'id' => 'confirmed_return_box_'.$values->id, 'data-id'=>$values->id));

                $output .= html_writer::start_tag('label', array('class'=>'confirmed-return-label', 'style'=>'display:block;'));
                $params = array('type'=>'radio', 'value'=>'1', 'name'=>'confirmed-return-'.$values->id.'[]', 'id' => 'confirmed_return_'.$values->id, 'class'=>'confirmed-return');
                if ($values->confirmed_return == '1'){
                    $params['checked'] = 'checked';
                }
                $output .= html_writer::tag('input', 'Yes', $params);
                $output .= html_writer::end_tag('label');

                $output .= html_writer::start_tag('label', array('class'=>'confirmed-return-label', 'style'=>'display:block;'));
                $params = array('type'=>'radio', 'value'=>'2', 'name'=>'confirmed-return-'.$values->id.'[]', 'id' => 'confirmed_return_'.$values->id, 'class'=>'confirmed-return');
                if ($values->confirmed_return == '2'){
                    $params['checked'] = 'checked';
                }
                $output .= html_writer::tag('input', 'No', $params);
                $output .= html_writer::end_tag('label');

            $output .= html_writer::end_tag("div");
        }
        
      return $output;
    }
    
    function col_email_sendable_return($values) {
        global $OUTPUT, $PAGE;    
            
        $output = '-';
        
        if ($this->is_downloading()){
            $output = ($values->email_sendable_return == '1') ? 'Yes' : 'No';
        } else {
            $output = html_writer::start_tag('label', array('class'=>'email-return-label', 'style'=>'display:block;'));
            $params = array('type'=>'checkbox', 'value'=>'1', 'name'=>'email-sendable-return-'.$values->id.'[]', 'id' => 'email-sendable-return'.$values->id, 'class'=>'email-sendable-return', 'data-id'=>$values->id);
            if ($values->email_sendable_return == '1'){
                $params['checked'] = 'checked';
            }
            $output .= html_writer::tag('input', '', $params);
            $output .= html_writer::end_tag('label');
        }
        
      return $output;
    }
    
    function col_email_sent_return($values) {
        
        $output = ($values->email_sent_return > 0) ? 'Yes' : 'No';
        return $output;
    }
    
    function col_actions($values) {
      global $OUTPUT, $PAGE;
        
        if ($this->is_downloading()){
            return '';
        }
        
        $strdelete  = get_string('delete');
        $stredit  = get_string('edit');
      
        $edit = array();
        
        $aurl = new moodle_url('/local/mxschool/vacationandtravel/form.php', array('id'=>$values->id));
        $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/edit', $stredit, 'core', array('class' => 'iconsmall')));

        $aurl = new moodle_url('/local/mxschool/vacationandtravel/return.php', array('action'=>'delete', 'id'=>$values->id));
        $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/delete', $strdelete, 'core', array('class' => 'iconsmall')), null, array('onclick'=>"if (!confirm('Are you sure want to delete this redord?')) return false;"));
        
      return implode('', $edit);
    }
    
    function set_sites(){
        global $DB;
        $data = array();
        $sites = $DB->get_records("local_mxschool_pickup_sites");
        if (count($sites) > 0){
            foreach ($sites as $site){
                $data[$site->id] = $site->site;
            }
            $this->_sites = $data;
        }
    }
    
    function set_pickup_sites(){
        global $DB;
        $data = array(''=>'----');
        $sites = $DB->get_records_sql("SELECT id, site 
                                            FROM {local_mxschool_pickup_sites}
                                        WHERE type = '2' AND status = 1 AND sitetype = 'admin'
                                            ORDER BY site");
        if (count($sites) > 0){
            foreach ($sites as $site){
                $data[$site->id] = $site->site;
            }
            $this->_pickup_sites = $data;
        }
    }
    
    function set_pickup_times(){
        global $DB;
        $data = array(''=>'----');
        $times = $DB->get_records_sql("SELECT id, datetime 
                                            FROM {local_mxschool_pickup_times}
                                        WHERE type = '2' AND status = 1
                                            ORDER BY datetime");
        if (count($times) > 0){
            foreach ($times as $time){
                $data[$time->id] = $time->datetime;
            }
            $this->_pickup_times = $data;
        }
    }
}
