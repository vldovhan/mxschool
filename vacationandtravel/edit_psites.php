<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     mxschool
 * @copyright  2016 mxschool.edu
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */


require('../../../config.php');
require_once('edit_psites_form.php');
require_once($CFG->dirroot."/local/mxschool/classes/alerts/alerts.php");

$id         = optional_param('id', 0, PARAM_INT);
$type       = optional_param('type', 0, PARAM_INT);

require_login();
$systemcontext   = context_system::instance();
require_capability('local/mxschool:vacation_manage', $systemcontext);

if ($type == 2){
    $title = get_string('return_pickupsites', 'local_mxschool');
} else {
    $title = get_string('departure_pickupsites', 'local_mxschool');
}

$PAGE->set_url(new moodle_url("/local/mxschool/vacationandtravel/edit_psites.php", array()));
$PAGE->navbar->add(get_string('pluginname', 'local_mxschool'), new moodle_url('/local/mxschool/index.php'));
$PAGE->navbar->add(get_string('vacationandtravel', 'local_mxschool'), new moodle_url('/local/mxschool/vacationandtravel/index.php'));
if ($type == 2){
    $PAGE->navbar->add(get_string('return_pickupsites', 'local_mxschool'), new moodle_url('/local/mxschool/vacationandtravel/return_pickupsites.php'));
} else {
    $PAGE->navbar->add(get_string('departure_pickupsites', 'local_mxschool'), new moodle_url('/local/mxschool/vacationandtravel/departure_pickupsites.php'));
}
$PAGE->navbar->add($title);
$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('admin');
$PAGE->set_title($title);
$PAGE->set_heading($title);

$record = $DB->get_record('local_mxschool_pickup_sites', array('id'=>$id));
$sitetypes = array('admin'=>'', 'plane'=>'Plane', 'train'=>'Train', 'bus'=>'Bus', 'nyc_direct'=>'NYC Direct');

$editform = new edit_form(null, array('record'=>$record, 'type'=>$type, 'sitetypes'=>$sitetypes));

if ($editform->is_cancelled()) {
    // The form has been cancelled, take them back to what ever the return to is.
    if ($type == 2){
        redirect(new moodle_url('/local/mxschool/vacationandtravel/return_pickupsites.php'));
    } else {
        redirect(new moodle_url('/local/mxschool/vacationandtravel/departure_pickupsites.php'));
    }
} else if ($data = $editform->get_data()) {
    
    if ($data->id > 0){
        $DB->update_record('local_mxschool_pickup_sites', $data);   
    } else {
        $DB->insert_record('local_mxschool_pickup_sites', $data);   
    }
    
    if ($type == 2){
        $jAlert->create(array('type'=>'success', 'text'=>get_string('return_pickupsites', 'local_mxschool').' was successfully sent'));
        redirect(new moodle_url('/local/mxschool/vacationandtravel/return_pickupsites.php'));
    } else {
        $jAlert->create(array('type'=>'success', 'text'=>get_string('departure_pickupsites', 'local_mxschool').' was successfully sent'));
        redirect(new moodle_url('/local/mxschool/vacationandtravel/departure_pickupsites.php'));
    }
}

// Print the form.

$PAGE->set_context(context_system::instance());
$PAGE->set_title($title);
$PAGE->set_heading($title);

echo $OUTPUT->header();
echo $OUTPUT->heading($title);

echo html_writer::start_tag('div', array('class' => 'mx-adviser-select-form'));
$editform->display();
echo html_writer::end_tag('div');

echo $OUTPUT->footer();

