<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

require('../../../config.php');
require_once($CFG->dirroot."/local/mxschool/classes/alerts/alerts.php");

$systemcontext   = context_system::instance();
require_login();
$title = get_string('vacationandtravel', 'local_mxschool');

$PAGE->set_url(new moodle_url("/local/mxschool/vacationandtravel/index.php", array()));
$PAGE->navbar->add(get_string('pluginname', 'local_mxschool'), new moodle_url('/local/mxschool/index.php'));
$PAGE->navbar->add($title);
$PAGE->set_pagelayout('course');
$PAGE->set_context(context_system::instance());
$PAGE->set_title($title);
$PAGE->set_heading($title);

echo $OUTPUT->header();
echo $OUTPUT->heading($title, 2);

echo html_writer::start_tag('div', array('class' => 'mxschool-box'));
    echo html_writer::start_tag('ul', array('class' => 'mxschool-manage-menu'));
        echo html_writer::tag('li', html_writer::link(new moodle_url('/local/mxschool/vacationandtravel/form.php'), get_string('vacationandtravel_form', 'local_mxschool')));
        echo html_writer::tag('li', html_writer::link(new moodle_url('/local/mxschool/vacationandtravel/departure.php'), get_string('view_departure', 'local_mxschool')));
        echo html_writer::tag('li', html_writer::link(new moodle_url('/local/mxschool/vacationandtravel/return.php'), get_string('view_return', 'local_mxschool')));
        /*echo html_writer::tag('li', html_writer::link(new moodle_url('/local/mxschool/vacationandtravel/departure_pickuptime.php'), get_string('departure_pickuptime', 'local_mxschool')));
        echo html_writer::tag('li', html_writer::link(new moodle_url('/local/mxschool/vacationandtravel/return_pickuptime.php'), get_string('return_pickuptime', 'local_mxschool')));*/
        echo html_writer::tag('li', html_writer::link(new moodle_url('/local/mxschool/vacationandtravel/departure_pickupsites.php'), get_string('departure_pickupsites', 'local_mxschool')));
        echo html_writer::tag('li', html_writer::link(new moodle_url('/local/mxschool/vacationandtravel/return_pickupsites.php'), get_string('return_pickupsites', 'local_mxschool')));
        if (has_capability('local/mxschool:vacation_settings', context_system::instance())){
            echo html_writer::tag('li', html_writer::link(new moodle_url('/local/mxschool/vacationandtravel/preferences.php'), get_string('preferences', 'local_mxschool')));
        }
    echo html_writer::end_tag('ul');
echo html_writer::end_tag('div', array('class' => 'mxschool-box'));

echo $OUTPUT->footer();

