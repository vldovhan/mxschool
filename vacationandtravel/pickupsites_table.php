<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/tablelib.php');

class departure_pickupsites_table extends table_sql {
    
    public $sitetypes = array('admin'=>'', 'plane'=>'Plane', 'train'=>'Train', 'bus'=>'Bus', 'nyc_direct'=>'NYC Direct');
    
    function __construct($uniqueid, $search) {
		global $CFG, $USER, $DB;

        parent::__construct($uniqueid);
        
        $columns = array('site', 'sitetype', 'status', 'actions');
        $headers = array(
            get_string('site', 'local_mxschool'),
            get_string('sitetype', 'local_mxschool'),
            get_string('state', 'local_mxschool'),
            get_string('actions', 'local_mxschool')
        );

        $this->define_columns($columns);
        $this->define_headers($headers);
        
        $fields = "st.id, st.site, st.sitetype, st.status, st.type";
        $from = "{local_mxschool_pickup_sites} st ";
        $where = 'st.id > 0 AND st.type = 1';
        $this->set_sql($fields, $from, $where, array());
        $this->define_baseurl("$CFG->wwwroot/local/mxschool/vacationandtravel/departure_pickupsites.php");
    }
    
    function col_sitetype($values) {
      return ($values->sitetype) ? $this->sitetypes[$values->sitetype] : '';
    }
    
    function col_status($values) {
      return ($values->status) ? 'Visible' : 'Not visible';
    }
    
    function col_actions($values) {
      global $OUTPUT, $PAGE;
        
        $strdelete  = get_string('delete');
        $stredit  = get_string('edit');
        $enable  = get_string('show');
        $disable  = get_string('hide');
        $edit = array();
        
        if ($values->status > 0){
            $aurl = new moodle_url('/local/mxschool/vacationandtravel/departure_pickupsites.php', array('action'=>'hide', 'id'=>$values->id));
            $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/hide', $disable, 'core', array('class' => 'iconsmall')));
        } else {
            $aurl = new moodle_url('/local/mxschool/vacationandtravel/departure_pickupsites.php', array('action'=>'show', 'id'=>$values->id));
            $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/show', $enable, 'core', array('class' => 'iconsmall')));
        }
        
        $aurl = new moodle_url('/local/mxschool/vacationandtravel/edit_psites.php', array('id'=>$values->id));
        $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/edit', $stredit, 'core', array('class' => 'iconsmall')));

        $aurl = new moodle_url('/local/mxschool/vacationandtravel/departure_pickupsites.php', array('action'=>'delete', 'id'=>$values->id));
        $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/delete', $strdelete, 'core', array('class' => 'iconsmall')), null, array('onclick'=>"if (!confirm('Are you sure want to delete this redord?')) return false;"));
        
      return implode('', $edit);
    }
}

class return_pickupsites_table extends table_sql {
    
    public $sitetypes = array('admin'=>'', 'plane'=>'Plane', 'train'=>'Train', 'bus'=>'Bus', 'nyc_direct'=>'NYC Direct');
    
    function __construct($uniqueid, $search) {
		global $CFG, $USER, $DB;

        parent::__construct($uniqueid);
        
        $columns = array('site', 'sitetype', 'status', 'actions');
        $headers = array(
            get_string('site', 'local_mxschool'),
            get_string('sitetype', 'local_mxschool'),
            get_string('state', 'local_mxschool'),
            get_string('actions', 'local_mxschool')
        );

        $this->define_columns($columns);
        $this->define_headers($headers);
        
        $fields = "ps.id, ps.site, ps.sitetype, ps.status, ps.type";
        $from = "{local_mxschool_pickup_sites} ps ";
        $where = 'ps.id > 0 AND ps.type = 2';
        $this->set_sql($fields, $from, $where, array());
        $this->define_baseurl("$CFG->wwwroot/local/mxschool/vacationandtravel/return_pickupsites.php");
    }
    
    function col_sitetype($values) {
      return ($values->sitetype) ? $this->sitetypes[$values->sitetype] : '';
    }
    
    function col_status($values) {
      return ($values->status) ? 'Visible' : 'Not visible';
    }
    
    function col_actions($values) {
      global $OUTPUT, $PAGE;
        
        $strdelete  = get_string('delete');
        $stredit  = get_string('edit');
        $enable  = get_string('show');
        $disable  = get_string('hide');
        $edit = array();
        
        if ($values->status > 0){
            $aurl = new moodle_url('/local/mxschool/vacationandtravel/return_pickupsites.php', array('action'=>'hide', 'id'=>$values->id));
            $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/hide', $disable, 'core', array('class' => 'iconsmall')));
        } else {
            $aurl = new moodle_url('/local/mxschool/vacationandtravel/return_pickupsites.php', array('action'=>'show', 'id'=>$values->id));
            $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/show', $enable, 'core', array('class' => 'iconsmall')));
        }
        
        $aurl = new moodle_url('/local/mxschool/vacationandtravel/edit_psites.php', array('id'=>$values->id));
        $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/edit', $stredit, 'core', array('class' => 'iconsmall')));

        $aurl = new moodle_url('/local/mxschool/vacationandtravel/return_pickupsites.php', array('action'=>'delete', 'id'=>$values->id));
        $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/delete', $strdelete, 'core', array('class' => 'iconsmall')), null, array('onclick'=>"if (!confirm('Are you sure want to delete this redord?')) return false;"));
        
      return implode('', $edit);
    }
}
