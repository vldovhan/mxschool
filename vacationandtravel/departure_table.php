<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/tablelib.php');

class departure_table extends table_sql {
    
    public $_transport_types = array(''=>'', '1'=>'Car', '2'=>'Plane', '3'=>'Train', '4'=>'Bus', '5'=>'NYC Direct');
    public $_trans_types = array(''=>'----', 'car'=>'Car', 'van'=>'Van', 'school transport'=>'School Transport', 'none'=>'None Needed');
    public $_sites = array();
    public $_dorms = array();
    public $_pickup_times = array();
    
    function __construct($uniqueid, $search, $filter, $download) {
		global $CFG, $USER, $DB;

        parent::__construct($uniqueid);
        
        $this->set_sites();
        $this->set_dorms();
        $this->set_pickup_times();
        
        $columns = array('student', 'destination', 'stu_cellphone', 'depart_date_time', 'depart_transport_type', 'depart_driver', 'depart_details', 'trans_type_depart', 'pickup_site_depart', 'pickup_time_depart', 'confirmed_depart', 'email_sendable_depart', 'email_sent_depart');
        $headers = array(
            get_string('student', 'local_mxschool'),
            get_string('destination', 'local_mxschool'),
            get_string('stu_cellphone', 'local_mxschool'),
            'Date & Time',
            get_string('transportation_type', 'local_mxschool'),
            'Airport / Station',
            'Details',
            'Trans Type',
            'Pickup Site',
            'Pickup Time',
            'Confirmed',
            'Email',
            'Sent'
        );
        
        if ($download == ''){
            $columns[] = 'actions';
            $headers[] = 'Actions';
            $this->no_sorting('actions');
        }
        $this->no_sorting('depart_driver');
        $this->no_sorting('depart_details');

        $this->define_columns($columns);
        $this->define_headers($headers);
        
        $sql_search = ($search) ? " AND (t.destination LIKE '%$search%' OR t.depart_driver LIKE '%$search%' OR u.firstname LIKE '%$search%' OR u.lastname LIKE '%$search%' OR u.email LIKE '%$search%' OR sa.site LIKE '%$search%' OR st.site LIKE '%$search%' OR sb.site LIKE '%$search%')" : "";
        
        if ($filter['start'] != '' or $filter['end'] != ''){
            $start = strtotime($filter['start']);
            $end = strtotime($filter['end']);
            if ($filter['start'] != '' && $filter['end'] != ''){
                $sql_search .= " AND t.depart_date_time BETWEEN $start AND $end";
            } elseif ($filter['start'] != ''){
                $sql_search .= " AND t.depart_date_time > $start";
            } elseif ($filter['end'] != ''){
                $sql_search .= " AND t.depart_date_time < $end";
            }
        }
        if ($filter['status'] != ''){
            if ($filter['status'] == 'pending'){
                $sql_search .= " AND (t.confirmed_depart = 2 OR t.confirmed_depart IS NULL)";
            } elseif($filter['status'] == 'confirmed'){
                $sql_search .= " AND t.confirmed_depart = 1";
            }
        }
        
        $fields = "t.*, CONCAT(u.firstname, ' ', u.lastname) as student";
        $from = "{local_mxschool_transport} t
                    LEFT JOIN {local_mxschool_students} s ON s.id = t.studentid
                    LEFT JOIN {user} u ON u.id = s.userid
                    LEFT JOIN {local_mxschool_pickup_sites} sa ON sa.id = t.depart_airport 
                    LEFT JOIN {local_mxschool_pickup_sites} st ON st.id = t.depart_train 
                    LEFT JOIN {local_mxschool_pickup_sites} sb ON sb.id = t.depart_bus ";
        
        $where = 't.id > 0 AND u.deleted = 0'.$sql_search;
        $this->set_sql($fields, $from, $where, array());
        $this->define_baseurl($CFG->wwwroot.$_SERVER['REQUEST_URI']);
    }
    
    function col_depart_date_time($values) {
        if ($this->is_downloading()){
            $date = date('m/d/Y h:i A', $values->depart_date_time);
        } else {
            $date = date('m/d/Y', $values->depart_date_time);
            $date .= '<br />'.date('h:i A', $values->depart_date_time);   
        }
      return $date;
    }
    
    function col_depart_transport_type($values) {
      return $this->_transport_types[$values->depart_transport_type];
    }
    
    function col_depart_driver($values) {
        $data = '-';
        if ($values->depart_transport_type == '2' and isset($this->_sites[$values->depart_airport])){
            $data = $this->_sites[$values->depart_airport];
        } elseif ($values->depart_transport_type == '3'){
            if ($values->depart_train != '0' and isset($this->_sites[$values->depart_train])){
                $data = $this->_sites[$values->depart_train];
            } elseif ($values->depart_train == '0'){
                $data = 'Other';
            }
        } elseif ($values->depart_transport_type == '4'){
            if ($values->depart_bus != '0' and isset($this->_sites[$values->depart_bus])){
                $data = $this->_sites[$values->depart_bus];
            } elseif ($values->depart_bus == '0'){
                $data = 'Other';
            }
        }
      return $data;
    }
    
    function col_depart_details($values) {
        $data = '-';
        if ($values->depart_transport_type == '1'){
            if ($this->is_downloading()){
                $data = 'Driver(s): '.$values->depart_driver;   
            } else {
                $data = '<div class="tr_details"><label>Driver(s):</label>'.$values->depart_driver.'</div>';
            }
        } elseif ($values->depart_transport_type == '2'){
            if ($this->is_downloading()){
                $data = 'Carrier: '.$values->depart_carrier;   
                $data .= '; Flight #: '.$values->depart_flight;   
                $data .= '; Customs?: '.(($values->depart_customs == 1) ? 'Yes' : 'No');   
            } else {
                $data = '<div class="tr_details"><label>Carrier:</label>'.$values->depart_carrier.'</div>';
                $data .= '<div class="tr_details"><label>Flight #:</label>'.$values->depart_flight.'</div>';
                $data .= '<div class="tr_details"><label>Customs?:</label>'.(($values->depart_customs == 1) ? 'Yes' : 'No').'</div>';
            }
        } elseif ($values->depart_transport_type == '3'){
            if ($values->depart_train_other != ''){
                if ($this->is_downloading()){
                    $data = $values->depart_train_other;
                } else {
                    $data = '<div class="tr_details">'.$values->depart_train_other.'</div>';
                }
            }
        } elseif ($values->depart_transport_type == '4'){
            if ($values->depart_bus_other != ''){
                if ($this->is_downloading()){
                    $data = $values->depart_bus_other;
                } else {
                    $data = '<div class="tr_details">'.$values->depart_bus_other.'</div>';
                }
            }
        } elseif ($values->depart_transport_type == '5'){
            if (isset($this->_sites[$values->depart_nyc_stops])){
                if ($this->is_downloading()){
                    $data = 'NYC Stop: '.$this->_sites[$values->depart_nyc_stops];
                } else {
                    $data = '<div class="tr_details"><label>NYC Stop: </label>'.$this->_sites[$values->depart_nyc_stops].'</div>';
                }
            }
        }
      return $data;
    }
    
    function col_trans_type_depart($values) {
        global $DB, $CFG;
        $output = '-';

        if ($this->is_downloading()){
            $output = (isset($this->_trans_types[$values->trans_type_depart]) and $values->trans_type_depart != '') ? $this->_trans_types[$values->trans_type_depart] : '-';
        } else {
            $output = html_writer::start_tag('select', array('name' => 'trans-type-select-'.$values->id, 'id'=>'trans-type-'.$values->id, 'data-id'=>$values->id, 'class'=>'trans-type-select'));
            foreach ($this->_trans_types as $key=>$name){
                $params = array('value' => $key);
                if ($key == $values->trans_type_depart){
                    $params['selected'] = 'selected';
                }
                $output .= html_writer::tag('option', $name, $params);   
            }
            $output .= html_writer::end_tag("select");   
        }

        return $output;
    }
    
    function col_pickup_site_depart($values) {
        global $DB, $CFG;
        $output = '-';

        if ($this->is_downloading()){
            $output = (isset($this->_dorms[$values->pickup_site_depart]) and $this->_dorms[$values->pickup_site_depart] != '') ? $this->_dorms[$values->pickup_site_depart] : '-';
        } else {
            $output = html_writer::start_tag('select', array('name' => 'pickup-site-depart-'.$values->id, 'id'=>'pickup-site-depart-'.$values->id, 'data-id'=>$values->id, 'class'=>'pickup-site-depart'));
                foreach ($this->_dorms as $key=>$name){
                    $params = array('value' => $key);
                    if ($key == $values->pickup_site_depart){
                        $params['selected'] = 'selected';
                    }
                    $output .= html_writer::tag('option', $name, $params);   
                }
            $output .= html_writer::end_tag("select");
        }

        return $output;
    }
    
    function col_pickup_time_depart($values) {
        global $DB, $CFG;
        $output = '-';

        if ($this->is_downloading()){
            if ($values->pickup_time_depart === '0'){
                $time_depart_other = (int)$values->pickup_time_depart_other;
                if ($time_depart_other > 0){
                    $time_depart_other = date('m/d/Y h:i A', $time_depart_other);
                } else {
                    $time_depart_other = '-';
                }
                $output = $time_depart_other;
            } else {
                //$output = (isset($this->_pickup_times[$values->pickup_time_depart])) ? date('m/d/Y h:i A', $this->_pickup_times[$values->pickup_time_depart]) : '-';
                $output = ($values->pickup_time_depart == '-1') ? date('m/d/Y h:i A', $values->depart_date_time) : '-';
            }
        } else {
            $output = html_writer::start_tag('select', array('name' => 'pickup-time-depart-'.$values->id, 'id'=>'pickup-time-depart-'.$values->id, 'data-id'=>$values->id, 'class'=>'pickup-time-depart'));
                /*foreach ($this->_pickup_times as $key=>$name){
                    $params = array('value' => $key);
                    if ($key == $values->pickup_time_depart){
                        $params['selected'] = 'selected';
                    }
                    $output .= html_writer::tag('option', date('m/d/Y h:i A', $name), $params);   
                }*/
                $params = array('value' => '');
                $output .= html_writer::tag('option', '----', $params);   
                $params = array('value' => '-1');
                if ($values->pickup_time_depart == '-1'){
                    $params['selected'] = 'selected';
                }
                $output .= html_writer::tag('option', date('m/d/Y h:i A', $values->depart_date_time), $params);   
            
                $params = array('value' => '0');
                if ($values->pickup_time_depart === '0'){
                    $params['selected'] = 'selected';
                }
                $output .= html_writer::tag('option', 'Other', $params);   
            $output .= html_writer::end_tag("select");
            $time_depart_other = (int)$values->pickup_time_depart_other;
            if ($time_depart_other > 0){
                $time_depart_other = date('m/d/Y h:i A', $time_depart_other);
            } else {
                $time_depart_other = '';
            }
            $output .= html_writer::start_tag('div', array('id' => 'pickup_time_other_box_'.$values->id, 'class' => 'pickup-time-other-box', 'style'=>(($values->pickup_time_depart === '0') ? '' : 'display:none;')));
                $output .= html_writer::start_tag("div", array('class'=>'input-append datetimepicker'));
                $output .= html_writer::empty_tag('input', array('type' => 'text', 'name' => 'pickup-time-other-'.$values->id, 'id' => 'pickup_time_other_'.$values->id, 'value' => $time_depart_other, 'class' => 'date-time', 'data-format'=>'MM/dd/yyyy HH:mm PP'));
                $output .= html_writer::start_tag("span", array('class'=>'add-on'));
                $output .= html_writer::tag("span", '', array('class'=>'fa fa-calendar'));
                $output .= html_writer::end_tag("span");
                $output .= html_writer::end_tag("div");
                $output .= html_writer::empty_tag('input', array('type' => 'button', 'data-id'=>$values->id, 'value' => get_string('save', 'local_mxschool')));
            $output .= html_writer::end_tag('div');
        }

        return $output;
    }
    
    
    
    function col_confirmed_depart($values) {
        global $OUTPUT, $PAGE;    
            
        $output = '-';
        if ($this->is_downloading()){
            if ($values->confirmed_depart == '1'){
                $output = 'Yes';
            } elseif ($values->confirmed_depart == '2'){
                $output = 'No';
            }
        } else {
            $output = html_writer::start_tag('div', array('class' => 'confirmed-depart-box', 'id' => 'confirmed_depart_box_'.$values->id, 'data-id'=>$values->id));

                $output .= html_writer::start_tag('label', array('class'=>'confirmed-depart-label', 'style'=>'display:block;'));
                $params = array('type'=>'radio', 'value'=>'1', 'name'=>'confirmed-depart-'.$values->id.'[]', 'id' => 'confirmed_depart_'.$values->id, 'class'=>'confirmed-depart');
                if ($values->confirmed_depart == '1'){
                    $params['checked'] = 'checked';
                }
                $output .= html_writer::tag('input', 'Yes', $params);
                $output .= html_writer::end_tag('label');

                $output .= html_writer::start_tag('label', array('class'=>'confirmed-depart-label', 'style'=>'display:block;'));
                $params = array('type'=>'radio', 'value'=>'2', 'name'=>'confirmed-depart-'.$values->id.'[]', 'id' => 'confirmed_depart_'.$values->id, 'class'=>'confirmed-depart');
                if ($values->confirmed_depart == '2'){
                    $params['checked'] = 'checked';
                }
                $output .= html_writer::tag('input', 'No', $params);
                $output .= html_writer::end_tag('label');

            $output .= html_writer::end_tag("div");
        }
        
      return $output;
    }
    
    function col_email_sendable_depart($values) {
        global $OUTPUT, $PAGE;    
            
        $output = '-';
        
        if ($this->is_downloading()){
            $output = ($values->email_sendable_depart == '1') ? 'Yes' : 'No';
        } else {
            $output = html_writer::start_tag('label', array('class'=>'email-depart-label', 'style'=>'display:block;'));
            $params = array('type'=>'checkbox', 'value'=>'1', 'name'=>'email-sendable-depart-'.$values->id.'[]', 'id' => 'email-sendable-depart'.$values->id, 'class'=>'email-sendable-depart', 'data-id'=>$values->id);
            if ($values->email_sendable_depart == '1'){
                $params['checked'] = 'checked';
            }
            $output .= html_writer::tag('input', '', $params);
            $output .= html_writer::end_tag('label');
        }
        
      return $output;
    }
    
    function col_email_sent_depart($values) {
        
        $output = ($values->email_sent_depart > 0) ? 'Yes' : 'No';
        return $output;
    }
    
    function col_actions($values) {
      global $OUTPUT, $PAGE;
        
        if ($this->is_downloading()){
            return '';
        }
        
        $strdelete  = get_string('delete');
        $stredit  = get_string('edit');
      
        $edit = array();
        
        $aurl = new moodle_url('/local/mxschool/vacationandtravel/form.php', array('id'=>$values->id));
        $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/edit', $stredit, 'core', array('class' => 'iconsmall')));

        $aurl = new moodle_url('/local/mxschool/vacationandtravel/departure.php', array('action'=>'delete', 'id'=>$values->id));
        $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/delete', $strdelete, 'core', array('class' => 'iconsmall')), null, array('onclick'=>"if (!confirm('Are you sure want to delete this redord?')) return false;"));
        
      return implode('', $edit);
    }
    
    function set_sites(){
        global $DB;
        $data = array();
        $sites = $DB->get_records("local_mxschool_pickup_sites");
        if (count($sites) > 0){
            foreach ($sites as $site){
                $data[$site->id] = $site->site;
            }
            $this->_sites = $data;
        }
    }
    
    function set_dorms(){
        global $DB;
        $data = array(''=>'----');
        $dorms = $DB->get_records_sql("SELECT abbreviation, name 
                                        FROM {local_mxschool_dorms}
                                        WHERE type = 'boarding' 
                                            UNION SELECT 'Elliot', 'Eliot Hall'
                                            UNION SELECT 'Ware', 'Ware Hall' 
                                        ORDER BY Name ASC");
        if (count($dorms) > 0){
            foreach ($dorms as $dorm){
                $data[$dorm->abbreviation] = $dorm->name;
            }
            $this->_dorms = $data;
        }
    }
    
    function set_pickup_times(){
        global $DB;
        $data = array(''=>'----');
        $times = $DB->get_records_sql("SELECT id, datetime 
                                            FROM {local_mxschool_pickup_times}
                                        WHERE type = '1' AND status = 1
                                            ORDER BY datetime");
        if (count($times) > 0){
            foreach ($times as $time){
                $data[$time->id] = $time->datetime;
            }
            $this->_pickup_times = $data;
        }
    }
    
}
