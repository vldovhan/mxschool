<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

require('../../../config.php');
require ('pickuptime_table.php');
require_once($CFG->dirroot."/local/mxschool/classes/alerts/alerts.php");

$search     = optional_param('search', '', PARAM_RAW);
$action     = optional_param('action', '', PARAM_RAW);
$id         = optional_param('id', 0, PARAM_INT);

require_login();
$systemcontext   = context_system::instance();
require_capability('local/mxschool:vacation_manage', $systemcontext);

if ($action == 'delete' and $id) {
    $DB->delete_records('local_mxschool_pickup_times', array('id'=>$id));
    $jAlert->create(array('type'=>'success', 'text'=>get_string('departure_pickuptime', 'local_mxschool').' was successfully deleted'));
    redirect(new moodle_url('/local/mxschool/vacationandtravel/departure_pickuptime.php'));
} elseif ($action == 'show' and $id) {
    $record = $DB->get_record('local_mxschool_pickup_times', array('id'=>$id));
    $record->status = 1;
    $DB->update_record('local_mxschool_pickup_times', $record);
    $jAlert->create(array('type'=>'success', 'text'=>get_string('departure_pickuptime', 'local_mxschool').' was successfully updated'));
    redirect(new moodle_url('/local/mxschool/vacationandtravel/departure_pickuptime.php'));
} elseif ($action == 'hide' and $id) {
    $record = $DB->get_record('local_mxschool_pickup_times', array('id'=>$id));
    $record->status = 0;
    $DB->update_record('local_mxschool_pickup_times', $record);
    $jAlert->create(array('type'=>'success', 'text'=>get_string('departure_pickuptime', 'local_mxschool').' was successfully updated'));
    redirect(new moodle_url('/local/mxschool/vacationandtravel/departure_pickuptime.php'));
}

$title = get_string('departure_pickuptime', 'local_mxschool');

$PAGE->set_url(new moodle_url("/local/mxschool/vacationandtravel/departure_pickuptime.php", array()));
$PAGE->navbar->add(get_string('pluginname', 'local_mxschool'), new moodle_url('/local/mxschool/index.php'));
$PAGE->navbar->add(get_string('vacationandtravel', 'local_mxschool'), new moodle_url('/local/mxschool/vacationandtravel/index.php'));
$PAGE->navbar->add($title);
$PAGE->requires->jquery();
$PAGE->set_pagelayout('admin');
$PAGE->set_context(context_system::instance());
$PAGE->set_title($title);
$PAGE->set_heading($title);

$table = new departure_pickuptime_table('departure_pickuptime_table', $search);
$table->is_collapsible = false;

echo $OUTPUT->header();
echo $OUTPUT->heading($title);

echo html_writer::start_tag("form",  array("action"=> $PAGE->url, 'class'=>'mxschool-search-form'));
echo html_writer::empty_tag('input', array('type' => 'button', 'value' => get_string('add_pickup_time', 'local_mxschool'), 'onclick'=>'location="'.$CFG->wwwroot.'/local/mxschool/vacationandtravel/edit_ptimes.php?type=1"'));
echo html_writer::end_tag("form");

echo html_writer::start_tag('div', array('class' => 'mxschool-table-box'));
$table->out(20, true);
echo html_writer::end_tag("div");

echo $OUTPUT->footer();
