<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

require('../../../config.php');
require ('enter_driving_form.php');
require_once($CFG->dirroot."/local/mxschool/classes/alerts/alerts.php");

$student = optional_param('student',0,PARAM_INT);

require_login();
$systemcontext   = context_system::instance();
require_capability('local/mxschool:driving_settings', $systemcontext);

$title = get_string('enter_driving_form','local_mxschool');

$PAGE->set_url(new moodle_url("/local/mxschool/driving/enter-driving.php", array()));
$PAGE->navbar->add(get_string('pluginname', 'local_mxschool'), new moodle_url('/local/mxschool/index.php'));
$PAGE->navbar->add(get_string('driving_name', 'local_mxschool'), new moodle_url('/local/mxschool/driving/index.php'));
$PAGE->navbar->add($title);
$PAGE->requires->jquery();
$PAGE->set_pagelayout('admin');
$PAGE->set_context(context_system::instance());
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->add_body_class('print-table');

$form = new enter_driving_form(null,array('student'=>$student));
if (!$form->is_cancelled() && $data = $form->get_data()) {
    if($record = $DB->get_record('local_mxschool_driving',array('student'=>$data->student))){

        $DB->delete_records('local_mxschool_driving',array('student'=>$data->student));

        $cars = json_decode($data->data);
        foreach($cars as $car){
            $car->student = $data->student;
            $car->date = date('n/j/Y',$data->date_license);
            $car->date_time = $data->date_license;
            $car->timecreate = time();
            $DB->insert_record('local_mxschool_driving', $car);
        }

        $jAlert->create(array('type'=>'success', 'text'=>'Successfully updated'));
        redirect(new moodle_url('/local/mxschool/driving/manage-driving.php'));
    }else{
        $cars = json_decode($data->data);
        foreach($cars as $car){
            $car->student = $data->student;
            $car->date = date('n/j/Y',$data->date_license);
            $car->date_time = $data->date_license;
            $car->timecreate = time();
            $DB->insert_record('local_mxschool_driving', $car);
        }

        $jAlert->create(array('type'=>'success', 'text'=>'Successfully created'));
        redirect(new moodle_url('/local/mxschool/driving/manage-driving.php'));
    }
}

echo $OUTPUT->header();
echo $OUTPUT->heading($title);

echo html_writer::start_tag('div', array('class' => 'mxschool-table-box'));
$form->display();
echo html_writer::end_tag("div");

echo $OUTPUT->footer();

?>
    <script>
        jQuery(document).ready(function(){
            var i = 1;
            $('#student').change(function () {
                var val = $(this).val();
                if(val != ''){
                    jQuery.ajax({
                        url: M.cfg.wwwroot+"/local/mxschool/driving/get_student_licenses.php",
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            'student' :val,
                        },
                        complete: function(data) {
                            $('.fitem.fitem_fgroup.additional-license').detach();
                            $('#id_make_of_car').val('');
                            $('#id_model_of_car').val('');
                            $('#id_color').val('');
                            $('#id_license_plate').val('');

                            var request = data.responseJSON;
                            $('#id_date_license_day option[value='+request.time[1]+']').attr('selected','selected');
                            $('#id_date_license_month option[value='+request.time[0]+']').attr('selected','selected');
                            $('#id_date_license_year option[value='+request.time[2]+']').attr('selected','selected');

                            var response = request.data;
                            i = 1;
                            for (var key in response) {
                                if(i == 1){
                                    //response[key]
                                    $('#fgroup_id_license fieldset').removeClass('hide');
                                    $('#fgroup_id_license').removeClass('license-hidden');
                                    $('#id_make_of_car').val(response[key]['make_of_car']);
                                    $('#id_model_of_car').val(response[key]['model_of_car']);
                                    $('#id_color').val(response[key]['color']);
                                    $('#id_license_plate').val(response[key]['license_plate']);
                                }else{
                                    var html = $('#fgroup_id_license .felement').html();
                                    $('#fgroup_id_license').after('<div class="fitem fitem_fgroup additional-license license_'+i+'"><fieldset class="felement fgroup">'+html+'<span class="remove-license" data_license="'+i+'"><i class="fa fa-minus"></i>Remove</span></fieldset></div>');

                                    $('.fitem.fitem_fgroup.license_'+i+' input[name="make_of_car"]').val(response[key]['make_of_car']);
                                    $('.fitem.fitem_fgroup.license_'+i+' input[name="model_of_car"]').val(response[key]['model_of_car']);
                                    $('.fitem.fitem_fgroup.license_'+i+' input[name="color"]').val(response[key]['color']);
                                    $('.fitem.fitem_fgroup.license_'+i+' input[name="license_plate"]').val(response[key]['license_plate']);
                                    $('.fitem.fitem_fgroup.license_'+i+' input').attr('id','');
                                }
                                i++;
                                $('.remove-license').click(function () {
                                    var id = $(this).attr('data_license');
                                    $('.fitem.fitem_fgroup.additional-license.license_'+id).detach();
                                });
                            }
                        }
                    });
                }
            });


            if($('#student').val() != '')
                $('#student').change();

            $('#add-license').click(function () {
                if($('#fgroup_id_license fieldset').hasClass('hide')){
                    $('#fgroup_id_license fieldset').removeClass('hide');
                    $('#fgroup_id_license').removeClass('license-hidden');
                }else {
                    var html = $('#fgroup_id_license .felement').html();
                    $(this).before('<div class="fitem fitem_fgroup additional-license license_' + i + '"><fieldset class="felement fgroup">' + html + '<span class="remove-license" data_license="' + i + '"><i class="fa fa-minus"></i>Remove</span></fieldset></div>');
                    i++;

                    $('.remove-license').click(function () {
                        var id = $(this).attr('data_license');
                        console.log(id);
                        $('.fitem.fitem_fgroup.additional-license.license_' + id).detach();
                    });
                }

            });

            $('#mform1').submit(function () {
                var data = { };
                var i = 0;
                $('.fitem_fgroup:not(.fitem_actionbuttons) .fgroup').each(function () {
                    var values = { };
                    $(this).children('input').each(function () {
                        var name = $(this).attr('name');
                        var val = $(this).val();
                        if(val != '')
                            values[name] = $(this).val();
                    });
                    if(!$.isEmptyObject(values))
                        data[i++] = values;
                });
                var json = JSON.stringify(data);
                $('#mform1 input[name="data"]').val(json);
                
                return true;
            });

            function submit_form() {
                $('#mform1').submit();
            }

            $('#fgroup_id_license fieldset').append('<span class="hide-license" data_license="'+i+'"><i class="fa fa-minus"></i>Remove</span>');

            $('.hide-license').click(function () {
                $('#fgroup_id_license fieldset input').val('');
                $('#fgroup_id_license fieldset').addClass('hide');
                $('#fgroup_id_license').addClass('license-hidden');
            });
            
        });
    </script>
<?php
