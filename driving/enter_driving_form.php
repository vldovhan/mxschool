<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/tablelib.php');
require_once($CFG->libdir.'/formslib.php');
require_once('../lib.php');


class enter_driving_form extends moodleform {
    /**
     * Form definition.
     */
    function definition() {
        global $DB,$USER;

        $mform          = $this->_form;
        $student_id             = $this->_customdata['student'];
        $record = $DB->get_record('local_mxschool_driving',array('student'=>$student_id));

        $students_obj = $DB->get_records_sql('SELECT s.id, CONCAT(u.firstname, \' \', u.lastname) as username 
                                    FROM {local_mxschool_students} s 
                                        LEFT JOIN {user} u ON u.id = s.userid 
                                        LEFT JOIN {local_mxschool_dorms} d ON d.abbreviation = s.dorm 
                                        WHERE u.id > 0 AND u.deleted = 0 AND d.name LIKE "%Day House%"
                                    ORDER BY u.firstname, u.lastname');
        $students = array(''=>'');
        foreach($students_obj as $student){
            $students[$student->id] = $student->username;
        }

        $mform->addElement('select', 'student', get_string('student', 'local_mxschool'), $students, array('id' => 'student'));
        $mform->setType('student', PARAM_INT);
        $mform->addRule('student', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        $mform->setDefault('student', $student_id);

        $mform->addElement('date_selector', 'date_license', get_string('date_license','local_mxschool'),array('startyear' => date('Y',strtotime('-6 year')),'stopyear'  => date('Y')));
        $mform->setType('date_license', PARAM_INT);
        $mform->addRule('date_license', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        if(isset($record->id))
            $mform->setDefault('date_license', strtotime($record->date));

        $elementarray=array();
        $elementarray[] =& $mform->createElement('text', 'make_of_car', '',array('placeholder'=>get_string('make_of_car','local_mxschool')));
        $elementarray[] =& $mform->createElement('text', 'model_of_car', '',array('placeholder'=>get_string('model_of_car','local_mxschool')));
        $elementarray[] =& $mform->createElement('text', 'color', '',array('placeholder'=>get_string('color','local_mxschool')));
        $elementarray[] =& $mform->createElement('text', 'license_plate', '',array('placeholder'=>get_string('license_plate','local_mxschool')));
        $mform->setType('make_of_car', PARAM_RAW);
        $mform->setType('model_of_car', PARAM_RAW);
        $mform->setType('color', PARAM_RAW);
        $mform->setType('license_plate', PARAM_RAW);

        $mform->addGroup($elementarray, 'license', get_string('cars','local_mxschool'), array(' '), false);
        $mform->addRule('license', get_string('required_field', 'local_mxschool'), 'required', null, 'client');

        $mform->addElement('html', html_writer::tag('span', html_writer::tag('i', '',array('class' => 'fa fa-plus')).get_string('add'),array('id' => 'add-license')));

        $mform->addElement('html', html_writer::tag('h3', 'Student signature: ______________________________________________',array('class' => 'signature')));
        $mform->addElement('html', html_writer::tag('h3', 'Parent signature: ______________________________________________',array('class' => 'signature')));

        $elementarray=array();
        $elementarray[] =& $mform->createElement('submit', get_string('submit_form', 'local_mxschool'), get_string('submit_form', 'local_mxschool'));
        //$elementarray[] =& $mform->createElement('submit', get_string('print_form', 'local_mxschool'), get_string('print_form', 'local_mxschool'),array('onclick' => 'javascript:window.print();'));
        $mform->addGroup($elementarray, 'submit_button', get_string('cars','local_mxschool'), array(' '), false);

        //$this->add_action_buttons(get_string('print_form', 'local_mxschool'), get_string('submit_form', 'local_mxschool'));
        /*$mform->addElement('submit', get_string('submit_form', 'local_mxschool'), get_string('submit_form', 'local_mxschool'));
        $mform->addElement('submit', get_string('print_form', 'local_mxschool'), get_string('print_form', 'local_mxschool'),array('onclick' => 'javascript:window.print();'));*/

        $mform->addElement('hidden', 'data', '0');
        $mform->setType('data', PARAM_RAW);

    }
    function validation($data, $files) {
        global $DB;

        $errors = parent::validation($data, $files);

        if($data['data'] == '{}'){
            $errors['license'] = 'Please enter one or more cars';
        }

        return $errors;
    }
}
