<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

require('../../../config.php');
require ('announcements_table.php');
require_once($CFG->dirroot."/local/mxschool/classes/alerts/alerts.php");

$search     = optional_param('search', '', PARAM_RAW);
$download   = optional_param('download', '', PARAM_ALPHA);
$action     = optional_param('action', '', PARAM_RAW);
$id         = optional_param('id', 0, PARAM_INT);

require_login();
$systemcontext   = context_system::instance();
$title = get_string('announcements', 'local_mxschool');
require_capability('local/mxschool:announcements_manage', $systemcontext);

if ($action == 'delete' and $id) {
    $DB->delete_records('local_mxschool_announcements', array('id'=>$id));
    $jAlert->create(array('type'=>'success', 'text'=>'Announcement was successfully deleted'));
    redirect(new moodle_url("/local/mxschool/announcements/index.php"));
} elseif ($action == 'show' and $id){
    $announcement = $DB->get_record('local_mxschool_announcements', array('id'=>$id));
    if ($announcement){
        $announcement->state = 1;
        $DB->update_record('local_mxschool_announcements', $announcement);
    }
    $jAlert->create(array('type'=>'success', 'text'=>'Announcement was successfully updated'));
    redirect(new moodle_url("/local/mxschool/announcements/index.php"));
} elseif($action == 'hide' and $id){
    $announcement = $DB->get_record('local_mxschool_announcements', array('id'=>$id));
    if ($announcement){
        $announcement->state = 0;
        $DB->update_record('local_mxschool_announcements', $announcement);
    }
    $jAlert->create(array('type'=>'success', 'text'=>'Announcement was successfully updated'));
    redirect(new moodle_url("/local/mxschool/announcements/index.php"));
}

$PAGE->set_url(new moodle_url("/local/mxschool/announcements/index.php", array()));
$PAGE->navbar->add(get_string('pluginname', 'local_mxschool'), new moodle_url('/local/mxschool/index.php'));
$PAGE->navbar->add($title);
$PAGE->set_pagelayout('course');
$PAGE->set_context(context_system::instance());
$PAGE->set_title($title);
$PAGE->set_heading($title);

$table = new announcements_table('announcements_table', $search);
$table->is_collapsible = false;

echo $OUTPUT->header();
echo $OUTPUT->heading($title);
echo html_writer::start_tag("form",  array("action"=> $PAGE->url, 'class'=>'mxschool-search-form'));
echo html_writer::start_tag("label",  array());
echo html_writer::empty_tag('input', array('type' => 'text', 'name' => 'search', 'placeholder' => get_string('search').' ...', 'value' => $search));
echo html_writer::empty_tag('input', array('type' => 'submit', 'value' => get_string('submit')));
echo html_writer::empty_tag('input', array('type' => 'button', 'value' => get_string('create_announcement', 'local_mxschool'), 'onclick'=>'location="'.$CFG->wwwroot.'/local/mxschool/announcements/edit.php"'));
echo html_writer::end_tag("label");
echo html_writer::end_tag("form");

echo html_writer::end_tag("form");

$table->out(20, true);

echo $OUTPUT->footer();

