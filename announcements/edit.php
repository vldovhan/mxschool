<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */


require('../../../config.php');
require_once('edit_form.php');
require_once('../lib.php');

require_login();
$systemcontext   = context_system::instance();
require_capability('local/mxschool:announcements_manage', $systemcontext);

$id = optional_param('id', 0, PARAM_INT); // Announcement id.

$title = ($id > 0) ? get_string('edit_announcement', 'local_mxschool') : get_string('create_announcement', 'local_mxschool');
$PAGE->set_url(new moodle_url("/local/mxschool/announcements/edit.php", array()));
$PAGE->navbar->add(get_string('pluginname', 'local_mxschool'), new moodle_url('/local/mxschool/index.php'));
$PAGE->navbar->add(get_string('announcements', 'local_mxschool'), new moodle_url('/local/mxschool/announcements/index.php'));
$PAGE->navbar->add($title);
$PAGE->requires->jquery();
$PAGE->requires->js('/local/mxschool/announcements/edit.js', true);
$PAGE->set_pagelayout('course');
$PAGE->set_context(context_system::instance());
$PAGE->set_title($title);
$PAGE->set_heading($title);

// Prepare course and the editor.
$editoroptions = array('maxfiles' => 1, 'maxbytes'=>$CFG->maxbytes, 'trusttext'=>false, 'noclean'=>true, 'context'=>$systemcontext, 'subdirs'=>0);
if ($id > 0) {
    $announcement = $DB->get_record('local_mxschool_announcements', array('id'=>$id));
} else {
    $announcement = new stdClass();
    $announcement->id = null;
}

$courses_list = array();
$announcement = file_prepare_standard_editor($announcement, 'description', $editoroptions, $systemcontext, 'local_mxschool_announcements', 'descriptionfile', $announcement->id);


// First create the form.
$args = array(
    'id' => $id,
    'announcement' => $announcement,
    'editoroptions' => $editoroptions,
    'courses' => get_announcements_courseslist()
);
$editform = new edit_form(null, $args);

if ($editform->is_cancelled()) {
    // The form has been cancelled, take them back to what ever the return to is.
    redirect(new moodle_url('/local/mxschool/announcements/index.php'));
} else if ($data = $editform->get_data()) {
    // Process data if submitted.
    $announcement = $data;
    $announcement->type              = $data->type;
    $announcement->courseid          = ($data->type == 'course' and $data->courseid > 0) ? $data->courseid : 0;
    $announcement->description       = '';          // updated later
    $announcement->descriptionformat = FORMAT_HTML; // updated later
    $announcement->timemodified      = time();
    $announcement->userid            = $USER->id;
    
    if ($data->id > 0) {
        // Update existing announcement.
        $DB->update_record('local_mxschool_announcements', $announcement);
    } else {
        // Add new announcement.
        $announcement->timecreated = time();
        $announcement->id = $DB->insert_record('local_mxschool_announcements', $announcement);
    }
    
    // save and relink embedded images and save attachments
    $announcement = file_postupdate_standard_editor($announcement, 'description', $editoroptions, $systemcontext, 'local_mxschool_announcements', 'descriptionfile', $announcement->id);
   
    // store the updated value values
    $DB->update_record('local_mxschool_announcements', $announcement);
    
    redirect(new moodle_url('/local/mxschool/announcements/index.php'));
}

// Print the form.
echo $OUTPUT->header();
echo $OUTPUT->heading($title);

echo html_writer::start_tag('div', array('class' => 'mxschool-announcements-form'));
$editform->display();
echo html_writer::end_tag('div');

echo $OUTPUT->footer();
