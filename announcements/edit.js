jQuery(document).ready(function(){
   
    if (jQuery('#id_type').val() == 'course'){
        jQuery('#fitem_id_courseid').show();
    }
    
    jQuery('#id_type').change(function(e){
       if (jQuery(this).val() == 'course') {
           jQuery('#fitem_id_courseid').show();
       } else {
           jQuery('#fitem_id_courseid').hide();
       }
    });
    
});

