<?php

defined('MOODLE_INTERNAL') || die;

require_once($CFG->libdir.'/formslib.php');

/**
 * The form for handling editing a course.
 */
class edit_form extends moodleform {
    protected $id;
    protected $announcement;
    protected $context;
    
    /**
     * Form definition.
     */
    function definition() {
        global $CFG, $PAGE;

        $mform          = $this->_form;
        $id             = $this->_customdata['id'];
        $announcement   = $this->_customdata['announcement'];
        $editoroptions  = $this->_customdata['editoroptions'];
        $courses        = $this->_customdata['courses'];
        
        $systemcontext   = context_system::instance();
        
        $this->id  = $id;
        $this->context = $systemcontext;
        $this->announcement = $announcement;

        $mform->addElement('hidden', 'id', $id);
        $mform->setType('id', PARAM_INT);
        
        $choices = array(''=>'Select type');
        $choices['system'] = 'System';
        if (count($courses) > 0){
            $choices['course'] = 'Course';
        }
        $mform->addElement('select', 'type', get_string('type', 'local_mxschool'), $choices);
        $mform->addRule('type', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        
        $mform->addElement('select', 'courseid', get_string('course'), $courses);
        
        $mform->addElement('text', 'title', get_string('title', 'local_mxschool'), 'maxlength="254"  size="50"');
        $mform->addRule('title', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        $mform->setType('title', PARAM_TEXT);
        
        $mform->addElement('editor', 'description_editor', get_string('description', 'local_mxschool'), null, $editoroptions);
        $mform->setType('description_editor', PARAM_RAW);
        $mform->addRule('description_editor', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        
        $mform->addElement('date_time_selector', 'announcementdate', get_string('announcementdate', 'local_mxschool'), array('optional'=>true));

        $mform->addElement('date_time_selector', 'startdate', get_string('startdate', 'local_mxschool'));
        $mform->setDefault('startdate', time());
        $mform->addRule('startdate', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        
        $mform->addElement('date_time_selector', 'enddate', get_string('enddate', 'local_mxschool'), array('optional'=>true));
        
        $choices = array();
        $choices['0'] = get_string('hide');
        $choices['1'] = get_string('show');
        $mform->addElement('select', 'state', get_string('visibility', 'local_mxschool'), $choices);
        $mform->setDefault('state', 1);
    
        $this->add_action_buttons(get_string('cancel'), (($id > 0) ? get_string('save', 'local_mxschool') : get_string('create', 'local_mxschool')));

        // Finally set the current form data
        $this->set_data($announcement);
    }

    /**
     * Fill in the current page data for this course.
     */
    function definition_after_data() {
        global $DB;

        $mform = $this->_form;

    }

    /**
     * Validation.
     *
     * @param array $data
     * @param array $files
     * @return array the errors that were found
     */
    function validation($data, $files) {
        global $DB;

        $errors = parent::validation($data, $files);

        return $errors;
    }
}

