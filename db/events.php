<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool capabilities.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.edu
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$handlers = array(
    'role_assigned' => array (
        'handlerfile'     => '/local/mxschool/locallib.php',
        'handlerfunction' => array('events_handler', 'role_assigned'),
        'schedule'        => 'instant',
        'internal'        => 1,
    ),
    'role_unassigned' => array (
        'handlerfile'     => '/local/mxschool/locallib.php',
        'handlerfunction' => array('events_handler', 'role_unassigned'),
        'schedule'        => 'instant',
        'internal'        => 1,
    ),
);

