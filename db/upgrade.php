<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.edu
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

function xmldb_local_mxschool_upgrade($oldversion) {
    global $CFG, $DB;

    $dbman = $DB->get_manager();

    
    /* local_mxschool_advisors */
    
	// Define table local_mxschool_advisors to be created.
	$table = new xmldb_table('local_mxschool_advisors');

	// Adding fields to table local_mxschool_advisors.
	$table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
    $table->add_field('studentid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('currentadvisor', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('keep_current_advisor', XMLDB_TYPE_CHAR, '60', null, null, null, 'yes');
    $table->add_field('advisor1', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('advisor2', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('advisor3', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('advisor4', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('advisor5', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('finaladvisor', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('comment', XMLDB_TYPE_TEXT, null, null, null, null, null);
    $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
	$table->add_field('status', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '0');
    
	// Adding keys to table local_mxschool_advisors.
	$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

	// Conditionally launch create table for local_mxschool_advisors.
	if (!$dbman->table_exists($table)) {
		$dbman->create_table($table);
	}
    
    
    /* local_mxschool_announcements */
    
    // Define table local_mxschool_announcements to be created.
	$table = new xmldb_table('local_mxschool_announcements');

	// Adding fields to table local_mxschool_announcements.
	$table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
    $table->add_field('title', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('type', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('courseid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('description', XMLDB_TYPE_TEXT, null, null, null, null, null);
    $table->add_field('descriptionformat', XMLDB_TYPE_INTEGER, '4', null, XMLDB_NOTNULL, null, '1');
    $table->add_field('announcementdate', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('startdate', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('enddate', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
	$table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
	$table->add_field('state', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '1');
    
	// Adding keys to table local_mxschool_announcements.
	$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

	// Conditionally launch create table for local_mxschool_announcements.
	if (!$dbman->table_exists($table)) {
		$dbman->create_table($table);
	}
    
    
    /* local_mxschool_dates */
    
    // Define table local_mxschool_dates to be created.
	$table = new xmldb_table('local_mxschool_dates');

	// Adding fields to table local_mxschool_dates.
	$table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
    $table->add_field('date', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('weekendtype', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    
	// Adding keys to table local_mxschool_dates.
	$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

	// Conditionally launch create table for local_mxschool_dates.
	if (!$dbman->table_exists($table)) {
		$dbman->create_table($table);
	}
    
    
    /* local_mxschool_dorms */
    
    // Define table local_mxschool_dorms to be created.
	$table = new xmldb_table('local_mxschool_dorms');

	// Adding fields to table local_mxschool_dorms.
	$table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
    $table->add_field('abbreviation', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('name', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('gender', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('type', XMLDB_TYPE_CHAR, '255', null, null, null, 'boarding');
    $table->add_field('status', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '1');
    
	// Adding keys to table local_mxschool_dorms.
	$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

	// Conditionally launch create table for local_mxschool_dorms.
	if (!$dbman->table_exists($table)) {
		$dbman->create_table($table);
	}
    
    
    /* local_mxschool_notifications */
    
    // Define table local_mxschool_notifications to be created.
	$table = new xmldb_table('local_mxschool_notifications');

	// Adding fields to table local_mxschool_notifications.
	$table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
    $table->add_field('type', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('name', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('description', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('tags', XMLDB_TYPE_TEXT, null, null, null, null, null);
    $table->add_field('subject', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('body', XMLDB_TYPE_TEXT, null, null, null, null, null);
    $table->add_field('sender', XMLDB_TYPE_CHAR, '255', null, null, null, 'supportuser');
    $table->add_field('sendto', XMLDB_TYPE_TEXT, null, null, null, null, null);
    $table->add_field('status', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '1');
    $table->add_field('role', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('category', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('sortorder', XMLDB_TYPE_INTEGER, '5', null, XMLDB_NOTNULL, null, '0');
    
	// Adding keys to table local_mxschool_notifications.
	$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

	// Conditionally launch create table for local_mxschool_notifications.
	if (!$dbman->table_exists($table)) {
		$dbman->create_table($table);
	}
    
    
    /* local_mxschool_parents */
    
    // Define table local_mxschool_parents to be created.
	$table = new xmldb_table('local_mxschool_parents');

	// Adding fields to table local_mxschool_parents.
	$table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
    $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('childid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('name', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('address', XMLDB_TYPE_TEXT, null, null, null, null, null);
    $table->add_field('homephone', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('cellphone', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('workphone', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('email', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('timeupdated', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    
	// Adding keys to table local_mxschool_parents.
	$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

	// Conditionally launch create table for local_mxschool_parents.
	if (!$dbman->table_exists($table)) {
		$dbman->create_table($table);
	}
    
    
    /* local_mxschool_pickup_sites */
    
    // Define table local_mxschool_pickup_sites to be created.
	$table = new xmldb_table('local_mxschool_pickup_sites');

	// Adding fields to table local_mxschool_pickup_sites.
	$table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
    $table->add_field('site', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('type', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '1');
    $table->add_field('sitetype', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('status', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '1');
    
	// Adding keys to table local_mxschool_pickup_sites.
	$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

	// Conditionally launch create table for local_mxschool_pickup_sites.
	if (!$dbman->table_exists($table)) {
		$dbman->create_table($table);
	}
    
    
    /* local_mxschool_pickup_times */
    
    // Define table local_mxschool_pickup_times to be created.
	$table = new xmldb_table('local_mxschool_pickup_times');

	// Adding fields to table local_mxschool_pickup_times.
	$table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
    $table->add_field('datetime', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('type', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '1');
    $table->add_field('status', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '1');
    
	// Adding keys to table local_mxschool_pickup_times.
	$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

	// Conditionally launch create table for local_mxschool_pickup_times.
	if (!$dbman->table_exists($table)) {
		$dbman->create_table($table);
	}
    
    
    /* local_mxschool_students */
    
    // Define table local_mxschool_students to be created.
	$table = new xmldb_table('local_mxschool_students');

	// Adding fields to table local_mxschool_students.
	$table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
    $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('middle', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('preferred', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('grade', XMLDB_TYPE_CHAR, '10', null, null, null, null);
    $table->add_field('admissionsyear', XMLDB_TYPE_CHAR, '5', null, null, null, null);
    $table->add_field('gender', XMLDB_TYPE_CHAR, '10', null, null, null, null);
    $table->add_field('dorm', XMLDB_TYPE_CHAR, '15', null, null, null, null);
    $table->add_field('room', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('birthdate', XMLDB_TYPE_CHAR, '20', null, null, null, null);
    $table->add_field('overnight', XMLDB_TYPE_CHAR, '40', null, null, null, null);
    $table->add_field('driving', XMLDB_TYPE_CHAR, '40', null, null, null, null);
    $table->add_field('comment', XMLDB_TYPE_TEXT, null, null, null, null, null);
    $table->add_field('boston', XMLDB_TYPE_CHAR, '40', null, null, null, null);
    $table->add_field('advisor', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('dormphone', XMLDB_TYPE_CHAR, '20', null, null, null, null);
    $table->add_field('studentcell', XMLDB_TYPE_CHAR, '20', null, null, null, null);
    $table->add_field('maydrive', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('maygiverides', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('swimcompetent', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('swimallowed', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('boatallowed', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('status', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '1');
    
	// Adding keys to table local_mxschool_students.
	$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

	// Conditionally launch create table for local_mxschool_students.
	if (!$dbman->table_exists($table)) {
		$dbman->create_table($table);
	}
    
    
    /* local_mxschool_transport */
    
    // Define table local_mxschool_transport to be created.
	$table = new xmldb_table('local_mxschool_transport');

	// Adding fields to table local_mxschool_transport.
	$table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
    $table->add_field('studentid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('destination', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('stu_cellphone', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('depart_date_time', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('depart_transportation', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('depart_transport_type', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('depart_driver', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('depart_airport', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('depart_carrier', XMLDB_TYPE_CHAR, '30', null, null, null, null);
    $table->add_field('depart_flight', XMLDB_TYPE_CHAR, '30', null, null, null, null);
    $table->add_field('depart_customs', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('depart_train', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('depart_train_other', XMLDB_TYPE_CHAR, '30', null, null, null, null);
    $table->add_field('depart_bus', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('depart_bus_other', XMLDB_TYPE_CHAR, '30', null, null, null, null);
    $table->add_field('depart_nyc_stops', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('return_date_time', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('return_transportation', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('return_transport_type', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('return_driver', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('return_airport', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('return_carrier', XMLDB_TYPE_CHAR, '30', null, null, null, null);
    $table->add_field('return_flight', XMLDB_TYPE_CHAR, '30', null, null, null, null);
    $table->add_field('return_customs', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('return_train', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('return_train_other', XMLDB_TYPE_CHAR, '30', null, null, null, null);
    $table->add_field('return_bus', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('return_bus_other', XMLDB_TYPE_CHAR, '30', null, null, null, null);
    $table->add_field('return_nyc_stops', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('trans_type_depart', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('pickup_site_depart', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('pickup_time_depart', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('pickup_time_depart_other', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('confirmed_depart', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('trans_type_return', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('pickup_site_return', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('pickup_time_return', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('pickup_time_return_other', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('confirmed_return', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('email_sendable_depart', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('email_sent_depart', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('email_sendable_return', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('email_sent_return', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('status', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, null, '0');
    
	// Adding keys to table local_mxschool_transport.
	$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

	// Conditionally launch create table for local_mxschool_transport.
	if (!$dbman->table_exists($table)) {
		$dbman->create_table($table);
	}
    
    
    /* local_mxschool_rooming */
    
	// Define table local_mxschool_rooming to be created.
	$table = new xmldb_table('local_mxschool_rooming');

	// Adding fields to table local_mxschool_rooming.
	$table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
    $table->add_field('studentid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('dormaterequest', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('dorm', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('roomtype', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('haslivedindouble', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('preferreddoubleroommate', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('status', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('timecreate', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, null, '0');
    $table->add_field('timeupdate', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, null, '0');
    
	// Adding keys to table local_mxschool_rooming.
	$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

	// Conditionally launch create table for local_mxschool_rooming.
	if (!$dbman->table_exists($table)) {
		$dbman->create_table($table);
	}
    

    return true;
}
