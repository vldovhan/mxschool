<?php
/**
 * ELIS(TM): Enterprise Learning Intelligence Suite
 * Copyright (C) 2008-2013 Remote-Learner.net Inc (http://www.remote-learner.net)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * Mxschool
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.edu
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

function xmldb_local_mxschool_install() {
    global $CFG, $DB;

    // Insert dorms
    $dorms = array();
    $dorms[] = array('abbreviation'=>'BP', 'name'=>'Bryant-Paine', 'gender'=>'M', 'type'=>'boarding');
    $dorms[] = array('abbreviation'=>'CLAY', 'name'=>'Clay', 'gender'=>'M', 'type'=>'boarding');
    $dorms[] = array('abbreviation'=>'HAL', 'name'=>'Hallowell', 'gender'=>'F', 'type'=>'boarding');
    $dorms[] = array('abbreviation'=>'HIG', 'name'=>'Higginson', 'gender'=>'F', 'type'=>'boarding');
    $dorms[] = array('abbreviation'=>'KRA', 'name'=>'Kravis', 'gender'=>'F', 'type'=>'boarding');
    $dorms[] = array('abbreviation'=>'LB', 'name'=>'LeBaron Briggs', 'gender'=>'F', 'type'=>'boarding');
    $dorms[] = array('abbreviation'=>'PEA', 'name'=>'Peabody', 'gender'=>'F', 'type'=>'boarding');
    $dorms[] = array('abbreviation'=>'RW', 'name'=>'Robert Winsor', 'gender'=>'M', 'type'=>'boarding');
    $dorms[] = array('abbreviation'=>'LOW', 'name'=>'Lowell Day House', 'gender'=>'M', 'type'=>'day');
    $dorms[] = array('abbreviation'=>'EST', 'name'=>'Estabrook Day House', 'gender'=>'F', 'type'=>'day');
    $dorms[] = array('abbreviation'=>'WD', 'name'=>'Withdrawn', 'gender'=>'all', 'type'=>'all');
    $dorms[] = array('abbreviation'=>'ATKINS', 'name'=>'Atkins', 'gender'=>'M', 'type'=>'boarding');
    $dorms[] = array('abbreviation'=>'BAT', 'name'=>'Bateman Day House', 'gender'=>'M', 'type'=>'day');
    $dorms[] = array('abbreviation'=>'WIN', 'name'=>'Winsor Day House', 'gender'=>'F', 'type'=>'day');
    
    foreach ($dorms as $dorm){
        $new_dorm = $dorm;
        $DB->insert_record('local_mxschool_dorms', $new_dorm, false);
    }
    
    // Notifications
    $notifications = array();
    $notifications[] = array('type'=>'user', 
                             'name'=>'Select advisor', 
                             'description'=>'Send when Dean sends email to students to select advisor', 
                             'tags'=>'a:4:{i:0;s:14:"recipient name";i:1;s:15:"recipient email";i:2;s:17:"support user name";i:3;s:13:"support email";}', 
                             'subject'=>'Please select your advisor', 
                             'body'=>'Hello, [recipient name]<br><br>Please select advisor in LMS.<br><br>Best reguards,<br>[support user name]<br>', 
                             'sender'=>'supportuser', 
                             'status'=>1, 
                             'role'=>'All', 
                             'category'=>'Advisor selection', 
                             'sortorder	'=>1);
    
    $notifications[] = array('type'=>'custom', 
                             'name'=>'New Advisor selected', 
                             'description'=>'Sends when new advisor selected', 
                             'tags'=>'a:6:{i:0;s:15:"recipient email";i:1;s:12:"student name";i:2;s:16:"previous advisor";i:3;s:15:"current advisor";i:4;s:17:"support user name";i:5;s:13:"support email";}', 
                             'subject'=>'New Advisor selected for [student name]', 
                             'body'=>"Hello,<br>[student name]'s previous advisor was [previous advisor] and the new advisor is [current advisor].<br>Best reguards,<br>[support user name]", 
                             'sender'=>'supportuser', 
                             'sendto'=>'barnettrobert@yahoo.com, belichickpats.bill@gmail.com', 
                             'status'=>1, 
                             'role'=>'All', 
                             'category'=>'Advisor selection', 
                             'sortorder	'=>2);
    
    $notifications[] = array('type'=>'custom', 
                             'name'=>'Transportation record', 
                             'description'=>'Sanding then Transportation record was created or updated', 
                             'tags'=>'a:4:{i:0;s:15:"recipient email";i:1;s:12:"student name";i:2;s:17:"support user name";i:3;s:13:"support email";}', 
                             'subject'=>'Transportation record for [student name]', 
                             'body'=>"Please preview Transportation record for [student name].", 
                             'sender'=>'supportuser', 
                             'sendto'=>'barnett.rob@gmail.com, belichickpats.bill@gmail.com', 
                             'status'=>1,  
                             'category'=>'Vacation and Travel', 
                             'sortorder	'=>3);
    
    $notifications[] = array('type'=>'user', 
                             'name'=>'Departure Transportation confirmation', 
                             'description'=>'Sends when Departure Transportation confirmed', 
                             'tags'=>'a:9:{i:0;s:14:"recipient name";i:1;s:15:"recipient email";i:2;s:12:"student name";i:3;s:17:"support user name";i:4;s:13:"support email";i:5;s:19:"transportation type";i:6;s:11:"pickup time";i:7;s:11:"pickup site";i:8;s:10:"dean phone";}', 
                             'subject'=>'Departure Transportation confirmation for [student name]', 
                             'body'=>"Departure Transportation has been arranged for [student name].<br><br>A [transportation type] will pick you up at [pickup time] in front of [pickup site].<br><br>If you have any change in plans or complications with your travel please contact the Deans' Office at [dean phone].", 
                             'sender'=>'supportuser', 
                             'status'=>1,  
                             'role'=>'Student', 
                             'category'=>'Vacation and Travel', 
                             'sortorder	'=>4);
    
    $notifications[] = array('type'=>'user', 
                             'name'=>'Return Transportation confirmation', 
                             'description'=>'Sends when Return Transportation confirmed', 
                             'tags'=>'a:9:{i:0;s:14:"recipient name";i:1;s:15:"recipient email";i:2;s:12:"student name";i:3;s:17:"support user name";i:4;s:13:"support email";i:5;s:19:"transportation type";i:6;s:11:"pickup time";i:7;s:11:"pickup site";i:8;s:10:"dean phone";}', 
                             'subject'=>'Return Transportation confirmation for [student name]', 
                             'body'=>"Return Transportation has been arranged for [student name].<br><br>A [transportation type] will pick you up at [pickup time] in front of [pickup site].<br><br>If you have any change in plans or complications with your travel please contact the Deans' Office at [dean phone].", 
                             'sender'=>'supportuser', 
                             'status'=>1,  
                             'role'=>'Student', 
                             'category'=>'Vacation and Travel', 
                             'sortorder	'=>5);
    
    $notifications[] = array('type'=>'user', 
                             'name'=>'Weekend form',
                             'description'=>'Sends when Weekend form submitted',
                             'tags'=>'a:11:{i:0;s:6:"update";i:1;s:12:"student name";i:2;s:10:"today date";i:3;s:14:"departure date";i:4;s:11:"return date";i:5;s:14:"transportation";i:6;s:11:"destination";i:7;s:5:"phone";i:8;s:15:"today date time";i:9;s:7:"hohname";i:10;s:11:"hohphonenum";}',
                             'subject'=>'[update] Weekend form for [student name] ([today date]) ',
                             'body'=>"MIDDLESEX WEEKEND FORM<br>
---------------------------<br>
Name: [student name]<br>
Departing: [departure date]<br>
Returning: [return date]<br>
Transportation by: [transportation]<br>
Destination: [destination]<br>
Phone number: [phone]<br>
Form submitted on [today date]<br>
<br>
You may not leave for the weekend until you see your name on the OK list.<br>
<br>
Permission phone calls should be addressed to [hohname] @ [hohphonenum].<br>
If your plans change you must get permission from [hohname] ",
                             'sender'=>'supportuser',
                             'status'=>1,  
                             'role'=>'All',
                             'category'=>'Weekend',
                             'sortorder	'=>6);
    $notifications[] = array('type'=>'custom',
                            'name'=>'Transportation record',
                            'description'=>'Sanding then Transportation record was created or updated',
                            'tags'=>'a:4:{i:0;s:15:"recipient email";i:1;s:12:"student name";i:2;s:17:"support user name";i:3;s:13:"support email";}',
                            'subject'=>'Transportation record for [student name]',
                            'body'=>"Please preview Transportation record for [student name].",
                            'sender'=>'supportuser',
                            'sendto'=>'chuck@mxschool.edu',
                            'status'=>1,
                            'category'=>'Vacation and Travel',
                            'sortorder	'=>7);
    $notifications[] = array('type'=>'custom',
                            'name'=>'Passenger form results',
                            'description'=>'Sanding then passengers record was created or updated',
                            'tags'=>'a:8:{i:0;s:14:"passenger_name";i:1;s:11:"driver_name";i:2;s:9:"departure";i:3;s:11:"destination";i:4;s:6:"return";i:5;s:14:"permision_from";i:6;s:9:"permision";i:7;s:4:"call";}',
                            'subject'=>'Middlesex Passenger form results for [passenger_name]',
                            'body'=>"<p><font size=\"2\" face=\"Verdana\" color=\"#5E8CB2\">Middlesex Passenger Form Results:</font></p> 
              <table>
	              <tr>
		              <td bgcolor=\"#CCCCC\"><font size=\"1\" face=\"Verdana\">Passenger Name: </font></td>
		              <td>[passenger_name]</td>
	              </tr>
	              <tr>
		              <td bgcolor=\"#CCCCC\"><font size=\"1\" face=\"Verdana\">Driver Name: </font></td>
                  <td>[driver_name]</td>
	              </tr>
	              <tr>
		              <td bgcolor=\"#CCCCC\"><font size=\"1\" face=\"Verdana\">Departure Time: </font></td>
		              <td>[departure]</td>
	              </tr>
	              <tr> 
		              <td bgcolor=\"#CCCCC\"><font size=\"1\" face=\"Verdana\">Destination: </font></td>
		              <td>[destination]</td>
	              </tr>
	              <tr>
		              <td bgcolor=\"#CCCCC\"><font size=\"1\" face=\"Verdana\">Return Time: </font></td>
		              <td>[return]</td>
	              </tr>
	              <tr>
                  <td bgcolor=\"#CCCCC\"><font size=\"1\" face=\"Verdana\">Face to Face Permission Granted By: </font></td>
                  <td>[permision_from]</td>
                </tr>
	              <tr>
		              <td bgcolor=\"#CCCCC\"><font size=\"1\" face=\"Verdana\">Permission: </font></td>
		              <td>[permision]</td>
                </tr>
				<tr>
		              <td bgcolor=\"#CCCCC\"><font size=\"1\" face=\"Verdana\">Call Received: </font></td>
		              <td>[call]</td>
                </tr>
          </table>",
                            'sender'=>'supportuser',
                            'status'=>1,
                            'category'=>'Vacation and Travel',
                            'sortorder	'=>8);
    
    foreach ($notifications as $notification){
        $new_notification = $notification;
        $DB->insert_record('local_mxschool_notifications', $new_notification, false);
    }
    
    // Pickup sites
    $sites = array();
    $sites[] = array('site'=>'Text Paul 978-302-5273', 'type'=>2, 'sitetype'=>'admin', 'status'=>'1');
    $sites[] = array('site'=>'Penn Station', 'type'=>2, 'sitetype'=>'admin', 'status'=>'1');
    $sites[] = array('site'=>'Upper East Side', 'type'=>2, 'sitetype'=>'admin', 'status'=>'1');
    $sites[] = array('site'=>'Stamford, CT', 'type'=>2, 'sitetype'=>'admin', 'status'=>'1');
    $sites[] = array('site'=>'Logan', 'type'=>1, 'sitetype'=>'plane', 'status'=>'1');
    $sites[] = array('site'=>'South Station', 'type'=>1, 'sitetype'=>'train', 'status'=>'1');
    $sites[] = array('site'=>'128 Westwood', 'type'=>1, 'sitetype'=>'train', 'status'=>'1');
    $sites[] = array('site'=>'South Station', 'type'=>1, 'sitetype'=>'bus', 'status'=>'1');
    $sites[] = array('site'=>'Stamford, CT', 'type'=>1, 'sitetype'=>'nyc_direct', 'status'=>'1');
    $sites[] = array('site'=>'Upper East Side (NYC)', 'type'=>1, 'sitetype'=>'nyc_direct', 'status'=>'1');
    $sites[] = array('site'=>'Penn Station', 'type'=>1, 'sitetype'=>'nyc_direct', 'status'=>'1');
    $sites[] = array('site'=>'Logan', 'type'=>2, 'sitetype'=>'plane', 'status'=>'1');
    $sites[] = array('site'=>'South Station', 'type'=>2, 'sitetype'=>'train', 'status'=>'1');
    $sites[] = array('site'=>'128 Westwood', 'type'=>2, 'sitetype'=>'train', 'status'=>'1');
    $sites[] = array('site'=>'South Station', 'type'=>2, 'sitetype'=>'bus', 'status'=>'1');
    $sites[] = array('site'=>'Upper East Side (NYC)', 'type'=>2, 'sitetype'=>'nyc_direct', 'status'=>'1');
    $sites[] = array('site'=>'Penn Station', 'type'=>2, 'sitetype'=>'nyc_direct', 'status'=>'1');
    
    foreach ($sites as $site){
        $new_site = $site;
        $DB->insert_record('local_mxschool_pickup_sites', $new_site, false);
    }
}


