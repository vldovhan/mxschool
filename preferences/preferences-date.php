<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

require('../../../config.php');
require_once('preferences_form.php');
require_once($CFG->dirroot."/local/mxschool/classes/alerts/alerts.php");

require_login();
$systemcontext   = context_system::instance();
require_capability('local/mxschool:settings', $systemcontext);

$title = get_string('preferences_date', 'local_mxschool');

$PAGE->set_url(new moodle_url("/local/mxschool/preferences/preferences-date.php", array()));
$PAGE->navbar->add(get_string('pluginname', 'local_mxschool'), new moodle_url('/local/mxschool/index.php'));
$PAGE->navbar->add(get_string('preferences_name', 'local_mxschool'), new moodle_url('/local/mxschool/preferences/index.php'));
$PAGE->navbar->add($title);
$PAGE->requires->jquery();
$PAGE->set_pagelayout('admin');
$PAGE->set_context(context_system::instance());
$PAGE->set_title($title);
$PAGE->set_heading($title);

$datesform = new dates_form(null);
if (!$datesform->is_cancelled() && $data = $datesform->get_data()) {

    set_config('dorms_close', $data->dorms_close, 'local_mxschool');
    set_config('dorms_open', $data->dorms_open, 'local_mxschool');
    set_config('second_semester_starts', $data->second_semester_starts, 'local_mxschool');
    
    $weekands = $DB->get_records_sql("SELECT * FROM {local_mxschool_dates} WHERE weekendtype != 'open'");
    if (count($weekands)){
        foreach ($weekands as $weekand){
            $weekand->weekendtype = 'open';
            $DB->update_record('local_mxschool_dates', $weekand);
        }
    }

    $jAlert->create(array('type'=>'success', 'text'=>'Date was successfully updated'));
    redirect(new moodle_url('/local/mxschool/preferences/index.php'));
}
echo $OUTPUT->header();
echo $OUTPUT->heading($title);

$datesform->display();

echo $OUTPUT->footer();
