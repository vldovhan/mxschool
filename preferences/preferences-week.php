<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

require('../../../config.php');
require_once('preferences_form.php');
require_once($CFG->dirroot."/local/mxschool/classes/alerts/alerts.php");

require_login();
$systemcontext   = context_system::instance();
require_capability('local/mxschool:settings', $systemcontext);

$title = get_string('preferences_week', 'local_mxschool');

$PAGE->set_url(new moodle_url("/local/mxschool/user_management/preferences/preferences-week.php", array()));
$PAGE->navbar->add(get_string('pluginname', 'local_mxschool'), new moodle_url('/local/mxschool/index.php'));
$PAGE->navbar->add(get_string('preferences_name', 'local_mxschool'), new moodle_url('/local/mxschool/preferences/index.php'));
$PAGE->navbar->add($title);
$PAGE->requires->jquery();
$PAGE->set_pagelayout('admin');
$PAGE->set_context(context_system::instance());
$PAGE->set_title($title);
$PAGE->set_heading($title);

$editform = new edit_form(null);
if (!$editform->is_cancelled() && $data = $editform->get_data()) {
    unset($data->submitbutton);

    foreach($data as $date=>$type){
        if($record = $DB->get_record('local_mxschool_dates',array('date'=>$date))){
            $record->weekendtype = $type;
            $DB->update_record('local_mxschool_dates',$record);
        }else{
            $record = new stdClass();
            $record->date = $date;
            $record->time = strtotime($date);
            $record->weekendtype = $type;
            $DB->insert_record('local_mxschool_dates', $record);
        }
    }

    $jAlert->create(array('type'=>'success', 'text'=>'Date was successfully updated'));
    redirect(new moodle_url('/local/mxschool/preferences/index.php'));
}

echo $OUTPUT->header();
echo $OUTPUT->heading($title);

echo html_writer::start_tag('div', array('class' => 'mxschool-preferences-box'));
$editform->display();
echo html_writer::end_tag('div');

echo $OUTPUT->footer();
