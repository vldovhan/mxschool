<?php
define('AJAX_SCRIPT', true);
require_once('../../../config.php');

require_login();

$id		  = optional_param('id', 0, PARAM_INT);
$type	  = optional_param('type', 0, PARAM_INT);
$action = optional_param('action', '', PARAM_RAW);
$form	= (object)clean_param_array($_POST['form'], PARAM_RAW, true);

if($action == 'save-support'){	
	if ($form->name != '' and $support_user = $DB->get_record('config', array('name'=>'supportname'))){
		$support_user->value = $form->name;
		$DB->update_record('config', $support_user);
	}
	if ($form->email != '' and $support_email = $DB->get_record('config', array('name'=>'supportemail'))){
		$support_email->value = $form->email;
		$DB->update_record('config', $support_email);
	}
	if ($form->noreply != '' and $noreply_email = $DB->get_record('config', array('name'=>'noreplyaddress'))){
		$noreply_email->value = $form->noreply;
		$DB->update_record('config', $noreply_email);
	}
	purge_all_caches();
	echo time();
} elseif($action == 'adminnot-set-status' and $id){
	$status		= optional_param('status', 0, PARAM_INT);
	if($notification = $DB->get_record("local_mxschool_notifications", array('id'=>$id))){
		$notification->status = $status;
		$DB->update_record("local_mxschool_notifications", $notification);
	}
	echo time();
} elseif ($action == 'adminnot-save-form'){
	$DB->update_record("local_mxschool_notifications", $form);
	$notification = $DB->get_record("local_mxschool_notifications", array('id'=>$form->id));
	echo $notification->id;
	exit;
}	
?>


<?php if ($action == 'adminnot-load-form' and $id) : ?>
	<?php $notification = $DB->get_record("local_mxschool_notifications", array('id'=>$id)); ?>
	<?php $tag_description = array('recipient name'=>'Recipient name', 'recipient email'=>'Recipient Email', 'support user name'=>'Support User name', 'support email'=>'Support Email', 'user'=>'User fullname', 'email'=>'User Email', 'course'=>'Course name', 'content name'=>'Course content name', 'date'=>'Date value', 'student name'=>'Student name', 'Display Announcement'=>'Announcement text', 'teacher'=>'Teacher name', 'name'=>'Sender name', 'event'=>'Event text', 'student name'=>'Student name', 'previous advisor'=>'Previous advisor', 'current advisor'=>'Current advisor', 'transportation type'=>'Transportation Type', 'pickup time'=>'Pickup Time', 'pickup site'=>'Pickup Site', 'dean phone'=>'Dean Phone', 'update'=> 'Update or new request','today date'=>'like as '.date("D F j, Y"),'departure date'=>'like as '.date('m/d/Y  g:i a'),'return date'=>'like as '.date('m/d/Y  g:i a'),'transportation'=>'Transportation by','destination'=>'Destination','phone'=>'Phone','today date time'=>date('m/d/Y  g:i a'),'hohname'=>'Head of House name','hohphonenum'=>'Head of House phone number', 'passenger_name'=>'Student name','driver_name'=>'Driver','departure'=>'Departure time','return'=>'Return time','permision_from'=>'Faculty permission granted','permision'=>'Permissions verified','call'=>'Call Received');?>
    <?php //echo serialize(array('recipient name', 'recipient email', 'student name', 'support user name', 'support email', 'transportation type', 'pickup time', 'pickup site', 'dean phone')); ?>
	<div class="not-form-inner">
		<form name="adminnot-form" class="adminnot-form clearfix" id="adminnot_form_<?php echo $id; ?>" method="POST" action="<?php echo $CFG->wwwroot;?>/local/mxschool/notifications/ajax.php">
			<div class="not-form-items-f anot-form">
				<?php if ($notification->type == 'custom') : ?>
                    <div class="not-form-item">
                        <label>Send to:</label><input type="text" name="form[sendto]" id="sendto" value="<?php echo (isset($notification->sendto)) ? $notification->sendto : ''; ?>" />
                    </div>
                <?php endif; ?>
                <div class="not-form-item">
					<label>Subject:</label><input type="text" name="form[subject]" id="subject" value="<?php echo (isset($notification->subject)) ? $notification->subject : ''; ?>" />
				</div>
				<div class="not-form-item">
					<label>Body:</label><textarea name="form[body]" id="body"><?php echo (isset($notification->body)) ? $notification->body : ''; ?></textarea>
				</div>
				<div class="not-form-item">
					<input type="hidden" name="form[id]" value="<?php echo $id; ?>" />
					<input type="hidden" name="action" value="adminnot-save-form" />
					<button class="send btn btn-success" type="button">Save</button>
					<button class="cancel btn" type="button">Cancel</button>
				</div>
			</div>
			<div class="not-form-items-d">
				<div class="not-form-description">
					<span><strong>Subject</strong> - message subject</span>
					<span><strong>Body</strong> - message body text</span>
					<?php $tags = unserialize($notification->tags); ?>
					<?php if ($tags) : ?>
						<h3>Available Tags:</h3>
						<?php foreach ($tags as $tag) : ?>
							<span><strong>[<?php echo $tag; ?>]</strong> - <?php echo $tag_description[$tag]; ?></span>
						<?php endforeach; ?>
					<?php endif; ?>
				</div>
			</div>
		</form>
	</div>
	<script>
		jQuery("#adminnot_form_<?php echo $id; ?> #body").ClassyEdit();
		jQuery('#adminnot_form_<?php echo $id; ?> .not-form-item .send').click(function(event){
			jQuery.ajax({
				url: jQuery('#adminnot_form_<?php echo $id; ?>').attr('action'),
				type: "POST",
				data: jQuery('#adminnot_form_<?php echo $id; ?>').serialize(),
				beforeSend: function(){
					jQuery('#not_item_<?php echo $id; ?> .not-form-box').html('<i class="fa fa-spinner fa-spin"></i>');
				}
			}).done(function( data ) {
				jQuery('#not_item_<?php echo $id; ?> .not-form-box').addClass('center');
				jQuery('#not_item_<?php echo $id; ?> .not-form-box').html('<i class="fa fa-check green"> <span>Notification successfully saved.</span></i>');
				setTimeout(function() {
					jQuery('#not_item_<?php echo $id; ?> .not-form-box').removeClass('center');
					jQuery("#not_item_<?php echo $id; ?> .load").trigger("click");
				}, 1500);
			});
		});
		jQuery('#adminnot_form_<?php echo $id; ?> .not-form-item .cancel').click(function(event){
			jQuery("#not_item_<?php echo $id; ?> .load").trigger("click");
		});
	</script>
<?php endif; ?>

<?php exit;

