<?php

defined('MOODLE_INTERNAL') || die;

require_once($CFG->libdir.'/formslib.php');

/**
 * The form for handling editing a course.
 */
class edit_form extends moodleform {
    protected $id;
    protected $context;
    
    /**
     * Form definition.
     */
    function definition() {
        global $CFG, $PAGE;

        $mform          = $this->_form;
        $settings       = $this->_customdata['settings'];
        
        $systemcontext   = context_system::instance();
        $this->context = $systemcontext;
        
        $options = array(9=>9,10=>10,11=>11,12=>12);
        $mform->addElement('select', 'driver_grades', get_string('driver_grades', 'local_mxschool'), $options);
        $mform->addRule('driver_grades', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        $mform->getElement('driver_grades')->setMultiple(true);
        $mform->getElement('driver_grades')->setSelected(array(11, 12));
        
        $mform->addElement('textarea', 'permissions_required_text', get_string('permissions_required_text', 'local_mxschool'));
        $mform->addRule('permissions_required_text', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        $mform->setType('permissions_required_text', PARAM_RAW);

        $this->add_action_buttons(get_string('cancel'), get_string('save', 'local_mxschool'));
        
        // Finally set the current form data
        $this->set_data($settings);
    }

    /**
     * Fill in the current page data for this course.
     */
    function definition_after_data() {
        global $DB;

        $mform = $this->_form;

    }

    /**
     * Validation.
     *
     * @param array $data
     * @param array $files
     * @return array the errors that were found
     */
    function validation($data, $files) {
        global $DB;

        $errors = parent::validation($data, $files);

        return $errors;
    }
}

