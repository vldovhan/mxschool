<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

require('../../../config.php');
require ('passenger_classes.php');
require_once($CFG->dirroot."/local/mxschool/classes/alerts/alerts.php");

require_login();
$systemcontext   = context_system::instance();

$id = optional_param('id', 0, PARAM_INT);

if($id)    
    $title = get_string('update_passenger','local_mxschool');
else
    $title = get_string('create_passenger','local_mxschool');

$PAGE->set_url(new moodle_url("/local/mxschool/esignout/enter-passenger.php", array()));
$PAGE->navbar->add(get_string('pluginname', 'local_mxschool'), new moodle_url('/local/mxschool/index.php'));
$PAGE->navbar->add(get_string('driver_name', 'local_mxschool'), new moodle_url('/local/mxschool/esignout/index.php'));
$PAGE->navbar->add($title);
$PAGE->requires->jquery();
$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('admin');
$PAGE->set_title($title);
$PAGE->set_heading($title);

$form = new enter_passenger_form(null,array('id'=>$id));
if (!$form->is_cancelled() && $data = $form->get_data()) {
    require('../classes/notifications.php');
    $call_received = 0;
    if(isset($data->perm) && $data->perm == get_string('yes'))
        $call_received = 2;
    elseif(isset($data->perm) && $data->perm == get_string('no'))
        $call_received = 1;

   if($record = $DB->get_record('local_mxschool_epassenger',array('id'=>$data->id))){
       $id = $data->id;
       $record->passenger = $data->passenger;
       $record->driver = $data->driver;
       $record->driver_other = $data->driver_other;
       $record->departure_time = $data->departure_time;
       $record->destination = $data->destination;
       $record->return_time = $data->return_time;
       $record->granded_from = $data->granded_by;
       $record->permission = $data->permission;
       $record->permission = $data->permission;
       $record->call_received = $call_received;
       $DB->update_record('local_mxschool_epassenger',$record);

       $jAlert->create(array('type'=>'success', 'text'=>'Successfully updated'));
   }else{
       $record = new stdClass();
       $record->passenger = $data->passenger;
       $record->driver = $data->driver;
       $record->driver_other = $data->driver_other;
       $record->departure_time = $data->departure_time;
       $record->destination = $data->destination;
       $record->return_time = $data->return_time;
       $record->granded_from = $data->granded_by;
       $record->permission = $data->permission;
       $record->permission = $data->permission;
       $record->call_received = $call_received;
       $id = $DB->insert_record('local_mxschool_epassenger',$record);

       $jAlert->create(array('type'=>'success', 'text'=>'Successfully created'));       
   }

    $msg = new mxNotifications(8,$id);
    $msg->process();

    $student = $DB->get_record('local_mxschool_students',array('userid'=>$USER->id));
    if(isset($student->id))
        redirect(new moodle_url('/my/'));
    else
        redirect(new moodle_url('/local/mxschool/esignout/passengers.php'));
}

echo $OUTPUT->header();
echo $OUTPUT->heading($title);

echo html_writer::start_tag('div', array('class' => 'mxschool-table-box'));
$form->display();
echo html_writer::end_tag("div");

echo $OUTPUT->footer();

?>
    <script>
        $(window).ready(function () {
            $('#passenger_name').change(function () {
                var val  = $(this).val();
                if(val != '') {
                    jQuery.ajax({
                        url: M.cfg.wwwroot + "/local/mxschool/esignout/check_driver.php",
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            'id': val,
                            'action': 'check_permission'
                        },
                        complete: function (data) {
                            data = data.responseJSON;
                            if (data.permission) {
                                $('#fgroup_id_buttonar').show();
                                $('#action_buttons_perm').hide();
                            } else {
                                $('#fgroup_id_buttonar').hide();
                                $('#action_buttons_perm').show();
                            }
                        }
                    });
                }
            });
            var val  = $('#passenger_name').val();
            if(val != '') {
                jQuery.ajax({
                    url: M.cfg.wwwroot + "/local/mxschool/esignout/check_driver.php",
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        'id': val,
                        'action': 'check_permission'
                    },
                    complete: function (data) {
                        data = data.responseJSON;
                        if (data.permission) {
                            $('#fgroup_id_buttonar').show();
                            $('#action_buttons_perm').hide();
                        } else {
                            $('#fgroup_id_buttonar').hide();
                            $('#action_buttons_perm').show();
                        }
                    }
                });
            }
            $('#driver_name').change(function () {
                var val  = $(this).val();
                console.log(val);
                if(val != 'other')
                    $('#fitem_driver_other').hide();
                else
                    $('#fitem_driver_other').show();

            });
            var val  = $('#driver_name').val();
            if(val != 'other')
                $('#fitem_driver_other').hide();
            else
                $('#fitem_driver_other').show();

        });
    </script>
<?php


