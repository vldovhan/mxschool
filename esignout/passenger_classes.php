<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/tablelib.php');
require_once($CFG->libdir.'/formslib.php');

class manage_passenger_table extends table_sql {

    function __construct($uniqueid, $search) {
        global $CFG,$DB,$USER;

        parent::__construct($uniqueid);
        $student = $DB->get_record('local_mxschool_students',array('userid'=>$USER->id));

        $columns = array('passenger', 'driver', 'departure_time', 'destination', 'return_time', 'granded_from', 'permission', 'prior_approval', 'call_received', 'actions');
        $header = array('Passenger`s Name', 'Driver`s Name', 'Departure Time', 'Destination', 'Return Time', 'Faculty Permission','Permission','Prior Approval','Call Received', 'Actions');

        $this->define_columns($columns);
        $this->define_headers($header);


        $fields = "p.id, CONCAT (pu.firstname,' ',pu.lastname) as passenger, p.driver as driver_id, CONCAT (du.firstname,' ',du.lastname) as driver, p.driver_other,p.departure_time,p.destination,p.return_time,p.permission, CONCAT (fu.firstname,' ',fu.lastname) as granded_from, ps.driving as prior_approval, p.call_received ";
        $from = "{local_mxschool_epassenger} p 
                    LEFT JOIN {local_mxschool_students} ps ON ps.id=p.passenger
                    LEFT JOIN {user} pu ON pu.id=ps.userid
                    LEFT JOIN {local_mxschool_students} ds ON ds.id=p.driver
                    LEFT JOIN {user} du ON du.id=ds.userid
                    LEFT JOIN {local_mxschool_faculty} f ON f.id=p.granded_from
                    LEFT JOIN {user} fu ON fu.id=f.userid
                    ";

        $where = "p.id>0";
        if(!empty($search)){
            //LIKE '%$search%'

            $where .= " AND (CONCAT (pu.firstname,' ',pu.lastname) LIKE '%$search%' OR p.driver LIKE '%$search%' OR CONCAT (du.firstname,' ',du.lastname) LIKE '%$search%'  OR p.driver_other LIKE '%$search%'  OR p.destination LIKE '%$search%'  OR CONCAT (fu.firstname,' ',fu.lastname) LIKE '%$search%' )";
        }
        if(isset($student->id))
            $where .= " AND p.passenger=".$student->id;

        $this->set_sql($fields, $from, $where, array());
        $this->define_baseurl($CFG->wwwroot.$_SERVER['REQUEST_URI']);
    }

    function col_departure_time($values) {
        return date('m/d/Y h:i a',$values->departure_time);
    }
    function col_return_time($values) {
        return date('m/d/Y h:i a',$values->return_time);
    }
    function col_driver($values) {
        if($values->driver_id == 'other')
            return $values->driver_other;
        if($values->driver_id == 'parent')
            return "Parent";
        else
            return $values->driver;
    }
    function col_permission($values) {
        return ($values->permission == 1)?get_string('yes'):get_string('no');
    }
    function col_prior_approval($values) {
        return ($values->prior_approval == 'Any driver')?get_string('yes'):get_string('no');
    }
    function col_call_received($values) {
        switch($values->call_received){
            case 1:
                return get_string('no');
            case 2:
                return get_string('yes');
            case 0:
                return get_string('no_data','local_mxschool');
        }
    }
    /*function col_granded($values) {
        return ($values->granded)?get_string('yes'):get_string('no');
    }*/

    function col_actions($values) {
        global $OUTPUT;

        if ($this->is_downloading()){
            return '';
        }

        $strdelete  = get_string('delete');
        $stredit  = get_string('edit');

        $edit = array();
        $aurl = new moodle_url('/local/mxschool/esignout/enter-passenger.php', array('id'=>$values->id));
        $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/edit', $stredit, 'core', array('class' => 'iconsmall')));

        $aurl = new moodle_url('/local/mxschool/esignout/passengers.php', array('action'=>'delete', 'id'=>$values->id));
        $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/delete', $strdelete, 'core', array('class' => 'iconsmall')), null, array('onclick'=>"if (!confirm('Are you sure want to delete this redord?')) return false;"));


        return implode('', $edit);
    }
}

class enter_passenger_form extends moodleform {
    /**
     * Form definition.
     */
    function definition() {
        global $DB,$USER;

        $mform          = $this->_form;
        $record_id     = $this->_customdata['id'];
        $driver_grades  = get_config('local_mxschool', 'driver_grades');
        $student = $DB->get_record('local_mxschool_students',array('userid'=>$USER->id));
        
        $record = $DB->get_record('local_mxschool_epassenger',array('id'=>$record_id));

        $students_obj = $DB->get_records_sql("SELECT s.id, CONCAT (u.firstname,' ',u.lastname) as name
                                            FROM {local_mxschool_students} s
                                              LEFT JOIN {user} u ON u.id=s.userid
                                            WHERE s.dorm NOT LIKE 'wd'
                                              ");
        $students = array(''=>'Student Name');
        foreach($students_obj as $student){
            $students[$student->id] = $student->name;
        }
        
        $drivers_obj = $DB->get_records_sql("SELECT s.id, CONCAT (u.firstname,' ',u.lastname) as name
                                            FROM {local_mxschool_students} s
                                              LEFT JOIN {user} u ON u.id=s.userid
                                              LEFT JOIN {local_mxschool_dorms} d ON d.abbreviation = s.dorm
                                            WHERE s.grade IN($driver_grades) AND d.type='day'
                                              ");
        $drivers = array(''=>'Driver\'s Name','parent'=>'My parent');
        foreach($drivers_obj as $driver){
            $drivers[$driver->id] = $driver->name;
        }
        $drivers['other'] = 'Other';

        $faculty_obj = $DB->get_records_sql("SELECT f.id, CONCAT (u.firstname,' ',u.lastname) as name
                                            FROM {local_mxschool_faculty} f
                                              LEFT JOIN {user} u ON u.id=f.userid
                                              ");
        $faculties = array(''=>'Faculty Name');
        foreach($faculty_obj as $faculty){
            $faculties[$faculty->id] = $faculty->name;
        }

        if(isset($student->id)){
            $mform->addElement('hidden', 'passenger', $student->id,array('id' => 'passenger_name'));
            $mform->setType('passenger', PARAM_INT);
        }else{
            $mform->addElement('select', 'passenger', get_string('name'), $students, array('id' => 'passenger_name'));
            $mform->setType('passenger', PARAM_INT);
            $mform->addRule('passenger', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
            if(isset($record->id))
                $mform->setDefault('passenger', $record->passenger);
        }

        $mform->addElement('select', 'driver', get_string('driver_name', 'local_mxschool'), $drivers,array('id'=>'driver_name'));
        $mform->setType('driver', PARAM_RAW);
        $mform->addRule('driver', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        if(isset($record->id))
            $mform->setDefault('driver', $record->driver);

        $mform->addElement('text', 'driver_other', get_string('driver_name', 'local_mxschool'),array('id'=>'driver_other'));
        $mform->setType('driver_other', PARAM_RAW);
        if(isset($record->id))
            $mform->setDefault('driver_other', $record->driver_other);

        $times = array('startyear' => date('Y',strtotime('-6 month',get_config('local_mxschool','second_semester_starts'))),'stopyear'  => date('Y',get_config('local_mxschool','second_semester_starts')));
        $mform->addElement('date_time_selector', 'departure_time', get_string('departure_date', 'local_mxschool'), $times);
        $mform->setType('departure_time', PARAM_INT);
        $mform->addRule('departure_time', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        if(isset($record->id))
            $mform->setDefault('departure_time', $record->departure_time);

        $mform->addElement('text', 'destination', get_string('destination', 'local_mxschool'));
        $mform->setType('destination', PARAM_RAW);
        $mform->addRule('destination', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        if(isset($record->id))
            $mform->setDefault('destination', $record->destination);

        $mform->addElement('date_time_selector', 'return_time', get_string('estimated_return_time', 'local_mxschool'), $times);
        $mform->setType('return_time', PARAM_INT);
        $mform->addRule('return_time', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        if(isset($record->id))
            $mform->setDefault('return_time', $record->return_time);

        $mform->addElement('select', 'granded_by', get_string('permission_granted_by', 'local_mxschool'), $faculties);
        $mform->setType('granded_by', PARAM_INT);
        $mform->addRule('granded_by', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        if(isset($record->id))
            $mform->setDefault('granded_by', $record->granded_from);

        $mform->addElement('selectyesno', 'permission', get_string('permission_verified', 'local_mxschool'));
        $mform->addRule('permission', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        if(isset($record->id))
            $mform->setDefault('permission', $record->permission);

        $mform->addElement('hidden', 'id', $record_id);
        $mform->setType('id', PARAM_INT);

        $this->add_action_buttons(get_string('cancel'), get_string('submit_form', 'local_mxschool'));

        $elementarray=array();
        $elementarray[] =& $mform->createElement('submit', 'perm', get_string('yes'));
        $elementarray[] =& $mform->createElement('submit', 'perm', get_string('no'));


        $mform->addElement('html', '<div id="action_buttons_perm" class="action_buttons_perm">');
        $mform->addElement('html', '<p>'.get_config('local_mxschool','permissions_required_text').'</p>');
        $mform->addGroup($elementarray, 'submit_button', get_string('submit'), array(' '), false);
        $mform->addElement('html', '</div>');
    }
    function validation($data, $files) {
        global $DB;

        $errors = parent::validation($data, $files);

        if($data['driver'] == 'other' && $data['driver_other'] == ''){
            $errors['driver_other'] = get_string('required_field', 'local_mxschool');
        }

        return $errors;
    }
}
