<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

require('../../../config.php');
require ('driving_classes.php');
require_once($CFG->dirroot."/local/mxschool/classes/alerts/alerts.php");

require_login();
$systemcontext   = context_system::instance();

$id = optional_param('id', 0, PARAM_INT);

if($id)    
    $title = get_string('update_driver','local_mxschool');
else
    $title = get_string('create_driver','local_mxschool');

$PAGE->set_url(new moodle_url("/local/mxschool/esignout/enter-driving.php", array()));
$PAGE->navbar->add(get_string('pluginname', 'local_mxschool'), new moodle_url('/local/mxschool/index.php'));
$PAGE->navbar->add(get_string('driver_name', 'local_mxschool'), new moodle_url('/local/mxschool/esignout/index.php'));
$PAGE->navbar->add($title);
$PAGE->requires->jquery();
$PAGE->set_pagelayout('admin');
$PAGE->set_context(context_system::instance());
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->add_body_class('print-table');

$form = new enter_driver_form(null,array('id'=>$id));
if (!$form->is_cancelled() && $data = $form->get_data()) {
    $student = $DB->get_record('local_mxschool_students',array('userid'=>$USER->id));
    if($record = $DB->get_record('local_mxschool_edriver',array('id'=>$data->id))){
        $record->driver = $data->driver;
        $record->passenger = implode(',',$data->passengers);
        $record->departure_time = $data->departure_time;
        $record->destination = $data->destination;
        $record->return_time = $data->return_time;
        $record->granded_from = $data->granted_by;

        $DB->update_record('local_mxschool_edriver',$record);

        $jAlert->create(array('type'=>'success', 'text'=>'Successfully updated'));

        if(isset($student->id))
            redirect(new moodle_url('/my/'));
        else
            redirect(new moodle_url('/local/mxschool/esignout/drivings.php'));
    }else{
        $record = new stdClass();
        $record->timecreate = time();
        $record->driver = $data->driver;
        $record->passenger = implode(',',$data->passengers);
        $record->departure_time = $data->departure_time;
        $record->destination = $data->destination;
        $record->return_time = $data->return_time;
        $record->granded_from = $data->granted_by;
        $DB->insert_record('local_mxschool_edriver',$record);

        $jAlert->create(array('type'=>'success', 'text'=>'Successfully created'));
        
        if(isset($student->id))
            redirect(new moodle_url('/my/'));
        else
            redirect(new moodle_url('/local/mxschool/esignout/drivings.php'));
    }
}

echo $OUTPUT->header();
echo $OUTPUT->heading($title);

echo html_writer::start_tag('div', array('class' => 'mxschool-table-box'));
$form->display();
echo html_writer::end_tag("div");

echo $OUTPUT->footer();


