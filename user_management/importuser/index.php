<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Bulk user registration script from a comma separated file
 *
 * @package    tool
 * @subpackage uploaduser
 * @copyright  2004 onwards Martin Dougiamas (http://dougiamas.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../../../config.php');
require_once($CFG->libdir.'/adminlib.php');
require_once($CFG->libdir.'/csvlib.class.php');
require_once($CFG->dirroot.'/user/profile/lib.php');
require_once($CFG->dirroot.'/user/lib.php');
require_once($CFG->dirroot.'/group/lib.php');
require_once($CFG->dirroot.'/cohort/lib.php'); 
require_once('locallib.php');
require_once('user_form.php');

$iid         = optional_param('iid', '', PARAM_INT);
$previewrows = optional_param('previewrows', 10, PARAM_INT);

@set_time_limit(60*60); // 1 hour should be enough
raise_memory_limit(MEMORY_HUGE);

require_login();
require_capability('local/mxschool:import_students', context_system::instance());

$struserrenamed             = get_string('userrenamed', 'tool_uploaduser');
$strusernotrenamedexists    = get_string('usernotrenamedexists', 'error');
$strusernotrenamedmissing   = get_string('usernotrenamedmissing', 'error');
$strusernotrenamedoff       = get_string('usernotrenamedoff', 'error');
$strusernotrenamedadmin     = get_string('usernotrenamedadmin', 'error');

$struserupdated             = get_string('useraccountupdated', 'tool_uploaduser');
$strusernotupdated          = get_string('usernotupdatederror', 'error');
$strusernotupdatednotexists = get_string('usernotupdatednotexists', 'error');
$strusernotupdatedadmin     = get_string('usernotupdatedadmin', 'error');

$struseruptodate            = get_string('useraccountuptodate', 'tool_uploaduser');

$struseradded               = get_string('newuser');
$strusernotadded            = get_string('usernotaddedregistered', 'error');
$strusernotaddederror       = get_string('usernotaddederror', 'error');

$struserdeleted             = get_string('userdeleted', 'tool_uploaduser');
$strusernotdeletederror     = get_string('usernotdeletederror', 'error');
$strusernotdeletedmissing   = get_string('usernotdeletedmissing', 'error');
$strusernotdeletedoff       = get_string('usernotdeletedoff', 'error');
$strusernotdeletedadmin     = get_string('usernotdeletedadmin', 'error');

$strcannotassignrole        = get_string('cannotassignrole', 'error');

$struserauthunsupported     = get_string('userauthunsupported', 'error');
$stremailduplicate          = get_string('useremailduplicate', 'error');

$strinvalidpasswordpolicy   = get_string('invalidpasswordpolicy', 'error');
$errorstr                   = get_string('error');

$stryes                     = get_string('yes');
$strno                      = get_string('no');
$stryesnooptions = array(0=>$strno, 1=>$stryes);

$returnurl = new moodle_url('/local/mxschool/user_management/importuser/index.php');

$title = 'Import students';
$PAGE->set_url(new moodle_url("/local/mxschool/user_management/importuser/index.php", array()));
$PAGE->navbar->add(get_string('pluginname', 'local_mxschool'), new moodle_url('/local/mxschool/index.php'));
$PAGE->navbar->add(get_string('user_management', 'local_mxschool'), new moodle_url('/local/mxschool/user_management/index.php'));
$PAGE->navbar->add($title);
$PAGE->requires->jquery();
$PAGE->set_pagelayout('admin');
$PAGE->set_context(context_system::instance());
$PAGE->set_title($title);
$PAGE->set_heading($title);

$today = time();
$today = make_timestamp(date('Y', $today), date('m', $today), date('d', $today), 0, 0, 0);

// array of all valid fields for validation
$STD_FIELDS = array('id', 'username', 'email',
        'city', 'country', 'address', 
        'StuFirstName', 'StuFirstName', 'StuLastName', 'AddressLine1',
        'AddressLine2', 'AddressLine3', 'City', 'State', 'Zip', 'Country',
        'StuEMail', 'ID', 'StuMiddleName', 'StuPreferredName', 
        'Name1First', 'Name1Middle', 'Name1Last', 'Name2First', 'Name2Middle',
        'Name2Last', 'AddressCode', 'FamilyID', 'TelNo', 'OtherNoType1', 'OtherNo1',
        'OtherNoType2', 'OtherNo2', 'OtherNoType3', 'OtherNo3', 'OtherNoType4',
        'OtherNo4', 'EMail1', 'EMail2', 'AdmissionsYr', 'CurrentGrade', 'SexCode',
        'AdvisorName', 'DormID', 'DormRoom', 'DormTelNo', 'BirthDate', 'role'
    );

// Include all name fields.
$STD_FIELDS = array_merge($STD_FIELDS, get_all_user_name_fields());

$PRF_FIELDS = array();

if (empty($iid)) {
    $mform1 = new importuser_form1();

    if ($formdata = $mform1->get_data()) {
        $iid = csv_import_reader::get_new_iid('uploaduser');
        $cir = new csv_import_reader($iid, 'uploaduser');

        $content = $mform1->get_file_content('userfile');

        $readcount = $cir->load_csv_content($content, $formdata->encoding, $formdata->delimiter_name);
        $csvloaderror = $cir->get_error();
        unset($content);

        if (!is_null($csvloaderror)) {
            print_error('csvloaderror', '', $returnurl, $csvloaderror);
        }
        // test if columns ok
        $filecolumns = uu_validate_user_upload_columns($cir, $STD_FIELDS, $PRF_FIELDS, $returnurl);
        // continue to form2

    } else {
        echo $OUTPUT->header();

        echo $OUTPUT->heading(get_string('importusers', 'local_mxschool'));

        $mform1->display();
        echo $OUTPUT->footer();
        die;
    }
} else {
    $cir = new csv_import_reader($iid, 'uploaduser');
    $filecolumns = uu_validate_user_upload_columns($cir, $STD_FIELDS, $PRF_FIELDS, $returnurl);
}

$mform2 = new importuser_form2(null, array('columns'=>$filecolumns, 'data'=>array('iid'=>$iid, 'previewrows'=>$previewrows)));

// If a file has been uploaded, then process it
if ($formdata = $mform2->is_cancelled()) {
    $cir->cleanup(true);
    redirect($returnurl);

} else if ($formdata = $mform2->get_data()) {
    // Print the header
    echo $OUTPUT->header();
    echo $OUTPUT->heading(get_string('importusersresult', 'local_mxschool'));

    // verification moved to two places: after upload and into form2
    $usersnew      = 0;
    $usersupdated  = 0;
    $usersuptodate = 0; //not printed yet anywhere
    $userserrors   = 0;
    $deletes       = 0;
    $deleteerrors  = 0;
    $renames       = 0;
    $renameerrors  = 0;
    $usersskipped  = 0;
    $weakpasswords = 0;
    $countries = array_flip(get_string_manager()->get_list_of_countries());
    $dorms = array_flip($DB->get_records_menu('local_mxschool_dorms'));
    $advisors = array();
    $all_advisors = $DB->get_records_sql("SELECT f.id, CONCAT(u.firstname, ' ', u.lastname) as advisor FROM {local_mxschool_faculty} f LEFT JOIN {user} u ON u.id = f.userid AND u.deleted = 0");
    if (count($all_advisors)){
        foreach ($all_advisors as $advisor){
            $advisors[$advisor->advisor] = $advisor->id;
        }
    }
    
    // caches
    $ccache         = array(); // course cache - do not fetch all courses here, we  will not probably use them all anyway!
    $cohorts        = array();
    $rolecache      = uu_allowed_roles_cache(); // roles lookup cache
    $manualcache    = array(); // cache of used manual enrol plugins in each course
    $supportedauths = uu_supported_auths(); // officially supported plugins that are enabled

    // we use only manual enrol plugin here, if it is disabled no enrol is done
    if (enrol_is_enabled('manual')) {
        $manual = enrol_get_plugin('manual');
    } else {
        $manual = NULL;
    }

    // init csv import helper
    $cir->init();
    $linenum = 1; //column header is first line

    // init upload progress tracker
    $upt = new uu_progress_tracker();
    $upt->start(); // start table

    while ($line = $cir->next()) {
        $upt->flush();
        $linenum++;

        $upt->track('line', $linenum);

        $user = new stdClass();
        $student = new stdClass();
        $parent1 = new stdClass();
        $parent2 = new stdClass();

        // add fields to user object
        foreach ($line as $keynum => $value) {
            if (!isset($filecolumns[$keynum])) {
                // this should not happen
                continue;
            }
            $key = $filecolumns[$keynum];
            if (strpos($key, 'profile_field_') === 0) {
                //NOTE: bloody mega hack alert!!
                if (isset($USER->$key) and is_array($USER->$key)) {
                    // this must be some hacky field that is abusing arrays to store content and format
                    $user->$key = array();
                    $user->$key['text']   = $value;
                    $user->$key['format'] = FORMAT_MOODLE;
                } else {
                    $user->$key = trim($value);
                }
            } else {
                switch ($key) {
                    case 'ID':
                        $user->idnumber = trim($value);
                        break;
                    case 'StuEMail':
                        $user->email = trim($value);
                        $user->username = str_replace('@mxschool.edu', '', $user->email);
                        break;
                    case 'StuFirstName':
                        $user->firstname = trim($value);
                        break;
                    case 'StuLastName':
                        $user->lastname = trim($value);
                        break;
                    case 'AddressLine1':
                        $user->address = ($value != '') ? trim($value) : '';
                        break;
                    case 'AddressLine2':
                        $user->address .= ($value != '') ? ', '.trim($value) : '';
                        break;
                    case 'AddressLine3':
                        $user->address .= ($value != '') ? ', '.trim($value) : '';
                        break;
                    case 'State':
                        $user->address .= ($value != '') ? ', '.trim($value) : '';
                        break;
                    case 'Zip':
                        $user->address .= ($value != '') ? ', '.trim($value) : '';
                        break;
                    case 'City':
                        $user->city = trim($value);
                        break;
                    case 'Country':
                        $user->country = (isset($countries[trim($value)])) ? $countries[trim($value)] : '';
                        break;
                    case 'StuMiddleName':
                        $student->middle = trim($value);
                        break;
                    case 'StuPreferredName':
                        $student->preferred = trim($value);
                        break;
                    case 'AdmissionsYr':
                        $student->yearofgraduation = trim($value);
                        break;
                    case 'CurrentGrade':
                        $student->grade = trim($value);
                        break;
                    case 'SexCode':
                        $student->gender = trim($value);
                        break;
                    case 'AdvisorName':
                        $student->advisor = (isset($advisors[trim($value)])) ? $advisors[trim($value)] : 0;
                        break;
                    case 'DormID':
                        $student->dorm = (isset($dorms[trim($value)])) ? trim($value) : '';
                        break;
                    case 'DormRoom':
                        $student->room = trim($value);
                        break;
                    case 'DormTelNo':
                        $student->dormphone = trim($value);
                        break;
                    case 'BirthDate':
                        $student->birthdate = (trim($value) != '') ? date('m/d/Y', strtotime(trim($value))) : '';
                        break;
                    case 'Name1First':
                        if (trim($value) != ''){
                            $parent1->name = trim($value);   
                        }
                        break;
                    case 'Name1Middle':
                        if (trim($value) != ''){
                            $parent1->name = (isset($parent1->name)) ? $parent1->name.' '.trim($value) : trim($value);   
                        }
                        break;
                    case 'Name1Last':
                        if (trim($value) != ''){
                            $parent1->name = (isset($parent1->name)) ? $parent1->name.' '.trim($value) : trim($value);
                        }
                        break;
                    case 'Name2First':
                        if (trim($value) != ''){
                            $parent2->name = trim($value);   
                        }
                        break;
                    case 'Name2Middle':
                        if (trim($value) != ''){
                            $parent2->name = (isset($parent2->name)) ? $parent2->name.' '.trim($value) : trim($value);
                        }
                        break;
                    case 'Name2Last':
                        if (trim($value) != ''){
                            $parent2->name = (isset($parent2->name)) ? $parent2->name.' '.trim($value) : trim($value);
                        }
                        break;
                    case 'AddressCode':
                        if (trim($value) != ''){
                            $parent1->address = trim($value);
                            $parent2->address = trim($value);
                        }
                        break;
                    case 'TelNo':
                        if (trim($value) != ''){
                            $parent1->homephone = trim($value);
                            $parent2->homephone = trim($value);
                        }
                        break;
                    case 'EMail1':
                        if (trim($value) != ''){
                            $parent1->email = trim($value);
                        }
                        break;
                    case 'EMail2':
                        if (trim($value) != ''){
                            $parent2->email = trim($value);
                        }
                        break;
                    case 'OtherNoType1':
                        if (trim($value) != ''){
                            $parent1->OtherNoType1 = trim($value);
                        }
                        break;
                    case 'OtherNo1':
                        if (trim($value) != '' and isset($parent1->OtherNoType1)){
                            if (stristr($parent1->OtherNoType1, 'Work')){
                                $parent1->workphone = trim($value);
                            } elseif (stristr($parent1->OtherNoType1, 'Cell')){
                                $parent1->cellphone = trim($value);
                            }
                        }
                        break;
                    case 'OtherNoType2':
                        if (trim($value) != ''){
                            $parent1->OtherNoType2 = trim($value);
                        }
                        break;
                    case 'OtherNo2':
                        if (trim($value) != '' and isset($parent1->OtherNoType2)){
                            if (stristr($parent1->OtherNoType2, 'Work')){
                                $parent1->workphone = trim($value);
                            } elseif (stristr($parent1->OtherNoType2, 'Cell')){
                                $parent1->cellphone = trim($value);
                            }
                        }
                        break;
                    case 'OtherNoType3':
                        if (trim($value) != ''){
                            $parent2->OtherNoType3 = trim($value);
                        }
                        break;
                    case 'OtherNo3':
                        if (trim($value) != '' and isset($parent2->OtherNoType3)){
                            if (stristr($parent2->OtherNoType3, 'Work')){
                                $parent2->workphone = trim($value);
                            } elseif (stristr($parent2->OtherNoType3, 'Cell')){
                                $parent2->cellphone = trim($value);
                            }
                        }
                        break;
                    case 'OtherNoType4':
                        if (trim($value) != ''){
                            $parent2->OtherNoType4 = trim($value);
                        }
                        break;
                    case 'OtherNo4':
                        if (trim($value) != '' and isset($parent2->OtherNoType4)){
                            if (stristr($parent2->OtherNoType4, 'Work')){
                                $parent2->workphone = trim($value);
                            } elseif (stristr($parent2->OtherNoType4, 'Cell')){
                                $parent2->cellphone = trim($value);
                            }
                        }
                        break;
                    default:
                        $user->$key = trim($value);       
                }
            }

            if (in_array($key, $upt->columns)) {
                // default value in progress tracking table, can be changed later
                $upt->track($key, s($value), 'normal');
            }
        }
        
        // normalize username
        $user->username = clean_param($user->username, PARAM_USERNAME);
        
        // make sure we really have username
        if (empty($user->username)) {
            $upt->track('status', get_string('missingfield', 'error', 'username'), 'error');
            $upt->track('username', $errorstr, 'error');
            $userserrors++;
            continue;
        } else if ($user->username === 'guest') {
            $upt->track('status', get_string('guestnoeditprofileother', 'error'), 'error');
            $userserrors++;
            continue;
        }

        if ($user->username !== clean_param($user->username, PARAM_USERNAME)) {
            $upt->track('status', get_string('invalidusername', 'error', 'username'), 'error');
            $upt->track('username', $errorstr, 'error');
            $userserrors++;
        }

        if (empty($user->mnethostid)) {
            $user->mnethostid = $CFG->mnet_localhost_id;
        }

        if ($existinguser = $DB->get_record('user', array('username'=>$user->username, 'mnethostid'=>$user->mnethostid))) {
            $upt->track('id', $existinguser->id, 'normal', false);
        }

        if ($user->mnethostid == $CFG->mnet_localhost_id) {
            $remoteuser = false;
        } else {
            $remoteuser = true;

            // Make sure there are no changes of existing fields except the suspended status.
            foreach ((array)$existinguser as $k => $v) {
                if ($k === 'suspended') {
                    continue;
                }
                if (property_exists($user, $k)) {
                    $user->$k = $v;
                }
                if (in_array($k, $upt->columns)) {
                    if ($k === 'password' or $k === 'oldusername' or $k === 'deleted') {
                        $upt->track($k, '', 'normal', false);
                    } else {
                        $upt->track($k, s($v), 'normal', false);
                    }
                }
            }
            $user->auth = $existinguser->auth;
        }
 
        if ($existinguser) {
            $user->id = $existinguser->id;

            $upt->track('username', html_writer::link(new moodle_url('/user/profile.php', array('id'=>$existinguser->id)), s($existinguser->username)), 'normal', false);
            $upt->track('suspended', $stryesnooptions[$existinguser->suspended] , 'normal', false);
            $upt->track('auth', $existinguser->auth, 'normal', false);

            if (is_siteadmin($user->id)) {
                $upt->track('status', $strusernotupdatedadmin, 'error');
                $userserrors++;
                continue;
            }

            $existinguser->timemodified = time();
            // do NOT mess with timecreated or firstaccess here!

            //load existing profile data
            profile_load_data($existinguser);

            

            try {
                $auth = get_auth_plugin($existinguser->auth);
            } catch (Exception $e) {
                $upt->track('auth', get_string('userautherror', 'error', s($existinguser->auth)), 'error');
                $upt->track('status', $strusernotupdated, 'error');
                $userserrors++;
                continue;
            }
            $isinternalauth = $auth->is_internal();
            
            foreach($user as $k=>$v){
                if (isset($existinguser->$k) and $existinguser->$k != $v){
                    $existinguser->$k = $user->$k;
                }
            }

            // We want only users that were really updated.
            user_update_user($existinguser, false);

            $upt->track('status', $struserupdated);
            $usersupdated++;

            \core\session\manager::kill_user_sessions($existinguser->id);

        } else {
            // save the new user to the database
            $user->confirmed    = 1;
            $user->timemodified = time();
            $user->timecreated  = time();
            $user->mnethostid   = $CFG->mnet_localhost_id; // we support ONLY local accounts here, sorry

            if (!isset($user->suspended) or $user->suspended === '') {
                $user->suspended = 0;
            } else {
                $user->suspended = $user->suspended ? 1 : 0;
            }
            $upt->track('suspended', $stryesnooptions[$user->suspended], 'normal', false);

            if (empty($user->auth)) {
                $user->auth = 'ldap';
            }
            $upt->track('auth', $user->auth, 'normal', false);

            // do not insert record if new auth plugin does not exist!
            try {
                $auth = get_auth_plugin($user->auth);
            } catch (Exception $e) {
                $upt->track('auth', get_string('userautherror', 'error', s($user->auth)), 'error');
                $upt->track('status', $strusernotaddederror, 'error');
                $userserrors++;
                continue;
            }
            if (!isset($supportedauths[$user->auth])) {
                $upt->track('auth', $struserauthunsupported, 'warning');
            }

            $isinternalauth = $auth->is_internal();

            if (empty($user->email)) {
                $upt->track('email', get_string('invalidemail'), 'error');
                $upt->track('status', $strusernotaddederror, 'error');
                $userserrors++;
                continue;

            } else if ($DB->record_exists('user', array('email'=>$user->email))) {
                $upt->track('email', $stremailduplicate, 'error');
                $upt->track('status', $strusernotaddederror, 'error');
                $userserrors++;
                continue;
            }
            if (!validate_email($user->email)) {
                $upt->track('email', get_string('invalidemail'), 'warning');
            }

            $user->password = AUTH_PASSWORD_NOT_CACHED;
            $upt->track('password', '-', 'normal', false);
            
           
            $user->id = user_create_user($user, false);
            $upt->track('username', html_writer::link(new moodle_url('/user/profile.php', array('id'=>$user->id)), s($user->username)), 'normal', false);

            $upt->track('status', $struseradded);
            $upt->track('id', $user->id, 'normal', false);
            $usersnew++;

            // make sure user context exists
            context_user::instance($user->id);
        }

        if ($user->id){
            role_unassign(5, $user->id, 2, '');
            $student_record = $DB->get_record('local_mxschool_students', array('userid'=>$user->id));
            if (!$student_record){
                role_assign(5, $user->id, 2);
            }
            $student_record = $DB->get_record('local_mxschool_students', array('userid'=>$user->id));
            if ($student_record->id){
                if (count($student) > 0){
                    $sid = $student_record->id;
                    $student_record = $student;
                    $student_record->id = $sid;
                    $student_record->userid = $user->id;
                    $DB->update_record('local_mxschool_students', $student_record);
                }
                if (count($parent1)){
                    if (isset($parent1->email) and isset($parent1->name)){
                        $parent1_record = $DB->get_record('local_mxschool_parents', array('childid'=>$student_record->id, 'email'=>$parent1->email));
                        if ($parent1_record){
                            $pid = $parent1_record->id;
                            $parent1_record = $parent1;
                            $parent1_record->id = $pid;
                            $parent1_record->timeupdated = time();
                            $DB->update_record('local_mxschool_parents', $parent1_record);
                        } else {
                            $parent1_record = $parent1;
                            $parent1_record->childid = $student_record->id;
                            $parent1_record->timecreated = time();
                            $parent1_record->timeupdated = $parent1_record->timecreated;
                            $DB->insert_record('local_mxschool_parents', $parent1_record);
                        }
                    }
                }
                
                if (count($parent2)){
                    if (isset($parent2->email) and isset($parent2->name)){
                        $parent2_record = $DB->get_record('local_mxschool_parents', array('childid'=>$student_record->id, 'email'=>$parent2->email));
                        if ($parent2_record){
                            $pid = $parent2_record->id;
                            $parent2_record = $parent2;
                            $parent2_record->id = $pid;
                            $parent2_record->timeupdated = time();
                            $DB->update_record('local_mxschool_parents', $parent2_record);
                        } else {
                            $parent2_record = $parent2;
                            $parent2_record->childid = $student_record->id;
                            $parent2_record->timecreated = time();
                            $parent2_record->timeupdated = $parent2_record->timecreated;
                            $DB->insert_record('local_mxschool_parents', $parent2_record);
                        }
                    }
                }
            }        
        }
    }
    $upt->close(); // close table

    $cir->close();
    $cir->cleanup(true);

    echo $OUTPUT->box_start('boxwidthnarrow boxaligncenter generalbox', 'uploadresults');
    echo '<p>';
    
    echo get_string('userscreated', 'tool_uploaduser').': '.$usersnew.'<br />';
    echo get_string('usersupdated', 'tool_uploaduser').': '.$usersupdated.'<br />';
    if ($usersskipped) {
        echo get_string('usersskipped', 'tool_uploaduser').': '.$usersskipped.'<br />';
    }
    echo get_string('errors', 'tool_uploaduser').': '.$userserrors.'</p>';
    echo $OUTPUT->box_end();

    echo $OUTPUT->continue_button($returnurl);

    echo $OUTPUT->footer();
    die;
}

// Print the header
echo $OUTPUT->header();

echo $OUTPUT->heading(get_string('importusersresult', 'local_mxschool'));

// NOTE: this is JUST csv processing preview, we must not prevent import from here if there is something in the file!!
//       this was intended for validation of csv formatting and encoding, not filtering the data!!!!
//       we definitely must not process the whole file!

// preview table data
$data = array();
$cir->init();
$linenum = 1; //column header is first line
$noerror = true; // Keep status of any error.
while ($linenum <= $previewrows and $fields = $cir->next()) {
    $linenum++;
    $rowcols = array();
    $rowcols['line'] = $linenum;
    foreach($fields as $key => $field) {
        if (!isset($filecolumns[$key])) continue;
        $rowcols[$filecolumns[$key]] = s(trim($field));
    }
    $rowcols['username'] = '';
    $rowcols['status'] = array();

    if (isset($rowcols['username']) and $rowcols['username'] != '') {
        $stdusername = clean_param($rowcols['username'], PARAM_USERNAME);
        if ($rowcols['username'] !== $stdusername) {
            $rowcols['status'][] = get_string('invalidusernameupload');
        }
        if ($userid = $DB->get_field('user', 'id', array('username'=>$stdusername, 'mnethostid'=>$CFG->mnet_localhost_id))) {
            $rowcols['username'] = html_writer::link(new moodle_url('/user/profile.php', array('id'=>$userid)), $rowcols['username']);
        }
    } elseif (isset($rowcols['StuEMail'])) {
        if (!validate_email($rowcols['StuEMail'])) {
            $rowcols['status'][] = get_string('invalidemail');
        }
        $stdusername = str_replace('@mxschool.edu', '', $rowcols['StuEMail']);
        $stdusername_clean = clean_param($stdusername, PARAM_USERNAME);
        if ($stdusername !== $stdusername_clean) {
            $rowcols['status'][] = get_string('invalidusernameupload');
        }
        if ($userid = $DB->get_field('user', 'id', array('username'=>$stdusername_clean, 'mnethostid'=>$CFG->mnet_localhost_id))) {
            $rowcols['username'] = html_writer::link(new moodle_url('/user/profile.php', array('id'=>$userid)), $stdusername_clean);
        } elseif ($stdusername == $stdusername_clean) {
            $rowcols['username'] = $stdusername_clean;
        } else {
            $rowcols['username'] = '';
        }
        
    } else {
        $rowcols['status'][] = get_string('missingusername');
    }
    
    if (isset($rowcols['email'])) {
        if (!validate_email($rowcols['email'])) {
            $rowcols['status'][] = get_string('invalidemail');
        }
        if ($DB->record_exists('user', array('email'=>$rowcols['email']))) {
            $rowcols['status'][] = $stremailduplicate;
        }
    }
    
    if (isset($rowcols['city'])) {
        $rowcols['city'] = $rowcols['city'];
    }
    // Check if rowcols have custom profile field with correct data and update error state.
    $noerror = uu_check_custom_profile_data($rowcols) && $noerror;
    $rowcols['status'] = implode('<br />', $rowcols['status']);
    $data[] = $rowcols;
}
if ($fields = $cir->next()) {
    $data[] = array_fill(0, count($filecolumns) + 3, '...');
}
$cir->close();

$table = new html_table();
$table->id = "uupreview";
$table->attributes['class'] = 'generaltable';
$table->tablealign = 'center';
$table->summary = get_string('uploaduserspreview', 'tool_uploaduser');
$table->head = array();
$table->data = $data;

$table->head[] = get_string('uucsvline', 'tool_uploaduser');
foreach ($filecolumns as $column) {
    $table->head[] = $column;
}
$table->head[] = 'Username';
$table->head[] = get_string('status');

echo html_writer::tag('div', html_writer::table($table), array('class'=>'flexible-wrap', 'style'=>'overflow:auto; margin-bottom:25px;'));

// Print the form if valid values are available
if ($noerror) {
    $mform2->display();
}
echo $OUTPUT->footer();
die;

