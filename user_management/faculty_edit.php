<?php
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool capabilities.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.edu
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


require('../../../config.php');
require_once('faculty_edit_form.php');
require_once('../lib.php');
require_once($CFG->dirroot."/local/mxschool/classes/alerts/alerts.php");

require_login();
$systemcontext   = context_system::instance();
require_capability('local/mxschool:manage_users', $systemcontext);

$id = required_param('id', PARAM_INT); // Faculties id.

$user = null;
$faculty = $DB->get_record('local_mxschool_faculty', array('id'=>$id));
if ($faculty){
    $user = $DB->get_record('user', array('id'=>$faculty->userid));
}

$title = get_string('edit_faculty', 'local_mxschool').' '.fullname($user);
$PAGE->set_url(new moodle_url("/local/mxschool/user_management/faculty_edit.php", array('id'=>$id)));
$PAGE->navbar->add(get_string('pluginname', 'local_mxschool'), new moodle_url('/local/mxschool/index.php'));
$PAGE->navbar->add(get_string('user_management', 'local_mxschool'), new moodle_url('/local/mxschool/user_management/index.php'));
$PAGE->navbar->add(get_string('manage_faculties', 'local_mxschool'), new moodle_url('/local/mxschool/user_management/faculties.php'));
$PAGE->navbar->add($title);
$PAGE->requires->jquery();
$PAGE->set_pagelayout('admin');
$PAGE->set_context(context_system::instance());
$PAGE->set_title($title);
$PAGE->set_heading($title);

$args = array(
    'id'         => $id,
    'faculty'    => $faculty,
    'user'       => $user,
    'dorms'      => get_dorms_list()
);
$editform = new edit_form(null, $args);

if ($editform->is_cancelled()) {
    redirect(new moodle_url('/local/mxschool/user_management/faculties.php'));
} else if ($data = $editform->get_data()) {
    $faculty = $data;

    $DB->update_record('local_mxschool_faculty', $faculty);

    $user = $DB->get_record('user', array('id'=>$data->userid));
    if ($user){
        if(!empty($data->firstname))
            $user->firstname = $data->firstname;
        if(!empty($data->lastname))
            $user->lastname = $data->lastname;
        if(!empty($data->email))
            $user->email = $data->email;
        $user->phone1 = $data->phone1;
        $DB->update_record('user', $user);
    }

    $jAlert->create(array('type'=>'success', 'text'=>'Faculty member was successfully updated'));
    redirect(new moodle_url('/local/mxschool/user_management/faculties.php'));
}


echo $OUTPUT->header();
echo $OUTPUT->heading($title);

$editform->display();

echo $OUTPUT->footer();
