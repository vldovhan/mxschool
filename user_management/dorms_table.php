<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/tablelib.php');

class dorms_table extends table_sql {

    
    function __construct($uniqueid, $search, $filter) {
		global $CFG, $USER, $DB;

        parent::__construct($uniqueid);
        
        $columns = array('name', 'abbreviation', 'gender', 'type', 'actions');
        $headers = array(
            get_string('name', 'local_mxschool'),
            get_string('abbreviation', 'local_mxschool'),
            get_string('gender', 'local_mxschool'),
            get_string('type', 'local_mxschool'),
            get_string('actions', 'local_mxschool')
        );

        $this->define_columns($columns);
        $this->define_headers($headers);
        
        $sql_search = ($search) ? " AND (md.name LIKE '%$search%' OR md.abbreviation LIKE '%$search%' OR md.gender LIKE '%$search%')" : "";
        
        $fields = "md.id, md.name, md.abbreviation, md.gender, md.type, md.status ";
        $from = "{local_mxschool_dorms} md";
        $where = 'md.id > 0 '.$sql_search;
        $this->set_sql($fields, $from, $where, array());
        $this->define_baseurl("$CFG->wwwroot/local/mxschool/user_management/dorms.php?search=".$search);
    }
    
    function col_gender($values) {
      $output = '';
      $output = ucfirst($values->gender);
      return $output;
    }
    
    function col_type($values) {
      $output = '';
      $output = ucfirst($values->type);
      return $output;
    }
    
    function col_actions($values) {
      global $OUTPUT, $PAGE;
        
        if ($this->is_downloading()){
            return '';
        }
        
      $strdelete  = get_string('delete');
      $stredit  = get_string('edit');
      $strpreview  = get_string('preview', 'local_mxschool');
      $enable  = get_string('enable', 'local_mxschool');
      $disable  = get_string('disable', 'local_mxschool');
      
        $edit = array();
        
        if ($values->status > 0){
            $aurl = new moodle_url('/local/mxschool/user_management/dorms.php', array('action'=>'hide', 'id'=>$values->id));
            $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/hide', $disable, 'core', array('class' => 'iconsmall')));
        } else {
            $aurl = new moodle_url('/local/mxschool/user_management/dorms.php', array('action'=>'show', 'id'=>$values->id));
            $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/show', $enable, 'core', array('class' => 'iconsmall')));
        }
                
        $aurl = new moodle_url('/local/mxschool/user_management/dorm_edit.php', array('id'=>$values->id));
        $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/edit', $stredit, 'core', array('class' => 'iconsmall')));

        $aurl = new moodle_url('/local/mxschool/user_management/dorms.php', array('action'=>'delete', 'id'=>$values->id));
        $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/delete', $strdelete, 'core', array('class' => 'iconsmall')), null, array('onclick'=>"if (!confirm('Are you sure want to delete this redord?')) return false;"));
        
      return implode('', $edit);
    }
}
