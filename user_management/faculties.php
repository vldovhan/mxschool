<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

require('../../../config.php');
require ('faculties_table.php');
require ('../lib.php');


$search     = optional_param('search', '', PARAM_RAW);
$download   = optional_param('download', '', PARAM_ALPHA);
$filter     = optional_param('filter', 0, PARAM_INT);
$action     = optional_param('action', '', PARAM_RAW);
$id         = optional_param('id', 0, PARAM_INT);
$page         = optional_param('page', 1, PARAM_INT);

require_login();
$systemcontext   = context_system::instance();
require_capability('local/mxschool:manage_users', $systemcontext);

$table = new faculties_table('faculties_table', $search, $filter,$download,$page);
$table->is_collapsible = false;
//$table->is_downloading($download, 'faculties', 'Faculties');
if ($table->is_downloading()){
    $table->out(20, true);
    die();
}

require_once($CFG->dirroot."/local/mxschool/classes/alerts/alerts.php");
$title = get_string('manage_faculties', 'local_mxschool');

if (($action == 'hide' || $action == 'show') and $id) {
    $faculty = $DB->get_record('local_mxschool_faculty', array('id'=>$id));
    $faculty->available = !$faculty->available;
    $DB->update_record('local_mxschool_faculty', $faculty);

    $jAlert->create(array('type'=>'success', 'text'=>'Faculty member was successfully updated'));
    redirect(new moodle_url("/local/mxschool/user_management/faculties.php",array('page'=>$page)));
}elseif ($action == 'delete' and $id) {
    $faculty = $DB->get_record('local_mxschool_faculty', array('id'=>$id));
    if ($faculty){
        $user = $DB->get_record('user', array('id'=>$faculty->userid));
        if ($user){
            delete_user($user);
        }
    }
    $DB->delete_records('local_mxschool_faculty', array('id'=>$id));
    $jAlert->create(array('type'=>'success', 'text'=>'Faculty member was successfully deleted'));
    redirect(new moodle_url("/local/mxschool/user_management/faculties.php",array('page'=>$page)));
}

$PAGE->set_url(new moodle_url("/local/mxschool/user_management/faculties.php", array()));
$PAGE->navbar->add(get_string('pluginname', 'local_mxschool'), new moodle_url('/local/mxschool/index.php'));
$PAGE->navbar->add(get_string('user_management', 'local_mxschool'), new moodle_url('/local/mxschool/user_management/index.php'));
$PAGE->navbar->add($title);
$PAGE->requires->jquery();
$PAGE->set_pagelayout('admin');
$PAGE->set_context(context_system::instance());
$PAGE->set_title($title);
$PAGE->set_heading($title);



echo $OUTPUT->header();
echo $OUTPUT->heading($title);
echo html_writer::start_tag("form",  array("action"=> $PAGE->url, 'class'=>'mxschool-search-form'));
echo html_writer::start_tag("label",  array());
echo html_writer::empty_tag('input', array('type' => 'text', 'name' => 'search', 'placeholder' => get_string('search').' ...', 'value' => $search));
echo html_writer::empty_tag('input', array('type' => 'submit', 'value' => get_string('search')));
echo html_writer::empty_tag('input', array('type' => 'button', 'value' => get_string('assign_faculties', 'local_mxschool'), 'onclick'=>'location="'.$CFG->wwwroot.'/admin/roles/assign.php?contextid=2&roleid=10"'));
echo html_writer::empty_tag('input', array('type' => 'button', 'value' => get_string('create_user', 'local_mxschool'), 'onclick'=>'location="'.$CFG->wwwroot.'/user/editadvanced.php?id=-1"'));
echo html_writer::end_tag("label");
echo html_writer::end_tag("form");

echo html_writer::start_tag('div', array('class' => 'mxschool-table-box'));
$table->out(20, true);
echo html_writer::end_tag("div");

echo $OUTPUT->footer();
