<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

require('../../../config.php');
//require_once('lib.php');

require_login();
$systemcontext   = context_system::instance();
require_capability('local/mxschool:manage_users', $systemcontext);

$title = get_string('user_management', 'local_mxschool');


/*$students = $DB->get_records_sql("SELECT s.id as userid, mp.Name, mp.Address, mp.HomePhone, mp.CellPhone, mp.WorkPhone, mp.Email 
                                    FROM {local_mxschool_students} s
                                        LEFT JOIN {user} u ON u.id = s.userid
                                        LEFT JOIN {mx_students} ms ON ms.Email = u.email
                                        LEFT JOIN {mx_parents} mp ON mp.ChildID = ms.ID");
echo '<pre>';
foreach ($students as $student){
    $new_parent = new stdClass();
    $new_parent->childid = $student->userid;
    $new_parent->name = $student->name;
    $new_parent->address = $student->address;
    $new_parent->homephone = $student->homephone;
    $new_parent->cellphone = $student->cellphone;
    $new_parent->workphone = $student->workwhone;
    $new_parent->email = $student->email;
    $new_parent->timecreated = time();
    $new_parent->timeupdated = time();
    $new_parent->id = $DB->insert_record('local_mxschool_parents', $new_parent);
    echo $new_parent->id.'<hr />';
}*/




$PAGE->set_url(new moodle_url("/local/mxschool/user_management/index.php", array()));
$PAGE->navbar->add(get_string('pluginname', 'local_mxschool'), new moodle_url('/local/mxschool/index.php'));
$PAGE->navbar->add($title);
$PAGE->set_pagelayout('course');
$PAGE->set_context(context_system::instance());
$PAGE->set_title($title);
$PAGE->set_heading($title);

echo $OUTPUT->header();
echo $OUTPUT->heading($title, 2);

echo html_writer::start_tag('div', array('class' => 'mxschool-box'));
    echo html_writer::start_tag('ul', array('class' => 'mxschool-manage-menu'));
        echo html_writer::tag('li', html_writer::link(new moodle_url('/local/mxschool/user_management/students.php'), get_string('manage_students', 'local_mxschool')));
        echo html_writer::tag('li', html_writer::link(new moodle_url('/local/mxschool/user_management/faculties.php'), get_string('manage_faculties', 'local_mxschool')));
        echo html_writer::tag('li', html_writer::link(new moodle_url('/local/mxschool/user_management/dorms.php'), get_string('manage_dorms', 'local_mxschool')));
        echo html_writer::tag('li', html_writer::link(new moodle_url('/local/mxschool/user_management/parents.php'), get_string('manage_parents', 'local_mxschool')));
        echo html_writer::tag('li', html_writer::link(new moodle_url('/local/mxschool/user_management/tutors.php'), get_string('manage_tutors', 'local_mxschool')));
    echo html_writer::end_tag('ul');
echo html_writer::end_tag('div', array('class' => 'mxschool-box'));

echo $OUTPUT->footer();

