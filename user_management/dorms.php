<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

require('../../../config.php');
require ('dorms_table.php');
require ('../lib.php');
require_once($CFG->dirroot."/local/mxschool/classes/alerts/alerts.php");

$search     = optional_param('search', '', PARAM_RAW);
$filter     = optional_param('filter', 0, PARAM_INT);
$action     = optional_param('action', '', PARAM_RAW);
$id         = optional_param('id', 0, PARAM_INT);

require_login();
$systemcontext   = context_system::instance();
require_capability('local/mxschool:manage_users', $systemcontext);

$title = get_string('manage_dorms', 'local_mxschool');

if (($action == 'hide' || $action == 'show') and $id) {
    $dorm = $DB->get_record('local_mxschool_dorms', array('id'=>$id));
    $dorm->status = !$dorm->status;
    $DB->update_record('local_mxschool_dorms', $dorm);

    $jAlert->create(array('type'=>'success', 'text'=>'Dorm was successfully updated'));
    redirect(new moodle_url("/local/mxschool/user_management/dorms.php"));
} elseif ($action == 'delete' and $id) {
    $dorm = $DB->get_record('local_mxschool_dorms', array('id'=>$id));
    if ($dorm){
        $DB->delete_records('local_mxschool_dorms', array('id'=>$dorm->id));
    }    
    
    $jAlert->create(array('type'=>'success', 'text'=>'Dorm was successfully deleted'));
    redirect(new moodle_url("/local/mxschool/user_management/dorms.php"));
}

$PAGE->set_url(new moodle_url("/local/mxschool/user_management/dorms.php", array()));
$PAGE->navbar->add(get_string('pluginname', 'local_mxschool'), new moodle_url('/local/mxschool/index.php'));
$PAGE->navbar->add(get_string('user_management', 'local_mxschool'), new moodle_url('/local/mxschool/user_management/index.php'));
$PAGE->navbar->add($title);
$PAGE->requires->jquery();
$PAGE->set_pagelayout('admin');
$PAGE->set_context(context_system::instance());
$PAGE->set_title($title);
$PAGE->set_heading($title);

$table = new dorms_table('dorms_table', $search, $filter);
$table->is_collapsible = false;

echo $OUTPUT->header();
echo $OUTPUT->heading($title);
echo html_writer::start_tag("form",  array("action"=> $PAGE->url, 'class'=>'mxschool-search-form'));
echo html_writer::start_tag("label",  array());
echo html_writer::empty_tag('input', array('type' => 'text', 'name' => 'search', 'placeholder' => get_string('search').' ...', 'value' => $search));
echo html_writer::empty_tag('input', array('type' => 'submit', 'value' => get_string('search')));
echo html_writer::empty_tag('input', array('type' => 'button', 'value' => get_string('create_dorm', 'local_mxschool'), 'onclick'=>'location="'.$CFG->wwwroot.'/local/mxschool/user_management/dorm_edit.php"'));
echo html_writer::end_tag("label");
echo html_writer::end_tag("form");

echo html_writer::start_tag('div', array('class' => 'mxschool-table-box'));
$table->out(20, true);
echo html_writer::end_tag("div");

echo $OUTPUT->footer();
