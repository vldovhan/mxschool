<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool config settings script.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.edu
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

$settings = new admin_settingpage('local_mxschool', get_string('pluginname', 'local_mxschool'));
$ADMIN->add('localplugins', $settings);

$ADMIN->add('root', new admin_category('mxschooldroot', get_string('mxschooldroot', 'local_mxschool')));
				
$ADMIN->add('mxschooldroot', new admin_externalpage('advisor_selection', get_string('advisor_selection', 'local_mxschool'), $CFG->wwwroot.'/local/mxschool/advisor_selection/index.php', 'local/mxschool:view'));

$ADMIN->add('mxschooldroot', new admin_externalpage('vacationandtravel', get_string('vacationandtravel', 'local_mxschool'), $CFG->wwwroot.'/local/mxschool/vacationandtravel/index.php', 'local/mxschool:view'));

$ADMIN->add('mxschooldroot', new admin_externalpage('rooming', get_string('rooming_name', 'local_mxschool'), $CFG->wwwroot.'/local/mxschool/rooming/index.php', 'local/mxschool:view'));

$ADMIN->add('mxschooldroot', new admin_externalpage('user_management', get_string('user_management', 'local_mxschool'), $CFG->wwwroot.'/local/mxschool/user_management/index.php', 'local/mxschool:view'));

$ADMIN->add('mxschooldroot', new admin_externalpage('announcements', get_string('announcements', 'local_mxschool'), $CFG->wwwroot.'/local/mxschool/announcements/index.php', 'local/mxschool:view'));

$ADMIN->add('mxschooldroot', new admin_externalpage('notifications_mx', get_string('notifications', 'local_mxschool'), $CFG->wwwroot.'/local/mxschool/notifications/settings.php', 'local/mxschool:view'));

$ADMIN->add('mxschooldroot', new admin_externalpage('preferences', get_string('preferences_name', 'local_mxschool'), $CFG->wwwroot.'/local/mxschool/preferences/index.php', 'local/mxschool:view'));

$ADMIN->add('mxschooldroot', new admin_externalpage('tutors', get_string('tutors_name', 'local_mxschool'), $CFG->wwwroot.'/local/mxschool/tutors/index.php', 'local/mxschool:view'));

$ADMIN->add('mxschooldroot', new admin_externalpage('checkin', get_string('checkin_name', 'local_mxschool'), $CFG->wwwroot.'/local/mxschool/check-in/index.php', 'local/mxschool:view'));

$ADMIN->add('mxschooldroot', new admin_externalpage('weekend', get_string('weekend_name', 'local_mxschool'), $CFG->wwwroot.'/local/mxschool/weekend/index.php', 'local/mxschool:view'));

$ADMIN->add('mxschooldroot', new admin_externalpage('driving', get_string('driving_name', 'local_mxschool'), $CFG->wwwroot.'/local/mxschool/driving/index.php', 'local/mxschool:view'));

$ADMIN->add('mxschooldroot', new admin_externalpage('esignout', get_string('esignout_name', 'local_mxschool'), $CFG->wwwroot.'/local/mxschool/esignout/index.php', 'local/mxschool:settings'));



