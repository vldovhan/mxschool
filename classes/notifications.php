<?php

class mxNotifications {
    protected $_id;
    protected $_params;
    
    /**
     * Form definition.
     */
    function __construct($id = 0, $params = array()) {
        $this->_id = $id;
        $this->_params = $params;
    }
    
    function rebuild($id = 0, $params = array()) {
        $this->_id = $id;
        $this->_params = $params;
    }

    function process(){
        global $DB, $CFG, $USER;
        $params = (object)$this->_params;
        if ($this->_id){
            $id = $this->_id;
            $notification = $DB->get_record('local_mxschool_notifications', array('id'=>$id));
            $site = get_site();
            $supportuser = core_user::get_support_user();
            $message = new stdClass();
            if ($notification->id and $notification->status > 0){
                switch ($id) {
                    case 1 : 
                        $items = $DB->get_records_sql("SELECT s.id, s.userid, CONCAT(u.firstname, ' ', u.lastname) as student, u.email FROM {local_mxschool_students} s LEFT JOIN {user} u ON u.id = s.userid WHERE s.id NOT IN (SELECT mas.studentid FROM {local_mxschool_advisors} mas ORDER BY s.id)");
                        if (count($items)){
                            foreach ($items as $item){
                                $message->params = array('recipient name'=>$item->student, 'recipient email'=>$item->email, 'support user name'=>$supportuser->firstname.' '.$supportuser->lastname, 'support email'=>$supportuser->email);
                                if (filter_var($item->email, FILTER_VALIDATE_EMAIL)) {
                                    $message->to = $DB->get_record('user', array('id'=>$item->userid));
                                    $message->to->email = 'cjmcdonald@mxschool.edu';
                                    $message->from = $supportuser;
                                    $message->subject = $this->generate_text($notification->subject, $message->params, $notification->tags);
                                    $message->message = $this->generate_text($notification->body, $message->params, $notification->tags);
                                    email_to_user($message->to, $message->from, $message->subject, $message->message, $message->message);
                                }
                                break;
                            }
                        } $send_email = false;
                        break;
                    case 2 : 
                        if (isset($params->item)){
                            if ($notification->sendto != '') {
                                $sendto_arr = explode(',', $notification->sendto);
                                if (count($sendto_arr) > 0){
                                    foreach ($sendto_arr as $sendto){
                                        $sendto = trim($sendto);
                                        if (filter_var($sendto, FILTER_VALIDATE_EMAIL)) {
                                            $message->params = array('recipient email'=>$sendto, 'support user name'=>$supportuser->firstname.' '.$supportuser->lastname, 'support email'=>$supportuser->email, 'student name'=>$params->item->student, 'previous advisor'=>$params->item->previous_advisor, 'current advisor'=>$params->item->current_advisor);
                                            
                                            $message->to = $DB->get_record('user', array('id'=>2));
                                            //$message->to->email = $sendto;
                                            $message->to->email = 'cjmcdonald@mxschool.edu';
                                            $message->from = $supportuser;
                                            $message->subject = $this->generate_text($notification->subject, $message->params, $notification->tags);
                                            $message->message = $this->generate_text($notification->body, $message->params, $notification->tags);
                                            email_to_user($message->to, $message->from, $message->subject, $message->message, $message->message);
                                        }
                                    }
                                }
                            }
                        }
                        $send_email = false;
                        break;
                    case 3 : 
                        if (isset($params->item)){
                            if ($notification->sendto != '') {
                                $sendto_arr = explode(',', $notification->sendto);
                                if (count($sendto_arr) > 0){
                                    foreach ($sendto_arr as $sendto){
                                        $sendto = trim($sendto);
                                        if (filter_var($sendto, FILTER_VALIDATE_EMAIL)) {
                                            $message->params = array('recipient email'=>$sendto, 'support user name'=>$supportuser->firstname.' '.$supportuser->lastname, 'support email'=>$supportuser->email, 'student name'=>$params->item->student);
                                            $message->to = $DB->get_record('user', array('id'=>2));
                                            //$message->to->email = $sendto;
                                            $message->to->email = 'cjmcdonald@mxschool.edu';
                                            $message->from = $supportuser;
                                            $message->subject = $this->generate_text($notification->subject, $message->params, $notification->tags);
                                            $message->message = $this->generate_text($notification->body, $message->params, $notification->tags);
                                            email_to_user($message->to, $message->from, $message->subject, $message->message, $message->message);
                                        }
                                    }
                                }
                            }
                        }
                        $send_email = false;
                        break;
                    case 7 :
                        if (isset($params->item)){
                                $sendto = trim($params->item->email);
                                        if (filter_var($sendto, FILTER_VALIDATE_EMAIL)) {
                                            $message->params = array('recipient email'=>$sendto, 'support user name'=>$supportuser->firstname.' '.$supportuser->lastname, 'support email'=>$supportuser->email, 'student name'=>$params->item->student);
                                            $message->to = $DB->get_record('user', array('id'=>2));
                                            //$message->to->email = $sendto;
                                            $message->to->email = 'cjmcdonald@mxschool.edu';
                                            $message->from = $supportuser;
                                            $message->subject = $this->generate_text($notification->subject, $message->params, $notification->tags);
                                            $message->message = $this->generate_text($notification->body, $message->params, $notification->tags);
                                            email_to_user($message->to, $message->from, $message->subject, $message->message, $message->message);
                                        }
                        }
                        $send_email = false;
                        break;
                    case 4 : 
                        if (isset($params->item)){
                            $item = $params->item;
                            $dean_phone = get_config('local_mxschool', 'deans_phone');
                            $trans_types = array(''=>'', 'car'=>'Car', 'van'=>'Van', 'school transport'=>'School Transport', 'none'=>'None Needed');
                            $message->params = array('recipient name'=>$item->student, 'recipient email'=>$item->email, 'support user name'=>$supportuser->firstname.' '.$supportuser->lastname, 'support email'=>$supportuser->email, 'student name'=>$item->student, 'transportation type'=>((isset($trans_types[$item->trans_type_depart])) ? $trans_types[$item->trans_type_depart] : ''), 'pickup time'=>(($item->pickup_time_depart_other > 0) ? date('m/d/Y h:i a', $item->pickup_time_depart_other) : date('m/d/Y h:i a', $item->depart_date_time)), /*$item->pickup_time_depart_other) : date('m/d/Y h:i a', $item->ptime)),*/ 'pickup site'=>(($item->pickup_site != '') ? $item->pickup_site : '-'), 'dean phone'=>$dean_phone);
                            if (filter_var($item->email, FILTER_VALIDATE_EMAIL)) {
                                $message->to = $DB->get_record('user', array('id'=>$item->userid));
                                $message->to->email = 'cjmcdonald@mxschool.edu';
                                $message->from = $supportuser;
                                $message->subject = $this->generate_text($notification->subject, $message->params, $notification->tags);
                                $message->message = $this->generate_text($notification->body, $message->params, $notification->tags);
                                email_to_user($message->to, $message->from, $message->subject, $message->message, $message->message);
                            }
                        } $send_email = false;
                        break;
                    case 5 : 
                        if (isset($params->item)){
                            $item = $params->item;
                            $dean_phone = get_config('local_mxschool', 'deans_phone');
                            $trans_types = array(''=>'', 'car'=>'Car', 'van'=>'Van', 'school transport'=>'School Transport', 'none'=>'None Needed');
                            $message->params = array('recipient name'=>$item->student, 'recipient email'=>$item->email, 'support user name'=>$supportuser->firstname.' '.$supportuser->lastname, 'support email'=>$supportuser->email, 'student name'=>$item->student, 'transportation type'=>((isset($trans_types[$item->trans_type_return])) ? $trans_types[$item->trans_type_return] : ''), 'pickup time'=>(($item->pickup_time_return_other > 0) ? date('m/d/Y h:i a', $item->pickup_time_return_other) : date('m/d/Y h:i a', $item->return_date_time)), /*'pickup time'=>(($item->pickup_time_return_other > 0) ? date('m/d/Y h:i a', $item->pickup_time_return_other) : date('m/d/Y h:i a', $item->ptime)),*/ 'pickup site'=>(($item->pickup_site != '') ? $item->pickup_site : '-'), 'dean phone'=>$dean_phone);
                            if (filter_var($item->email, FILTER_VALIDATE_EMAIL)) {
                                $message->to = $DB->get_record('user', array('id'=>$item->userid));
                                $message->to->email = 'cjmcdonald@mxschool.edu';
                                $message->from = $supportuser;
                                $message->subject = $this->generate_text($notification->subject, $message->params, $notification->tags);
                                $message->message = $this->generate_text($notification->body, $message->params, $notification->tags);
                                email_to_user($message->to, $message->from, $message->subject, $message->message, $message->message);
                            }
                        } $send_email = false;
                        break;
                    case 6 :
                        $data = $this->_params;
                        $hoh = $DB->get_record_sql('SELECT u.*
                                        FROM {local_mxschool_faculty} f
                                          LEFT JOIN {user} u ON u.id=f.userid 
                                        WHERE f.dorm=:dorm AND f.type=1',array('dorm'=>$data->dorm));

                        $faculties = $DB->get_records_sql('SELECT u.*
                                        FROM {local_mxschool_faculty} f
                                          LEFT JOIN {user} u ON u.id=f.userid 
                                        WHERE f.dorm=:dorm',array('dorm'=>$data->dorm));

                        $student = $DB->get_record_sql('SELECT u.id, u.lastname, u.firstname
                                        FROM {local_mxschool_students} s
                                          LEFT JOIN {user} u ON u.id=s.userid 
                                        WHERE s.id=:student',array('student'=>$data->student));

                        $message->params = array();
                        $message->params['student name'] = $student->firstname.' '.$student->lastname;
                        $message->params['departure date'] = date('m/d/Y @ g:i a',$data->departure);
                        $message->params['return date'] = date('m/d/Y @ g:i a',$data->return);
                        $message->params['transportation'] = $data->transportation;
                        $message->params['destination'] = $data->destination;
                        $message->params['phone'] = $data->phone;
                        $message->params['today date time'] =  date('m/d/Y  g:i a');
                        $message->params['hohname'] = $hoh->lastname . ', ' . $hoh->firstname;
                        $message->params['hohphonenum'] = $hoh->phone1;
                        $message->params['update'] = (isset($data->instant->id))?'UPDATE: ':'';
                        $message->params['today date'] = date("D F j, Y");

                        $message->subject = $this->generate_text($notification->subject, $message->params, $notification->tags);
                        $message->message = $this->generate_text($notification->body, $message->params, $notification->tags);

                        $message->from = $supportuser;
                        foreach($faculties as $faculty){
                            $message->to = $faculty;
                            email_to_user($message->to, $message->from, $message->subject, $message->message, $message->message);
                        }
                        $send_email = false;
                        break;
                    case 8 :
                        $id = $this->_params;

                        $record = $DB->get_record_sql('SELECT p.id, 
                                                              CONCAT (pu.firstname,\' \',pu.lastname) as passenger, 
                                                              pu.email as passenger_email,
                                                              p.driver as driver_id, 
                                                              CONCAT (du.firstname,\' \',du.lastname) as driver, 
                                                              p.driver_other,
                                                              p.departure_time,
                                                              p.destination,
                                                              p.return_time,
                                                              p.permission, 
                                                              CONCAT (fu.firstname,\' \',fu.lastname) as granded_from, 
                                                              fu.email as granded_from_email,
                                                              ps.driving as prior_approval, 
                                                              p.call_received,
                                                              hu.email as hoh_email
                                        FROM {local_mxschool_epassenger} p 
                                            LEFT JOIN {local_mxschool_students} ps ON ps.id=p.passenger
                                            LEFT JOIN {user} pu ON pu.id=ps.userid
                                            LEFT JOIN {local_mxschool_students} ds ON ds.id=p.driver
                                            LEFT JOIN {user} du ON du.id=ds.userid
                                            LEFT JOIN {local_mxschool_faculty} f ON f.id=p.granded_from
                                            LEFT JOIN {user} fu ON fu.id=f.userid 
                                            LEFT JOIN {local_mxschool_faculty} fh ON fh.type=1 AND fh.dorm=ps.dorm
                                            LEFT JOIN {user} hu ON hu.id=fh.userid 
                                        WHERE p.id=:id',array('id'=>$id));
                        switch($record->call_received){
                            case 1:
                                $call_received = get_string('no');
                                break;
                            case 2:
                                $call_received = get_string('yes');
                                break;
                            case 0:
                                $call_received = get_string('no_data','local_mxschool');
                                break;
                        }
                        if($record->driver_id == 'other')
                            $driver = $record->driver_other;
                        elseif($record->driver_id == 'parent')
                            $driver = "Parent";
                        else
                            $driver = $record->driver;
                        
                        $message->params = array();
                        $message->params['passenger_name'] = $record->passenger;
                        $message->params['driver_name'] = $driver;
                        $message->params['departure'] = date('m/d/Y @ g:i a',$record->departure_time);
                        $message->params['destination'] = $record->destination;
                        $message->params['return'] = date('m/d/Y @ g:i a',$record->return_time);
                        $message->params['permision_from'] = $record->granded_from;
                        $message->params['permision'] = ($record->permission == 1)?get_string('yes'):get_string('no');
                        $message->params['call'] = $call_received;

                        $message->subject = $this->generate_text($notification->subject, $message->params, $notification->tags);
                        $message->message = $this->generate_text($notification->body, $message->params, $notification->tags);

                        $message->to = $DB->get_record('user', array('id'=>2));
                        $message->from = $supportuser;
                        $message_to = array($record->passenger_email,$record->granded_from_email,$record->hoh_email);
                        $message_to = array('cjmcdonald@mxschool.edu');
                        foreach($message_to as $item){
                            if(empty($item))continue;
                            $message->to->email = $item;
                            email_to_user($message->to, $message->from, $message->subject, $message->message, $message->message);
                        }
                        $send_email = false;
                        break;
                    case 300 : 
                        if ($params->id and $params->userid){
                            $assign = $DB->get_record("assign", array('id'=>$params->id));
                            $course = $DB->get_record("course", array('id'=>$assign->course));
                            if ($course->id){
                                $item = $DB->get_record_sql("SELECT u.*, nd.state as nd_state, nud.alert as n_alert, nud.email as n_email, nud.mobile as n_mobile, nud.status as nud_status, uc.value as u_carrier 
                                    FROM {$CFG->prefix}user as u
                                        LEFT JOIN {$CFG->prefix}notifications_data as nd ON nd.userid = u.id AND nd.not_id = $notification->id AND nd.data_id = $course->id AND nd.data_type = 'course'
                                        LEFT JOIN {$CFG->prefix}notifications_userdata as nud ON nud.userid = u.id AND nud.notid = $notification->id
                                        LEFT JOIN {$CFG->prefix}user_carriers as uc ON uc.id = u.carrier
                                            WHERE u.id = $params->userid");
                                if ($item->nud_status == 1 and $item->nd_state == 1) {
                                    $message->params = array('user'=>fullname($item), 'email'=>$item->email, 'coursename'=>$course->fullname, 'Assignment name'=>'<a href="'.$CFG->wwwroot.'/mod/assign/view.php?id='.$params->coursemodule.'">'.$instance->name.'</a>', 'supportuser'=>$supportuser->firstname.' '.$supportuser->lastname, 'supportemail'=>$supportuser->email);
                                    
                                    if (filter_var($item->email, FILTER_VALIDATE_EMAIL)) {
                                        $message->to = $DB->get_record('user', array('id'=>$item->id));
                                        $message->to->email = 'cjmcdonald@mxschool.edu';
                                        $message->from = $supportuser;
                                        $message->subject = $this->generate_text($notification->subject, $message->params, $notification->tags);
                                        $message->message = $this->generate_text($notification->body, $message->params, $notification->tags);
                                    }
                                }
                            }
                        } $send_email = true;
                        break;
                }
                if ($send_email) {
                    $result = email_to_user($message->to, $message->from, $message->subject, $message->message, $message->message);
                }
            }
        }
    }
    
    function generate_text($text = '', $params = array(), $tags){
        $message = '';
        if ($text == '') return $message;
        $tags = unserialize($tags);
        if (count($tags) > 0){
            $message = $text;
            foreach ($tags as $tag){
                if (strripos($message, '['.$tag.']') !== FALSE and isset($params[$tag])){
                    $message = str_replace('['.$tag.']', $params[$tag], $message);
                }
            }
        }
        return $message;
    }

}

?>

