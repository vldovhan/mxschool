<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

require('../../../config.php');
require_once('enter_rooming_form.php');
require_once($CFG->dirroot."/local/mxschool/classes/alerts/alerts.php");

require_login();
$systemcontext   = context_system::instance();

$id = optional_param('id', 0, PARAM_INT);

$title = get_string('rooming_form', 'local_mxschool');

$PAGE->set_url(new moodle_url("/local/mxschool/rooming/rooming-form.php", array()));
$PAGE->navbar->add(get_string('pluginname', 'local_mxschool'), new moodle_url('/local/mxschool/index.php'));
$PAGE->navbar->add(get_string('rooming_name', 'local_mxschool'), new moodle_url('/local/mxschool/rooming/index.php'));
$PAGE->navbar->add($title);
$PAGE->requires->jquery();
$PAGE->requires->js('/local/mxschool/assets/js/rooming.js');
$PAGE->set_pagelayout('admin');
$PAGE->set_context(context_system::instance());
$PAGE->set_title($title);
$PAGE->set_heading($title);

$datesform = new enter_rooming_form(null,array('id'=>$id));
if (!$datesform->is_cancelled() && $data = $datesform->get_data()) {
    $student = $DB->get_record('local_mxschool_students',array('userid'=>$USER->id));
    if(isset($student->id)){
        $data->name = $student->id;
    }

    $new_record = new stdClass();
    $new_record->studentid = $data->name;
    $new_record->dormaterequest = implode(',',array($data->dormate_select1,$data->dormate_select2,$data->dormate_select3,$data->dormate_select4,$data->dormate_select5,$data->dormate_select6));
    $new_record->roomtype = $data->room_type;
    $new_record->haslivedindouble = $data->past_one_rm_db;
    $new_record->preferreddoubleroommate = $data->preferred_roommate;
    $new_record->status = 1;

    if($old_record = $DB->get_record('local_mxschool_rooming',array('studentid' => $data->name))){
        $new_record->id = $old_record->id;
        $new_record->timeupdate = time();
        $DB->update_record('local_mxschool_rooming',$new_record);
        $jAlert->create(array('type'=>'success', 'text'=>'Successfully updated'));
    }else{
        $new_record->timecreate = time();
        $DB->insert_record('local_mxschool_rooming',$new_record,false);
        $jAlert->create(array('type'=>'success', 'text'=>'Successfully created'));
    }

    if(isset($student->id)){
        redirect(new moodle_url('/my/'));
    } else {
        redirect(new moodle_url('/local/mxschool/rooming/rooming-requests.php'));
    }
}elseif($datesform->is_cancelled()){
    redirect(new moodle_url('/local/mxschool/rooming/index.php'));
}
echo $OUTPUT->header();
echo $OUTPUT->heading($title);

$datesform->display();

echo $OUTPUT->footer();
