<?php

defined('MOODLE_INTERNAL') || die;

require_once($CFG->libdir.'/formslib.php');

/**
 * The form for handling editing a course.
 */
class edit_course_form extends moodleform {
    /**
     * Form definition.
     */
    function definition() {
        global $DB;

        $mform          = $this->_form;
        $id             = $this->_customdata['id'];
        if($id){
            $instant = $DB->get_record('local_mxschool_tutors_course',array('id'=>$id));
            $mform->addElement('hidden', 'id', $id);
            $mform->setType('id', PARAM_INT);
        }
        
        $categories_obj = $DB->get_records('local_mxschool_tutors_cat');
        $categories = array();
        foreach($categories_obj as $item){
            $categories[$item->id] = $item->name;
        }
        

        $mform->addElement('select', 'category', get_string('category','local_mxschool'),$categories);
        $mform->setType('category', PARAM_INT);
        if(isset($instant)){
            $mform->setDefault('category', $instant->category);
        }
        $mform->addRule('category', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        
        $mform->addElement('text', 'name', get_string('name'));
        $mform->setType('name', PARAM_RAW);
        if(isset($instant)){
            $mform->setDefault('name', $instant->name);
        }
        $mform->addRule('name', get_string('required_field', 'local_mxschool'), 'required', null, 'client');

        $this->add_action_buttons(get_string('cancel'), get_string('save', 'local_mxschool'));
    }
}


