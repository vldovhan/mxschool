<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

require('../../../config.php');
require_once($CFG->libdir.'/csvlib.class.php');
require_once('tutoring_import_form.php');
require_once('../importlib.php');
require_once($CFG->dirroot."/local/mxschool/classes/alerts/alerts.php");

require_login();
$systemcontext   = context_system::instance();
require_capability('local/mxschool:tutors_settings', $systemcontext);

$id = optional_param('id', 0, PARAM_INT);
$iid         = optional_param('iid', '', PARAM_INT);
$previewrows = optional_param('previewrows', 10, PARAM_INT);

$title = get_string('import_tutoring_form', 'local_mxschool');

$FIELDS = array('lastname','firstname','department');
$returnurl = new moodle_url("/local/mxschool/tutors/tutoring-import.php", array());

$PAGE->set_url(new moodle_url("/local/mxschool/tutors/tutoring-import.php", array()));
$PAGE->navbar->add(get_string('pluginname', 'local_mxschool'), new moodle_url('/local/mxschool/index.php'));
$PAGE->navbar->add(get_string('tutors_name', 'local_mxschool'), new moodle_url('/local/mxschool/tutors/index.php'));
$PAGE->navbar->add($title);
$PAGE->requires->jquery();
$PAGE->set_pagelayout('admin');
$PAGE->set_context(context_system::instance());
$PAGE->set_title($title);
$PAGE->set_heading($title);

if(empty($iid)){
    $form = new tutoring_import_form(null, array());
    if(!$form->is_cancelled() && $formdata = $form->get_data()){
        $iid = csv_import_reader::get_new_iid('importfile');
        $cir = new csv_import_reader($iid, 'importfile');

        $content = $form->get_file_content('importfile');

        $readcount = $cir->load_csv_content($content, $formdata->encoding, $formdata->delimiter_name);
        $csvloaderror = $cir->get_error();
        unset($content);

        if(!is_null($csvloaderror)){
            print_error('csvloaderror', '', $returnurl, $csvloaderror);
        }
        // test if columns ok
        $filecolumns = fi_validate_file_upload_columns($cir, $FIELDS, $returnurl);
        // continue to form2

    }elseif($form->is_cancelled()){
        redirect(new moodle_url('/local/mxschool/tutors/courses.php'));
    }else{
        echo $OUTPUT->header();
        echo $OUTPUT->heading($title);
        $form->display();
        echo $OUTPUT->footer();
        die;
    }
} else {
    $cir = new csv_import_reader($iid, 'importfile');
    $filecolumns = fi_validate_file_upload_columns($cir, $FIELDS, $returnurl);
}

$mform2 = new tutoring_import_form(null, array('data'=>array('iid'=>$iid, 'previewrows'=>$previewrows)));

if ($formdata = $mform2->is_cancelled()) {
    $cir->cleanup(true);
    redirect($returnurl);

} else if ($formdata = $mform2->get_data()) {
    // Print the header
    echo $OUTPUT->header();
    echo $OUTPUT->heading($title);

    $recordsnew     = 0;
    $recordsprc     = 0;
    $recordsupd     = 0;
    $recordsdel     = 0;
    $recordserrors  = 0;

    // init csv import helper
    $cir->init();
    $linenum = 1; //column header is first line

    // init upload progress tracker
    $upt = new fi_progress_tracker_tutors();
    $upt->columns = array('status', 'line', 'department', 'user');
    $upt->start(); // start table

    while ($line = $cir->next()) {
        $recordsprc++;
        $upt->flush();
        $linenum++;

        $upt->track('line', $linenum);

        $record = new stdClass();

        // add fields to user object
        foreach ($line as $keynum => $value) {
            if (!isset($filecolumns[$keynum])) {
                // this should not happen
                continue;
            }
            $key = $filecolumns[$keynum];

            switch ($key) {
                case 'department':
                    $record->department = ($value != '') ? trim($value) : '';
                    break;
                case 'lastname':
                    $record->lastname = ($value != '') ? trim($value) : '';;
                    break;
                case 'firstname':
                    $record->firstname = ($value != '') ? trim($value) : '';;
                    break;
                default:
                    $record->$key = trim($value);
            }

            if (in_array($key, $upt->columns)) {
                // default value in progress tracking table, can be changed later
                $upt->track($key, $value, 'normal');
            }
        }
        $upt->track('user', $record->lastname.' '.$record->firstname);


        if (empty($record->department)) {
            $upt->track('status', get_string('missingfield', 'error', 'department'), 'error');
            $upt->track('department', get_string('error'), 'error');
            $recordserrors++;
            continue;
        }

        if (empty($record->lastname)) {
            $upt->track('status', get_string('missingfield', 'error', 'lastname'), 'error');
            $upt->track('user', get_string('error'), 'error');
            $recordserrors++;
            continue;
        }

        if (empty($record->firstname)) {
            $upt->track('status', get_string('missingfield', 'error', 'firstname'), 'error');
            $upt->track('user', get_string('error'), 'error');
            $recordserrors++;
            continue;
        }

        $tutor = $DB->get_record_sql('SELECT t.*
                                            FROM {local_mxschool_tutors} t
                                              LEFT JOIN {user} u ON u.id=t.userid
                                            WHERE u.firstname=:firstname AND u.lastname=:lastname AND u.deleted=0
                                            ',array('firstname'=>$record->firstname,'lastname'=>$record->lastname));
        $user = $DB->get_record('user',array('firstname'=>$record->firstname,'lastname'=>$record->lastname,'deleted'=>0));
        if(!isset($tutor->id) && !isset($user->id)){
            $upt->track('status', get_string('tutor_not_found', 'local_mxschool'), 'error');
            $upt->track('user', get_string('error'), 'error');
            $recordserrors++;
            continue;
        }elseif(isset($user->id)){
            $role = $DB->get_record('role',array('shortname'=>'tutor'));
            role_assign($role->id, $user->id, 2);
            $tutor = $DB->get_record('local_mxschool_tutors',array('userid'=>$user->id));
        }

        $departament = $DB->get_record('local_mxschool_tutors_cat',array('name'=>$record->department));
        if(!isset($departament->id)){
            $departament = new stdClass();
            $departament->name = $record->department;
            $departament->timecreate = time();
            $departament->id = $DB->insert_record('local_mxschool_tutors_cat',$departament);
        }


        if(empty($tutor->data)){
            $tutor->data = json_encode(array($departament->id));
            $DB->update_record('local_mxschool_tutors',$tutor);
            $recordsnew++;
        }else{
            $data = json_decode($tutor->data);
            $data = array_unique(array_merge($data,array($departament->id)));
            $tutor->data = json_encode($data);
            $DB->update_record('local_mxschool_tutors',$tutor);
            $recordsnew++;
        }

    }
    $upt->close(); // close table

    $cir->close();
    $cir->cleanup(true);

    echo $OUTPUT->box_start('boxwidthnarrow boxaligncenter generalbox', 'uploadresults');
    echo '<p>';

    echo get_string('recordscreated', 'local_mxschool').': '.$recordsnew.'<br />';
    echo get_string('recordsprocessed', 'local_mxschool').': '.$recordsprc.'<br />';
    echo get_string('recordserrors', 'local_mxschool').': '.$recordserrors.'<br />';
    echo $OUTPUT->box_end();

    echo $OUTPUT->continue_button(new moodle_url("/local/mxschool/tutors/tutoring-records.php", array()));

    echo $OUTPUT->footer();
    die;
}

