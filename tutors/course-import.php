<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

require('../../../config.php');
require_once($CFG->libdir.'/csvlib.class.php');
require_once('course_import_form.php');
require_once('../importlib.php');
require_once($CFG->dirroot."/local/mxschool/classes/alerts/alerts.php");

require_login();
$systemcontext   = context_system::instance();
require_capability('local/mxschool:tutors_settings', $systemcontext);

$id = optional_param('id', 0, PARAM_INT);
$iid         = optional_param('iid', '', PARAM_INT);
$previewrows = optional_param('previewrows', 10, PARAM_INT);

$title = get_string('import_course_form', 'local_mxschool');

$FIELDS = array('department', 'course');
$returnurl = new moodle_url("/local/mxschool/tutors/course-import.php", array());

$PAGE->set_url(new moodle_url("/local/mxschool/tutors/course-import.php", array()));
$PAGE->navbar->add(get_string('pluginname', 'local_mxschool'), new moodle_url('/local/mxschool/index.php'));
$PAGE->navbar->add(get_string('tutors_name', 'local_mxschool'), new moodle_url('/local/mxschool/tutors/index.php'));
$PAGE->navbar->add($title);
$PAGE->requires->jquery();
$PAGE->set_pagelayout('admin');
$PAGE->set_context(context_system::instance());
$PAGE->set_title($title);
$PAGE->set_heading($title);

if(empty($iid)){
    $form = new course_import_form(null, array());
    if(!$form->is_cancelled() && $formdata = $form->get_data()){
        $iid = csv_import_reader::get_new_iid('importfile');
        $cir = new csv_import_reader($iid, 'importfile');

        $content = $form->get_file_content('importfile');

        $readcount = $cir->load_csv_content($content, $formdata->encoding, $formdata->delimiter_name);
        $csvloaderror = $cir->get_error();
        unset($content);

        if(!is_null($csvloaderror)){
            print_error('csvloaderror', '', $returnurl, $csvloaderror);
        }
        // test if columns ok
        $filecolumns = fi_validate_file_upload_columns($cir, $FIELDS, $returnurl);
        // continue to form2

    }elseif($form->is_cancelled()){
        redirect(new moodle_url('/local/mxschool/tutors/courses.php'));
    }else{
        echo $OUTPUT->header();
        echo $OUTPUT->heading($title);
        $form->display();
        echo $OUTPUT->footer();
        die;
    }
} else {
    $cir = new csv_import_reader($iid, 'importfile');
    $filecolumns = fi_validate_file_upload_columns($cir, $FIELDS, $returnurl);
}

$mform2 = new course_import_form(null, array('data'=>array('iid'=>$iid, 'previewrows'=>$previewrows)));

if ($formdata = $mform2->is_cancelled()) {
    $cir->cleanup(true);
    redirect($returnurl);

} else if ($formdata = $mform2->get_data()) {
    // Print the header
    echo $OUTPUT->header();
    echo $OUTPUT->heading($title);

    $recordsnew     = 0;
    $recordsprc     = 0;
    $recordsupd     = 0;
    $recordsdel     = 0;
    $recordserrors  = 0;

    // init csv import helper
    $cir->init();
    $linenum = 1; //column header is first line

    // init upload progress tracker
    $upt = new fi_progress_tracker();
    $upt->columns = array('status', 'line', 'department', 'course');
    $upt->start(); // start table

    while ($line = $cir->next()) {
        $upt->flush();
        $linenum++;

        $upt->track('line', $linenum);

        $record = new stdClass();

        // add fields to user object
        foreach ($line as $keynum => $value) {
            if (!isset($filecolumns[$keynum])) {
                // this should not happen
                continue;
            }
            $key = $filecolumns[$keynum];

            switch ($key) {
                case 'department':
                    $record->department = ($value != '') ? trim($value) : '';
                    break;
                case 'course':
                    $record->course = ($value != '') ? trim($value) : '';;
                    break;
                default:
                    $record->$key = trim($value);
            }

            if (in_array($key, $upt->columns)) {
                // default value in progress tracking table, can be changed later
                $upt->track($key, $value, 'normal');
            }
        }


        if (empty($record->department)) {
            $upt->track('status', get_string('missingfield', 'error', 'department'), 'error');
            $upt->track('department', get_string('error'), 'error');
            $recordserrors++;
            continue;
        }

        if (empty($record->course)) {
            $upt->track('status', get_string('missingfield', 'error', 'course'), 'error');
            $upt->track('course', get_string('error'), 'error');
            $recordserrors++;
            continue;
        }

        $departament = $DB->get_record('local_mxschool_tutors_cat',array('name'=>$record->department));
        if(!isset($departament->id)){
            $departament = new stdClass();
            $departament->name = $record->department;
            $departament->timecreate = time();
            $departament->id = $DB->insert_record('local_mxschool_tutors_cat',$departament);
        }

        $course =  $DB->get_record('local_mxschool_tutors_course',array('name'=>$record->course));
        if(!isset($course->id)){
            $course = new stdClass();
            $course->name = $record->course;
            $course->category = $departament->id;
            $course->timecreate = time();
            $course->id = $DB->insert_record('local_mxschool_tutors_course',$course);
            if($course->id > 1)
                $recordsnew++;
        }
        $recordsprc++;
    }
    $upt->close(); // close table

    $cir->close();
    $cir->cleanup(true);

    echo $OUTPUT->box_start('boxwidthnarrow boxaligncenter generalbox', 'uploadresults');
    echo '<p>';

    echo get_string('recordscreated', 'local_mxschool').': '.$recordsnew.'<br />';
    echo get_string('recordsprocessed', 'local_mxschool').': '.$recordsprc.'<br />';
    echo get_string('recordserrors', 'local_mxschool').': '.$recordserrors.'<br />';
    echo $OUTPUT->box_end();

    echo $OUTPUT->continue_button(new moodle_url("/local/mxschool/tutors/courses.php", array()));

    echo $OUTPUT->footer();
    die;
}

