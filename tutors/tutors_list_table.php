<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/tablelib.php');

class tutors_table extends table_sql {

    function __construct($uniqueid, $search) {
        global $CFG,$DB;

        parent::__construct($uniqueid);

        $categories = $DB->get_records('local_mxschool_tutors_cat');

        $columns = array('lastname','firstname');
        $header = array('Last','First');
        foreach($categories as $category){
            $header[] = $category->name;
            $columns[] = $category->id;
        }

        $this->define_columns($columns);
        $this->define_headers($header);


        $fields = "t.id, t.data, u.lastname, u.firstname";
        $from = "{local_mxschool_tutors} t
                    LEFT JOIN {user} u ON u.id=t.userid";

        $where = "t.id>0";
        if(!empty($search)){
            //LIKE '%$search%'

            $where .= " AND (u.lastname LIKE '%$search%' OR u.firstname LIKE '%$search%')";
        }

        $this->set_sql($fields, $from, $where, array());
        $this->define_baseurl($CFG->wwwroot.$_SERVER['REQUEST_URI']);
    }

    function other_cols($column, $row) {
        if(is_int($column)){
            $attr = array('type' => 'checkbox', 'name' => $row->id.'_'.$column);
            $data = json_decode($row->data);
            if(is_array($data) && in_array($column,$data)){
                $attr['checked'] = 'checked';
            }

            if($this->is_downloading())
                return (isset($attr['checked']))?get_string('yes'):get_string('no');
            else
                return  html_writer::empty_tag('input', $attr);
        }else
            return NULL;
    }
}
