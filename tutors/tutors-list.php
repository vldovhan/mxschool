<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

require('../../../config.php');
require ('tutors_list_table.php');

$download = optional_param('download', '', PARAM_ALPHA);
$search = optional_param('search', '', PARAM_RAW);
$save_data = optional_param('save_data', '', PARAM_RAW);
$action = optional_param('action', '', PARAM_ALPHA);
$id = optional_param('id', 0, PARAM_INT);

require_login();
$systemcontext   = context_system::instance();
require_capability('local/mxschool:tutors_settings', $systemcontext);

$table = new tutors_table('tutors_table', $search);
$table->is_collapsible = false;
$table->is_downloading($download, 'tutors', 'Tutors');
if ($table->is_downloading()){
    $table->out(20, true);
    die();
}

if($action == 'saveusercat'){
    $user_id = required_param('user_id',PARAM_INT);
    $cat_id = required_param('cat_id',PARAM_INT);
    $state = required_param('state',PARAM_BOOL);

    $tutor = $DB->get_record('local_mxschool_tutors',array('id'=>$user_id));
    if(empty($tutor->data) && $state){
        $tutor->data = json_encode(array($cat_id));
        $DB->update_record('local_mxschool_tutors',$tutor);
    }else{
        $data = json_decode($tutor->data);
        if($state){
            $data[] = $cat_id;
        }else{
            $key = array_search($cat_id, $data);
            if ($key !== false)
                unset($data[$key]);
        }
        $tutor->data = json_encode(array_values($data));
        $DB->update_record('local_mxschool_tutors',$tutor);
    }
    die('ok');
}

require_once($CFG->dirroot."/local/mxschool/classes/alerts/alerts.php");

$title = get_string('tutors_list', 'local_mxschool');

$PAGE->set_url(new moodle_url("/local/mxschool/tutors/tutors-list.php", array()));
$PAGE->navbar->add(get_string('pluginname', 'local_mxschool'), new moodle_url('/local/mxschool/index.php'));
$PAGE->navbar->add(get_string('tutors_name', 'local_mxschool'), new moodle_url('/local/mxschool/tutors/index.php'));
$PAGE->navbar->add($title);
$PAGE->requires->jquery();
$PAGE->requires->js('/local/mxschool/assets/js/tutors.js',true);
$PAGE->set_pagelayout('admin');
$PAGE->set_context(context_system::instance());
$PAGE->set_title($title);
$PAGE->set_heading($title);

echo $OUTPUT->header();
echo $OUTPUT->heading($title);

echo html_writer::start_tag("form",  array("action"=> $PAGE->url, 'class'=>'mxschool-search-form'));
echo html_writer::empty_tag('input', array('type' => 'text', 'name' => 'search', 'placeholder' => get_string('search').' ...', 'value' => $search));
echo html_writer::empty_tag('input', array('type' => 'submit', 'value' => get_string('search')));
echo html_writer::end_tag("label");
echo html_writer::end_tag("form");

echo html_writer::start_tag('div', array('class' => 'mxschool-table-box'));
$table->out(20, true);
echo html_writer::end_tag("div");

echo $OUTPUT->footer();
