<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */


require('../../../config.php');
require_once('preferences_form.php');
require_once($CFG->dirroot."/local/mxschool/classes/alerts/alerts.php");

$systemcontext   = context_system::instance();
require_login();
require_capability('local/mxschool:tutors_settings', $systemcontext);
$title = get_string('preferences', 'local_mxschool');

$PAGE->set_url(new moodle_url("/local/mxschool/tutors/preferences.php", array()));
$PAGE->navbar->add(get_string('pluginname', 'local_mxschool'), new moodle_url('/local/mxschool/index.php'));
$PAGE->navbar->add(get_string('tutors_name', 'local_mxschool'), new moodle_url('/local/mxschool/tutors/index.php'));
$PAGE->navbar->add($title);
$PAGE->requires->jquery();
$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('admin');
$PAGE->set_title($title);
$PAGE->set_heading($title);

$settings = array();
$settings['start_date'] = get_config('local_mxschool', 'tutors_form_start_date');
$settings['end_date'] = get_config('local_mxschool', 'tutors_form_end_date');

$editform = new edit_form(null, array('settings'=>$settings));

if ($editform->is_cancelled()) {
    // The form has been cancelled, take them back to what ever the return to is.
    redirect(new moodle_url('/local/mxschool/tutors/index.php'));
} else if ($data = $editform->get_data()) {
    // Process data if submitted.
    set_config('tutors_form_start_date', $data->start_date, 'local_mxschool');
    set_config('tutors_form_end_date', $data->end_date, 'local_mxschool');

    $jAlert->create(array('type'=>'success', 'text'=>'Preferences was successfully saved'));
    redirect(new moodle_url('/local/mxschool/tutors/index.php'));
}

// Print the form.

$PAGE->set_context(context_system::instance());
$PAGE->set_title($title);
$PAGE->set_heading($title);

echo $OUTPUT->header();
echo $OUTPUT->heading($title);

echo html_writer::start_tag('div', array('class' => 'mx-adviser-settings-form'));
$editform->display();
echo html_writer::end_tag('div');

echo $OUTPUT->footer();
