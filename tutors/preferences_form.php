<?php

defined('MOODLE_INTERNAL') || die;

require_once($CFG->libdir.'/formslib.php');

/**
 * The form for handling editing a course.
 */
class edit_form extends moodleform {

    /**
     * Form definition.
     */
    function definition() {
        global $CFG, $PAGE;

        $mform          = $this->_form;
        $settings       = $this->_customdata['settings'];

        $mform->addElement('date_selector', 'start_date', get_string('start_date', 'local_mxschool'));
        $mform->addRule('start_date', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        $mform->setType('start_date', PARAM_RAW);

        $mform->addElement('date_selector', 'end_date', get_string('end_date', 'local_mxschool'));
        $mform->addRule('end_date', get_string('required_field', 'local_mxschool'), 'required', null, 'client');
        $mform->setType('end_date', PARAM_RAW);

        $this->add_action_buttons(get_string('cancel'), get_string('save', 'local_mxschool'));

        // Finally set the current form data
        $this->set_data($settings);
    }
}

