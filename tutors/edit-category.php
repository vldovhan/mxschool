<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

require('../../../config.php');
require_once('edit_category_form.php');
require_once($CFG->dirroot."/local/mxschool/classes/alerts/alerts.php");

require_login();
$systemcontext   = context_system::instance();
require_capability('local/mxschool:tutors_settings', $systemcontext);

$id = optional_param('id', 0, PARAM_INT);

$title = get_string('caregory_form', 'local_mxschool');

$PAGE->set_url(new moodle_url("/local/mxschool/tutors/edit-category.php", array()));
$PAGE->navbar->add(get_string('pluginname', 'local_mxschool'), new moodle_url('/local/mxschool/index.php'));
$PAGE->navbar->add(get_string('tutors_name', 'local_mxschool'), new moodle_url('/local/mxschool/tutors/index.php'));
$PAGE->navbar->add($title);
$PAGE->requires->jquery();
$PAGE->requires->js('/local/mxschool/assets/js/rooming.js');
$PAGE->set_pagelayout('admin');
$PAGE->set_context(context_system::instance());
$PAGE->set_title($title);
$PAGE->set_heading($title);

$form = new edit_category_form(null,array('id'=>$id));
if (!$form->is_cancelled() && $data = $form->get_data()) {
    $new_record = new stdClass();
    $new_record->name = $data->name;
    if($id && $record = $DB->get_record('local_mxschool_tutors_cat',array('id'=>$id))){
        $new_record->id = $record->id;
        $DB->update_record('local_mxschool_tutors_cat',$new_record);
        $jAlert->create(array('type'=>'success', 'text'=>'Successfully updated'));
    }else{
        $new_record->timecreate = time();
        $DB->insert_record('local_mxschool_tutors_cat', $new_record);
        $jAlert->create(array('type'=>'success', 'text'=>'Successfully created'));
    }
    redirect(new moodle_url('/local/mxschool/tutors/categories.php'));
    
}elseif($form->is_cancelled()){
    redirect(new moodle_url('/local/mxschool/tutors/categories.php'));
}
echo $OUTPUT->header();
echo $OUTPUT->heading($title);

$form->display();

echo $OUTPUT->footer();
