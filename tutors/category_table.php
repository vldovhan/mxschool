<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/tablelib.php');

class category_table extends table_sql {

    function __construct($uniqueid, $search) {
        global $CFG;

        parent::__construct($uniqueid);

        $columns = array('name', 'timecreate', 'actions');
        $header = array('Name', 'Created', 'Actions');

        $this->define_columns($columns);
        $this->define_headers($header);


        $fields = "c.*";
        $from = "{local_mxschool_tutors_cat} c";

        $where = "c.id>0";
        if(!empty($search)){
            //LIKE '%$search%'

            $where .= " AND c.name LIKE '%$search%'";
        }

        $this->set_sql($fields, $from, $where, array());
        $this->define_baseurl($CFG->wwwroot.$_SERVER['REQUEST_URI']);
    }

    function col_timecreate($values) {
        return  date('m/d/Y H:i a', usertime($values->timecreate, -get_user_timezone_offset()));
    }
    function col_actions($values) {
        global $OUTPUT;

        if ($this->is_downloading()){
            return '';
        }

        $strdelete  = get_string('delete');
        $stredit  = get_string('edit');

        $edit = array();

        $aurl = new moodle_url('/local/mxschool/tutors/edit-category.php', array('id'=>$values->id));
        $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/edit', $stredit, 'core', array('class' => 'iconsmall')));

        $aurl = new moodle_url('/local/mxschool/tutors/categories.php', array('action'=>'delete', 'id'=>$values->id));
        $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/delete', $strdelete, 'core', array('class' => 'iconsmall')), null, array('onclick'=>"if (!confirm('Are you sure want to delete this redord?')) return false;"));

        return implode('', $edit);
    }
}
