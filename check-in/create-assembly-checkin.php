<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

require('../../../config.php');
require ('create_assembly_checkin_classes.php');
require_once($CFG->dirroot."/local/mxschool/classes/alerts/alerts.php");

require_login();
$systemcontext   = context_system::instance();
require_capability('local/mxschool:checkin_settings', $systemcontext);

$title = get_string('create_assembly_checkin','local_mxschool');

$PAGE->set_url(new moodle_url("/local/mxschool/check-in/create-assembly-checkin.php", array()));
$PAGE->navbar->add(get_string('pluginname', 'local_mxschool'), new moodle_url('/local/mxschool/index.php'));
$PAGE->navbar->add(get_string('checkin_name', 'local_mxschool'), new moodle_url('/local/mxschool/check-in/index.php'));
$PAGE->navbar->add($title);
$PAGE->requires->jquery();
$PAGE->set_pagelayout('admin');
$PAGE->set_context(context_system::instance());
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->add_body_class('print-table');

$form = new assembly_checkin_form(null);
$table = null;
if (!$form->is_cancelled() && $data = $form->get_data()) {
    $users_obj = $DB->get_records_sql('SELECT u.id, u.lastname, u.firstname, s.grade
                                FROM {local_mxschool_students} s
                                    LEFT JOIN {user} u ON s.userid=u.id
                                ORDER BY u.lastname');
    $users = array();
    foreach($users_obj as $user){
        $users[] = $user;
    }

    $count_students = count($users);
    $extra = $count_students % $data->groups_size;
    $peoplePerGroup = ($count_students-$extra)/$data->groups_size;

    $mod = $data->group;
    $add_str = '';
    if($mod > $extra)
        $mod = $extra;
    $add_str .= ($data->group+1) . ": ";
    $beginning = ($data->group*$peoplePerGroup)+$mod;
    $add_str .= $users[$beginning]->lastname;
    $add_str .= "-";
    if($extra > $mod)
        $mod++;
    $end = (($data->group+1)*$peoplePerGroup)-1+$mod;
    $add_str .= $users[$end]->lastname;


    $table = new html_table();
    $table->head = array('Name', 'Grade','');
    for ($i = $beginning; $i<=$end; $i++){
        $current_user = $users[$i];
        $table->data[] = array($current_user->lastname.', '.$current_user->firstname,$current_user->grade,'');
    }
}

echo $OUTPUT->header();

if($table){
    echo html_writer::tag('div',html_writer::link(new moodle_url('#'), get_string('print', 'local_mxschool'),array('class' => 'btn','id'=>'print-table')),array('class' => 'before-table'));
    echo $OUTPUT->heading(get_string('assembly_checkin_table_title', 'local_mxschool', $add_str), 2, 'table_title_center');
    echo html_writer::start_tag('div', array('class' => 'mxschool-table-box no-full-table'));
    echo html_writer::table($table);
    echo html_writer::end_tag("div");
}else{
    echo $OUTPUT->heading($title);
    echo html_writer::start_tag('div', array('class' => 'mxschool-table-box'));
    $form->display();
    echo html_writer::end_tag("div");
}

echo $OUTPUT->footer();


?>
    <script>
        $(window).ready(function () {
            $('#print-table').click(function (e) {
                window.print();
                return false;
            });
            $('#groups_size').change(function () {
                var val = $(this).val();
                if(val != ''){
                    jQuery.ajax({
                        url: M.cfg.wwwroot+"/local/mxschool/check-in/get_user_groups.php",
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            'number_of_groups' :val,
                        },
                        complete: function(data) {
                            var old_val = $('#group').val();
                            var empty_option = '<option value="">------</option>';
                            $('#group').html(empty_option+data.responseJSON.options_text);
                            $('#group').val(old_val);
                        }
                    });
                }else{
                    $('#group').html('<option value="">------</option>');
                }
            });
            $('#groups_size').change();
        });
    </script>

<?php
