<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

require('../../../config.php');
require ('create_weekend_checkin_clesses.php');
require_once($CFG->dirroot."/local/mxschool/classes/alerts/alerts.php");

$original_data = optional_param('data','',PARAM_RAW);

require_login();
$systemcontext   = context_system::instance();
require_capability('local/mxschool:checkin_settings', $systemcontext);

$title = get_string('create_weekend_checkin','local_mxschool');

$PAGE->set_url(new moodle_url("/local/mxschool/check-in/create-weekdend-checkin.php", array()));
$PAGE->navbar->add(get_string('pluginname', 'local_mxschool'), new moodle_url('/local/mxschool/index.php'));
$PAGE->navbar->add(get_string('checkin_name', 'local_mxschool'), new moodle_url('/local/mxschool/check-in/index.php'));
$PAGE->navbar->add($title);
$PAGE->requires->jquery();
$PAGE->set_pagelayout('admin');
$PAGE->set_context(context_system::instance());
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->add_body_class('print-table');

$days = array('-3'=>'Wed.','-2'=>'Thurs.','-1'=>'Fri.','0'=>'Sat.','+1'=>'Sun.','+2'=>'Mon.','+3'=>'Tues.');
$form = new weekend_checkin_form(null);
$table = null;
if (!$form->is_cancelled() && $data = $form->get_data()) {
    $string_data = new stdClass();
    $string_data->dorm = (in_array($data->dorm,array('boarding','day','all')))?get_string('all_dorm_'.$data->dorm,'local_mxschool'):$DB->get_record('local_mxschool_dorms',array('abbreviation'=>$dorm_abbr))->name;
    $string_data->date = $data->date;
    $table_title = get_string('weekend_checkin_table_title','local_mxschool',$string_data);
    
    $table = new weekend_checkin_table('weekend_checkin_table', $data);
    $table->is_collapsible = false;
    $total_records = $DB->count_records('local_mxschool_students');
    $table_header = '';
    foreach($data->day as $day){
        $table_header .= '<th colspan="2" scope="col" style="" class="header">'.$days[$day].'</th>';
    }
}elseif($original_data != ''){
    $data = json_decode($original_data);
    $string_data = new stdClass();
    $string_data->dorm = (in_array($data->dorm,array('boarding','day','all')))?get_string('all_dorm_'.$data->dorm,'local_mxschool'):$DB->get_record('local_mxschool_dorms',array('abbreviation'=>$dorm_abbr))->name;
    $string_data->date = $data->date;
    $table_title = get_string('weekend_checkin_table_title','local_mxschool',$string_data);

    $table = new weekend_checkin_table('weekend_checkin_table', $data);
    $table->is_collapsible = false;
    $total_records = $DB->count_records('local_mxschool_students');
    $table_header = '';
    foreach($data->day as $day){
        $table_header .= '<th colspan="2" scope="col" style="" class="header">'.$days[$day].'</th>';
    }
}

echo $OUTPUT->header();

if($table){
    echo html_writer::tag('div',html_writer::link(new moodle_url('#'), get_string('print', 'local_mxschool'),array('class' => 'btn','id'=>'print-table')),array('class' => 'before-table'));
    echo $OUTPUT->heading($table_title, 2, 'table_title_center');
}else
    echo $OUTPUT->heading($title);


echo html_writer::start_tag('div', array('class' => 'mxschool-table-box weekend_checkin_table'));

if($table){
    $table->out($total_records, true);
}else
    $form->display();

echo html_writer::end_tag("div");

echo $OUTPUT->footer();

if($table):?>
<script>
    $(window).ready(function () {
        $('.flexible.generaltable thead').prepend('<tr><th colspan="1" scope="col" class="header"></th><th colspan="1" scope="col" class="header"></th><th colspan="1" scope="col" class="header"></th><?php echo $table_header;?><th colspan="1" scope="col" class="header"></th><th colspan="2" scope="col" class="header">Call</th><th colspan="1" scope="col" class="header"></th><th colspan="1" scope="col" class="header"></th><th colspan="1" scope="col" class="header"></th><th colspan="1" scope="col" class="header"></th><th colspan="1" scope="col" class="header"></th><th colspan="1" scope="col" class="header"></th></tr>');

        $('#print-table').click(function (e) {
            window.print();
            return false;
        });

        $('.mxschool-table-box input[type="checkbox"]').change(function () {
            var checked = this.checked;
            var name = $(this).attr('name');
            var value = $(this).attr('value');
            var element = this;
            jQuery.ajax({
                url: M.cfg.wwwroot+"/local/mxschool/weekend/ajax-edit-weekend.php",
                type: 'POST',
                dataType: 'json',
                data: {
                    'field' :name,
                    'value' :value,
                    'state' :checked,
                    'action' :'savechange'
                },
                complete: function(data) {
                    $(element).after('<span class="saved">Saved</span>');
                    if(checked)
                        $(element).parent().children("button").show(300);
                    setTimeout(function () {
                        $('.saved').fadeOut(function() {
                            $(this).remove();
                        });
                    }, 1000);
                }
            });
        });
    });
</script>
<?php endif;
