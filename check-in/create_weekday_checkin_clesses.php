<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/tablelib.php');
require_once($CFG->libdir.'/formslib.php');
require_once('../lib.php');

class weekday_checkin_table extends table_sql {

    function __construct($uniqueid, $dorm) {
        global $CFG;

        parent::__construct($uniqueid);

        $columns = array('name','room', 'grade', 'early1','late1', 'early2','late2', 'early3','late3', 'early4','late4', 'early5','late5');
        $header = array('Name','Room #', 'Grade', 'Early','Late', 'Early','Late', 'Early','Late', 'Early','Late', 'Early','Late');

        $this->define_columns($columns);
        $this->define_headers($header);
        $this->sort_default_column = 'name';
        


        $fields = "st.id, st.room, st.grade, CONCAT(u.lastname,' ',u.firstname) as name,'' as early1,'' as late1,'' as early2,'' as late2,'' as early3,'' as late3,'' as early4,'' as late4,'' as early5,'' as late5";
        $from = "{local_mxschool_students} st
                    LEFT JOIN {user} u ON st.userid=u.id";

        $where = "st.id>0";
        if(!empty($dorm)){
            $where .= " AND st.dorm='$dorm'";
        }

        $this->set_sql($fields, $from, $where, array());
        $this->define_baseurl(new moodle_url("/local/mxschool/check-in/create-weekday-checkin.php", array('dorm'=>$dorm)));
    }
}

class weekday_checkin_form extends moodleform {
    /**
     * Form definition.
     */
    function definition() {
        global $DB;

        $mform          = $this->_form;

        $mform->addElement('select', 'dorm', get_string('dorm','local_mxschool'),get_dorms_list());
        $mform->setType('dorm', PARAM_RAW);
        $mform->addRule('dorm', get_string('required_field', 'local_mxschool'), 'required', null, 'client');

        $this->add_action_buttons(null, get_string('generate_table', 'local_mxschool'));
    }
}
