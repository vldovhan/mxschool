<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool version file.
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

require('../../../config.php');
require ('create_generic_checkin_clesses.php');
require_once($CFG->dirroot."/local/mxschool/classes/alerts/alerts.php");

$dorm_abbr = optional_param('dorm','',PARAM_TEXT);

require_login();
$systemcontext   = context_system::instance();
require_capability('local/mxschool:checkin_settings', $systemcontext);

$title = get_string('create_generic_checkin','local_mxschool');

$PAGE->set_url(new moodle_url("/local/mxschool/check-in/create-generic-checkin.php", array()));
$PAGE->navbar->add(get_string('pluginname', 'local_mxschool'), new moodle_url('/local/mxschool/index.php'));
$PAGE->navbar->add(get_string('checkin_name', 'local_mxschool'), new moodle_url('/local/mxschool/check-in/index.php'));
$PAGE->navbar->add($title);
$PAGE->requires->jquery();
$PAGE->set_pagelayout('admin');
$PAGE->set_context(context_system::instance());
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->add_body_class('print-table');

$form = new generic_checkin_form(null);
$table = null;
if (!$form->is_cancelled() && $data = $form->get_data()) {
    $dorm_abbr = $data->dorm;
    $dorm = $DB->get_record('local_mxschool_dorms',array('abbreviation'=>$dorm_abbr));
    $table = new generic_checkin_table('generic_checkin_table', $dorm_abbr);
    $table->is_collapsible = false;
    $total_records = $DB->count_records('local_mxschool_students', array('dorm'=>$dorm_abbr));
}elseif($dorm_abbr != ''){
    $dorm = $DB->get_record('local_mxschool_dorms',array('abbreviation'=>$dorm_abbr));
    $table = new generic_checkin_table('generic_checkin_table', $dorm_abbr);
    $table->is_collapsible = false;
    $total_records = $DB->count_records('local_mxschool_students', array('dorm'=>$dorm_abbr));
}

echo $OUTPUT->header();

if($table){
    echo html_writer::tag('div',html_writer::link(new moodle_url('#'), get_string('print', 'local_mxschool'),array('class' => 'btn','id'=>'print-table')),array('class' => 'before-table'));
    echo $OUTPUT->heading(get_string('generic_checkin_table_title', 'local_mxschool', $dorm->name), 2, 'table_title_center');
    echo html_writer::start_tag('div', array('class' => 'mxschool-table-box no-full-table'));
    $table->out($total_records, true);
    echo html_writer::end_tag("div");
}else{
    echo $OUTPUT->heading($title);
    echo html_writer::start_tag('div', array('class' => 'mxschool-table-box'));
    $form->display();
    echo html_writer::end_tag("div");
}

echo $OUTPUT->footer();


if($table):?>
    <script>
        $(window).ready(function () {
            $('#print-table').click(function (e) {
                window.print();
                return false;
            });
        });
    </script>
<?php endif;