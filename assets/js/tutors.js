jQuery(document).ready(function(){
    $('.mxschool-table-box input[type="checkbox"]').change(function () {
        var checked = this.checked;
        var data = $(this).attr('name').split('_');
        var element = this;
        jQuery.ajax({
            url: M.cfg.wwwroot+"/local/mxschool/tutors/tutors-list.php",
            type: 'POST',
            dataType: 'json',
            data: {
                'user_id' :data[0],
                'cat_id' :data[1],
                'state' :checked,
                'action' :'saveusercat'
            },
            complete: function(data) {
                $(element).after('<span class="saved">Saved</span>');
                setTimeout(function () {
                    $('.saved').fadeOut(function() {
                        $(this).remove();
                    });
                }, 1000);
            }
        });
    });

    $('#subject_tutored').change(function () {
        var val = $(this).val();
        if(val != ''){
            jQuery.ajax({
                url: M.cfg.wwwroot+"/local/mxschool/tutors/get_course_list.php",
                type: 'POST',
                dataType: 'json',
                data: {
                    'cat_id' :val,
                },
                complete: function(data) {
                    var old_val = $('#course_tutored').val();
                    var empty_option = '<option value="">------</option>';
                    $('#course_tutored').html(empty_option+data.responseJSON.options_text);
                    $('#course_tutored').val(old_val);
                }
            });
        }else{
            $('#course_tutored').html('<option value="">------</option>');
        }
    });

    $('#subject_tutored').change();
});