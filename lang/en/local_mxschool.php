<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mxschool language file.
 *
 * @package    local_mxschool
 * @copyright  2016 mxschool.edu
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'Middlesex School';
$string['settings'] = 'Settings';
$string['mxschooldroot'] = 'Middlesex School';
$string['advisor_selection'] = 'Advisor selection';
$string['advisor_selection:manage'] = 'Advisor selection managing';
$string['select_advisor'] = 'Select advisor';
$string['view_and_edit'] = 'View & Edit Records';
$string['manage_student_selections'] = 'Manage Student Selections';
$string['students_with_new_advisors'] = 'Students with New Advisors';
$string['preferences_sa'] = 'Administrative Preferences / Faculty Availability';
$string['preferences'] = 'Administrative Preferences';
$string['form_start_date'] = 'Form display start date';
$string['form_end_date'] = 'Form display end date';
$string['required_field'] = 'This field is required!';
$string['advisor_message'] = 'Advisor Selection Closed Message';
$string['advisor_availability'] = 'Advisor availability';
$string['save'] = 'Save';
$string['advisor_form_enable'] = 'When do you want to enable the Advisor Selection Form?';
$string['second_semester_starts'] = 'Second Semester Start';
$string['dorms_close'] = 'Dorms Close';
$string['time_prior'] = 'Time prior to turn on form';
$string['advisor_selection_form'] = 'Advisor selection form';
$string['studentname'] = 'Student Name';
$string['currentadvisor'] = 'Current Advisor';
$string['keep_current_advisor'] = 'Keep current advisor?';
$string['advisors'] = 'Available advisors';
$string['students_not_selected'] = 'Not submitted students';
$string['select_advisor'] = 'Select advisor';
$string['student'] = 'Student';
$string['choosenadvisor'] = 'Choosen Advisor';
$string['actions'] = 'Actions';
$string['preview'] = 'Preview';
$string['comment'] = 'Comment';
$string['adviser_record'] = 'Advisor Selection Record';
$string['advisorchoice'] = 'Advisor Choice';
$string['notifications'] = 'Notifications';
$string['manage_students'] = 'Manage students';
$string['gender'] = 'Gender';
$string['admissionsyear'] = 'Admissions year';
$string['dorm'] = 'Dorm';
$string['room'] = 'Room';
$string['assign_students'] = 'Assign students';
$string['assign_faculties'] = 'Assign faculty members';
$string['create_user'] = 'Create user';
$string['user_management'] = 'Users management';
$string['manage_faculties'] = 'Manage faculty members';
$string['manage_deans'] = 'Manage deans';
$string['manage_dorms'] = 'Manage dorms';
$string['manage_parents'] = 'Manage parents';
$string['faculty'] = 'Faculty';
$string['type'] = 'Type';
$string['available'] = 'Available';
$string['notavailable'] = 'Not available';
$string['create_dorm'] = 'Create dorm';
$string['abbreviation'] = 'Abbreviation';
$string['name'] = 'Name';
$string['parent_name'] = 'Parent name';
$string['homephone'] = 'Home phone';
$string['cellphone'] = 'Cell phone';
$string['workphone'] = 'Work phone';
$string['edit_student'] = 'Edit student';
$string['edit_faculty'] = 'Edit faculty';
$string['edit_dorm'] = 'Edit dorm';
$string['create_dorm'] = 'Create dorm';
$string['dormexists'] = 'Dorm abbreviation already exists';
$string['create_parent'] = 'Create parent';
$string['edit_parent'] = 'Edit parent';
$string['grade'] = 'Grade';
$string['dormname'] = 'Dorm';
$string['studentcell'] = 'Student Cell';
$string['dormphone'] = 'Dorm Phone';
$string['overnight'] = 'Overnight';
$string['driving'] = 'Driving';
$string['boston'] = 'To Boston?';
$string['maydrive'] = 'Can Drive to Town?';
$string['maygiverides'] = 'Can Give Rides?';
$string['swimcompetent'] = 'Competent Swimmer?';
$string['swimallowed'] = 'Swimming';
$string['boatallowed'] = 'Boating';
$string['birthdate'] = 'Date';
$string['address'] = 'Address';
$string['email'] = 'Email';
$string['announcements'] = 'Announcements';
$string['create_announcement'] = 'Create announcement';
$string['edit_announcement'] = 'Edit announcement';
$string['title'] = 'Title';
$string['description'] = 'Text';
$string['announcementdate'] = 'Announcement date';
$string['startdate'] = 'Announcement start date';
$string['enddate'] = 'Announcement end date';
$string['visibility'] = 'Visibility';
$string['create'] = 'Create';
$string['preferences_name'] = 'Preferences';
$string['preferences_week'] = 'Week Info dates';
$string['preferences_date'] = 'Open/Close dates';
$string['second_semester_starts'] = 'Second semester starts on: ';
$string['dorms_open_on'] = 'Dorms open on: ';
$string['dorms_close_on'] = 'Dorms close on: ';
$string['created'] = 'Created';
$string['course'] = 'Course';
$string['state'] = 'Status';
$string['open'] = 'Open';
$string['closed'] = 'Closed';
$string['free'] = 'Free';
$string['vacation'] = 'Vacation';
$string['vacationandtravel'] = 'Vacation Travel';
$string['vacationandtravel_form'] = 'Submit Vacation Travel Form';
$string['view_departure'] = 'Departure Records';
$string['view_return'] = 'Return Records';
$string['departure_pickuptime'] = 'Departure Pickup Times';
$string['return_pickuptime'] = 'Return Pickup Times';
$string['return_pickupsites'] = 'Return Pickup Sites';
$string['departure_pickupsites'] = 'Departure Pickup Sites';
$string['deans_phone'] = 'Dean\'s Phone';
$string['deans_email'] = 'Dean\'s Email';
$string['date_time'] = 'Date/Time';
$string['add_pickup_time'] = 'Add Pickup Time';
$string['add_pickup_site'] = 'Add Pickup Site';
$string['site'] = 'Site';
$string['destination'] = 'Destination';
$string['stu_cellphone'] = 'Phone #';
$string['departure'] = 'Departure';
$string['return'] = 'Return';
$string['departure_date_time'] = 'Departure Date & Time';
$string['return_date_time'] = 'Return Date & Time';
$string['transportation_needed'] = 'School Transportation Needed?';
$string['transportation_type'] = 'Transportation Type';
$string['sitetype'] = 'Site type';
$string['allow_multiple_vacations'] = 'Allow multiple vacation records';
$string['clear_all_records'] = 'Clear all records';
$string['mxschool:view'] = 'Mxschool: View plugin';
$string['mxschool:manage'] = 'Mxschool: Manage';
$string['mxschool:notifications_manage'] = 'Mxschool: Manage notifications';
$string['mxschool:settings'] = 'Mxschool: Manage preferences';
$string['mxschool:announcements_manage'] = 'Mxschool: Manage announcements';
$string['mxschool:manage_users'] = 'Mxschool: Manage users';
$string['mxschool:vacation_manage'] = 'Mxschool: Manage Vacation Travel records';
$string['mxschool:vacation_settings'] = 'Mxschool: Manage vacation preferences';
$string['mxschool:advisor_selection_manage'] = 'Mxschool: Manage selected advisor records';
$string['mxschool:advisor_selection_settings'] = 'Mxschool: Manage advisor selection preferences';
$string['mxschool:rooming_manage'] = 'Mxschool: Manage rooming form';
$string['mxschool:rooming_settings'] = 'Mxschool: Manage rooming preferences';
$string['mxschool:import_students'] = 'Mxschool: Import Students';
$string['mxschool:clear_all_records'] = 'Mxschool: Clear all records';
$string['mxschool:driving_settings'] = 'Mxschool: Driving settings';
$string['mxschool:advisor_selection_view'] = 'Mxschool: Advisor selection view';
$string['mxschool:checkin_settings'] = 'Mxschool: Checkin settings';
$string['mxschool:tutors_settings'] = 'Mxschool: Tutors settings';
$string['mxschool:vacation_view'] = 'Mxschool: Vacation view';
$string['mxschool:weekend_settings'] = 'Mxschool: Weekend settings';
$string['rooming_name'] = 'Rooming';
$string['rooming_form'] = 'Rooming Form';
$string['enter_rooming_form'] = 'Enter Rooming Form';
$string['rooming_requests'] = 'View Rooming Requests';
$string['room_type'] = 'Room type';
$string['single'] = 'Single';
$string['double'] = 'Double';
$string['triple'] = 'Triple';
$string['quad'] = 'Quad';
$string['current_dorm'] = 'Current dorm';
$string['request_room_type'] = 'Request Room Type';
$string['dormate_from_grade'] = 'Requested Dormmate(s)<br />from Your Grade';
$string['dormate_from_any_grade'] = 'Requested Dormmate(s)<br />from Any Grade';
$string['dormate_check'] = 'Check if you have lived in a one-room double in the past';
$string['dormate_info'] = '<br>Because there are several one-room doubles on campus, there are years when students who prefer to be in a single must live in a double. If you have not lived in a one-room double before, please indicate with whom you would want to live if placed in one.<br>';
$string['save'] = 'Save';
$string['preferred_roommate'] = 'Preferred Roommate';
$string['advisorname'] = 'Advisor';
$string['preferences_vacations'] = 'Vacations Dates';
$string['thanksgiving_vacation_date'] = 'Thanksgiving Vacation Date';
$string['december_vacation_date'] = 'December Vacation Date';
$string['march_vacation_date'] = 'March Vacation Date';
$string['summer_vacation_date'] = 'Summer Vacation Date';
$string['vacation_form_enable'] = 'When do you want to enable the Vacation Travel Form?';
$string['rooming_time_prior'] = 'Time prior to turn on form before dorm close';
$string['yearofgraduation'] = 'Year of Graduation';
$string['clear_advisor'] = 'Clear Advisor Selection Records';
$string['clear_vacation'] = 'Clear Vacation Travel Records';
$string['clear_rooming'] = 'Clear Rooming Records';
$string['clear_announcements'] = 'Clear Announcements Records';
$string['importusers'] = 'Import Students';
$string['enable'] = 'Enable';
$string['disable'] = 'Disable';
$string['reset_student_records'] = 'Reset student\'s records';
$string['tutors_name'] = 'Peer Tutoring';
$string['create_category'] = 'New Subject';
$string['categories'] = 'Departments';
$string['category'] = 'Department';
$string['caregory_form'] = 'Course Subject form';
$string['create_course'] = 'New course';
$string['course_form'] = 'Course form';
$string['tutors_list'] = 'Tutors list';
$string['tutor_session_form'] = 'Peer Tutoring Session Report Form';
$string['peer_tutors'] = 'Tutor name';
$string['subject_tutored'] = 'Subject tutored';
$string['course_tutored'] = 'Course tutored';
$string['tutor_session_topic'] = 'Topic of tutor session';
$string['type_help'] = 'Type of help requested';
$string['effectiveness'] = 'Effectiveness of session';
$string['notes'] = 'Notes';
$string['peer_tutoring_records'] = 'Submit Peer Tutoring Form';
$string['new_peer_tutoring_record'] = 'New Peer Tutoring Record';
$string['start_date'] = 'Start date';
$string['end_date'] = 'End date';
$string['manage_tutors'] = 'Manage peer tutors';
$string['assign_tutors'] = 'Assign peer tutors';
$string['edit_tutor'] = 'Edit tutor';
$string['clear_subjects_courses_tutors_records'] = 'Clear courses records (not moodle courses), subjects records and peer tutors records';
$string['clear_peer_tutors_records'] = 'Clear peer tutors records';
$string['checkin_name'] = 'Check-in Sheets';
$string['create_weekday_checkin'] = 'Weekday Check-in Sheet';
$string['generate_table'] = 'Generate table';
$string['weekday_checkin_table_title'] = 'Weekday Check-in Sheet for {$a} for the Week of _______';
$string['print'] = 'Print';
$string['weekend'] = 'Weekend';
$string['long_weekend_days'] = 'Long weekend days';
$string['create_weekend_checkin'] = 'Weekend Check-in Sheet';
$string['all_dorm_all'] = 'All Houses';
$string['all_dorm_day'] = 'All Day Houses';
$string['all_dorm_boarding'] = 'All Boarding Houses';
$string['weekend_checkin_table_title'] = 'Weekend Check-in Sheet for {$a->dorm} for the Weekend of {$a->date}';
$string['create_generic_checkin'] = 'Generic Check-in Sheet';
$string['generic_checkin_table_title'] = 'Check-in Sheet for {$a} for _______';
$string['weekend_name'] = 'Weekend';
$string['enter_weekend'] = 'Submit Weekend Form';
$string['enter_weekend_form'] = 'Weekend Form';
$string['departure_date'] = 'Departure Date';
$string['return_date'] = 'Return Date';
$string['destination'] = 'Destination';
$string['transportation'] = 'Transportation by';
$string['phone_going_home'] = 'Phone number (even if you are going home)';
$string['send_weekend_form'] = 'Send Weekend Form';
$string['create_assembly_checkin'] = 'Assembly Check-in Sheet';
$string['number_of_groups'] = '# of Groups';
$string['group'] = 'Group';
$string['assembly_checkin_table_title'] = 'Check-in Group #{$a}';
$string['weekend_instruction_top'] = '<p id="instruction">Please fill out the form entirely.  Your form should be submitted to your 
Head of House no later than <b>10:30 PM on Friday</b>.  All relevant phone calls
 giving permission should also be received by  Friday at 10:30 PM.  <i>(Voice mail
  messages are OK; Email messages are NOT)</i><br></p>';
$string['weekend_instruction_bottom'] = '<i><br>You may not leave for the weekend until you see your name on the \'OK\' list.
  <br><br>
  Permission phone calls should be addressed to <b>{$a->hohname}</b> @ <b>{$a->hohphonenum}</b>.
  <br><br>
  If your plans change, you must get permission from <b>{$a->hohname}</b>.
  
  <b>Remember to sign out.</b><br></i>';
$string['view_weekends_calculator'] = 'View Weekends Calculator';
$string['semester'] = 'Semester';
$string['weekend_calculator_table_title'] = 'On-Campus Weekend Calculator for {$a->dorm} - {$a->semester}';
$string['manage_weekend_form'] = 'Manage weekend';
$string['driving_name'] = 'Driving';
$string['enter_driving_form'] = 'Day Student Driver Registration';
$string['date_license'] = 'Issue date of License';
$string['submit_form'] = 'Submit form';
$string['print_form'] = 'Print form';
$string['make_of_car'] = 'Make of car';
$string['model_of_car'] = 'Model of car';
$string['color'] = 'Color';
$string['license_plate'] = 'License Plate';
$string['license'] = 'License';
$string['manage_driving'] = 'Manage Driving';
$string['cars'] = 'Cars';
$string['create_driving'] = 'Driver: Sign Out';
$string['clear_weekend_records'] = 'Clear all weekend records';
$string['clear_driving_records'] = 'Clear all driving records';
$string['esignout_name'] = 'eSignout';
$string['drivings'] = 'Drivings';
$string['passengers'] = 'Passengers';

$string['manage_driver'] = 'Driver Records';
$string['driver_name'] = 'Driver';
$string['create_driver'] = 'New Driver: Sign In';
$string['update_driver'] = 'Update Driver: Sign In';
$string['create_passenger'] = 'Passenger: Sign Out';
$string['update_passenger'] = 'Update Passenger: Sign In';
$string['driver_grades'] = 'Driver Grades';
$string['passengers_name'] = 'Passenger Name(s)';
$string['estimated_return_time'] = 'Estimated Return Time';
$string['permission_granted_by'] = 'Face to face permission granted by';
$string['manage_passenger'] = 'Passenger Records';
$string['permission_verified'] = 'Permissions verified';
$string['permissions_required_text'] = 'Passenger Permissions Required Text';
$string['no_data'] = 'No data';
$string['import_course_form'] = 'Import course';
$string['example'] = 'Example';
$string['import'] = 'Import';
$string['recordscreated'] = 'Records created';
$string['recordsprocessed'] = 'Records processed';
$string['recordserrors'] = 'Records errors';
$string['import_tutoring_form'] = 'Import tutoring';
$string['tutor_not_found'] = 'Peer Tutor not found';
$string['clear_esignout_records'] = 'Clear eSignout records';
